#!/usr/bin/env python
# -*- coding:utf-8 -*-

# Стандартные библиотеки
from __future__ import unicode_literals

# Библиотеки Джанго
from django.contrib import admin
#from django.contrib.auth.models import User

# Локальные импорты
from blog.models import BlogPost, Comment

admin.site.register(BlogPost)
admin.site.register(Comment)