#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
Definition of views.
"""

# Стандартные библиотеки
from __future__ import unicode_literals
import time
import hashlib, random, json
import re 
from datetime import date, datetime, timedelta


# Библиотеки Джанго
from django.shortcuts import render
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponse, HttpResponseRedirect, HttpRequest, JsonResponse
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.core.mail import send_mail
from django.utils import timezone, translation
from django.views.generic import View, ListView, CreateView, DetailView
from django.views.generic.edit import DeleteView, UpdateView, FormView, FormMixin
from django.utils.decorators import method_decorator
from django.core.urlresolvers import reverse
from django.db.models import Avg, Sum, Count
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.csrf import ensure_csrf_cookie
from django.utils.translation import ugettext_lazy as _
from django.db.models import Q

from blog.forms import CreatePostForm, CommentForm
from blog.models import BlogPost, Comment
# Create your views here.

class Blog(ListView):
    def get(self, request, *args, **kwargs):
        queryset = BlogPost.objects.order_by('-timestamp')
        paginator = Paginator(queryset, 5) # Show 25 contacts per page

        page = request.GET.get('page')
        try:
            object_list = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            object_list = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            object_list = paginator.page(paginator.num_pages)


        return render(request, 'blog/blogpost_list.html', {'object_list': object_list})

class CreateBlogView(CreateView):

    model = BlogPost
    form_class = CreatePostForm
    template_name = 'blog/edit_blog.html'
    def form_valid(self, form):
        post = form.save(commit=False)
        post.author = self.request.user
        post.timestamp = timezone.now()
        return super(CreateBlogView, self).form_valid(form)


    def get_success_url(self):
        return reverse('blog:blog')

    def get_context_data(self, **kwargs):

        context = super(CreateBlogView, self).get_context_data(**kwargs)
        context['action'] = reverse('blog:blog-new')

        return context

class BlogPostView(DetailView, FormMixin):
    model = BlogPost
    template_name = 'blog/blogpost.html'
    form_class = CommentForm
    def get_success_url(self):
        return reverse('blog:blogpost-view', kwargs={'pk': self.object.pk})

    def get_context_data(self, **kwargs):
        context = super(BlogPostView, self).get_context_data(**kwargs)
        comments = Comment.objects.filter(blogpost = self.get_object().id)
        context['comments'] = comments
        if self.request.user.is_authenticated():
            context['form'] = self.get_form()
        context['action'] = reverse('blog:blogpost-view', kwargs={'pk': self.get_object().id})
        return context

    def post(self, request, *args, **kwargs):
        if not request.user.is_authenticated():
            return HttpResponseForbidden()
        self.object = self.get_object()
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        # Here, we would record the user's interest using the message
        # passed in form.cleaned_data['message']
        comment = form.save(commit = False)
        comment.author = self.request.user
        comment.timestamp = timezone.now()
        comment.blogpost = self.get_object()
        form.save() 
        return super(BlogPostView, self).form_valid(form)


class UpdateBlogPostView(UpdateView):

    model = BlogPost
    form_class = CreatePostForm

    template_name = 'blog/edit_blog.html'

    def get_success_url(self):
        return reverse('blog:blog')

    def get_context_data(self, **kwargs):

        context = super(UpdateBlogPostView, self).get_context_data(**kwargs)
        context['action'] = reverse('blog:blogpost-edit', kwargs={'pk': self.get_object().id})

        return context

class UpdateCommentView(UpdateView):

    model = Comment
    form_class = CommentForm
    template_name = 'blog/blogpost.html'

    def get_success_url(self):
        return reverse('blog:blogpost-view', kwargs={'pk': self.get_object().blogpost.id})

    def get_context_data(self, **kwargs):

        context = super(UpdateCommentView, self).get_context_data(**kwargs)
        context['action'] = reverse('blog:comment-edit', kwargs={'blogpost_id': self.get_object().blogpost.id, 'pk': self.get_object().id })
        context['blogpost'] = self.get_object().blogpost
        context['comments'] = Comment.objects.filter(blogpost = self.get_object().blogpost, parent_comment = None)

        return context

class DeleteBlogPostView(DeleteView):

    model = BlogPost
    template_name = 'blog/delete_blogpost.html'

    def get_success_url(self):
        return reverse('blog:blog')

class DeleteCommentView(DeleteView):

    model = Comment
    template_name = 'blog/delete_comment.html'

    def get_success_url(self):
        return reverse('blog:blogpost-view', kwargs={'pk': self.get_object().blogpost.id})
