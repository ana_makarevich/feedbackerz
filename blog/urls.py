"""
Definition of urls for DjangoWebProject.
"""


from datetime import datetime
from django.conf.urls import patterns, url,include
from django.contrib import admin
from django.views.generic import TemplateView
from blog.views import (Blog, CreateBlogView, BlogPostView, UpdateBlogPostView,
    DeleteBlogPostView, UpdateCommentView, DeleteCommentView)
from django.utils.translation import ugettext_lazy as _

# Uncomment the next lines to enable the admin:
admin.autodiscover()


urlpatterns = patterns('',
	url(r'^blog/$', Blog.as_view(), name='blog',),
    url(r'^new$', CreateBlogView.as_view(), name='blog-new',),
    url(r'^(?P<pk>\d+)/$', BlogPostView.as_view(), name='blogpost-view',),
    url(r'^edit/(?P<pk>\d+)/$', UpdateBlogPostView.as_view(), name='blogpost-edit',),
    url(r'^(?P<blogpost_id>\d+)/edit_comment/(?P<pk>\d+)/$', UpdateCommentView.as_view(), name='comment-edit',),
    url(r'^delete/(?P<pk>\d+)/$', DeleteBlogPostView.as_view(), name='blogpost-delete',),
    url(r'^(?P<blogpost_id>\d+)/delete-comment/(?P<pk>\d+)/$', DeleteCommentView.as_view(), name='comment-delete',),)