#!/usr/bin/env python
# -*- coding:utf-8 -*-
from django.db import models
from django.core.urlresolvers import reverse
from app.models import CustomUser
# Create your models here.

class Tag(models.Model):
	tag = models.CharField(max_length=50)
	def save(self, *args, **kwargs):
		if isinstance(self.tag, str):
			self.tag = self.tag.decode("utf-8")
		super(Tag, self).save(*args, **kwargs)
	class Meta:
		verbose_name = u'Тег'
		verbose_name_plural = u'Теги'

class BlogPost(models.Model):
	title = models.CharField(max_length = 200)
	author = models.ForeignKey(CustomUser)
	body = models.TextField()
	timestamp = models.DateTimeField()
	tag = models.ManyToManyField(Tag, blank = True)
	def save(self, *args, **kwargs):
		if isinstance(self.title, str):
			self.title = self.title.decode("utf-8")
		if isinstance(self.body, str):
			self.body = self.body.decode("utf-8")
		super(BlogPost, self).save(*args, **kwargs)	
	def get_absolute_url(self):
		return reverse('blog:blogpost-view', kwargs={'pk': self.id})

class Comment(models.Model):
	blogpost = models.ForeignKey(BlogPost)
	author = models.ForeignKey(CustomUser)
	title = models.CharField(max_length=200)
	body = models.TextField()
	timestamp = models.DateTimeField()
	parent_comment = models.ForeignKey("self", blank = True, null = True, default = None)
	def save(self, *args, **kwargs):
		if isinstance(self.title, str):
			self.title = self.title.decode("utf-8")
		if isinstance(self.body, str):
			self.body = self.body.decode("utf-8")
		super(Comment, self).save(*args, **kwargs)	