#!/usr/bin/env python
# -*- coding:utf-8 -*-

# Стандартные библиотеки
from __future__ import unicode_literals
import re
import datetime

# Библиотеки Джанго
#from django.contrib.auth.models import User
from django import forms
from django.contrib.auth.forms import AuthenticationForm
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ValidationError
from django.utils import timezone

# Импорт каптчи
#from nocaptcha_recaptcha.fields import NoReCaptchaField

# Локальные импорты 
from blog.models import BlogPost, Comment

class CreatePostForm(forms.ModelForm):
	class Meta:
		model = BlogPost
		fields = ('title', 'body',)
		widgets = {
		'title': forms.TextInput(attrs={'class': 'form-control'}),
		'body': forms.Textarea(attrs={'class': 'form-control'}),
		}
		labels = {
		'title':_(u'Название поста'),
		'body':_(u'Тест поста'),
		}

class CommentForm(forms.ModelForm):
	class Meta:
		model = Comment
		fields = ('title', 'body')
		widgets = {
		'title': forms.TextInput(attrs={'class': 'form-control'}),
		'body': forms.Textarea(attrs={'class': 'form-control'}),		
		}
		labels = {
		'title':_(u'Тема'),
		'body':_(u'Мысль полностью'),
		}
