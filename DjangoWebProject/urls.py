"""
Definition of urls for DjangoWebProject.
"""


from datetime import datetime
from django.conf.urls import patterns, url,include
from django.contrib import admin
from app.models import CustomUser, GTSIL_Results_Distribution
from rest_framework import routers, serializers, viewsets
from django.utils.translation import ugettext_lazy as _
import django_filters
from rest_framework import filters
from rest_framework import generics


# Serializers define the API representation.
class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = CustomUser
        fields = ('url', 'username', 'is_staff', 'status_confirmed')

# ViewSets define the view behavior.
class UserViewSet(viewsets.ModelViewSet):
    queryset = CustomUser.objects.all()
    serializer_class = UserSerializer

"""
class UserSerializer(serializers.Serializer):
    email = serializers.EmailField()
    username = serializers.CharField(max_length=100)

class CommentSerializer(serializers.Serializer):
    user = UserSerializer()
    content = serializers.CharField(max_length=200)
    created = serializers.DateTimeField()
"""

class GroupSerializer(serializers.Serializer):
    name = serializers.CharField(max_length = 100)

class LectureSerializer(serializers.Serializer):
    lecture_start_time = serializers.DateTimeField()

class SubjectSerializer(serializers.Serializer):
    full_name = serializers.CharField(max_length = 100)
    subject_id = serializers.CharField(max_length=10)
class InstructorSerializer(serializers.Serializer):
    cist_id = serializers.CharField(max_length=5)
    last_name = serializers.CharField(max_length = 50)
    first_name = serializers.CharField(max_length = 50)
    middle_name = serializers.CharField(max_length = 50)
class CriteriaSerializer(serializers.Serializer):
    criteria_name = serializers.CharField(max_length=100)

class GTSILResultDistributionSerializer(serializers.HyperlinkedModelSerializer):
    lecture = LectureSerializer()
    group = GroupSerializer()
    subject = SubjectSerializer()
    instructor = InstructorSerializer()
    criteria = CriteriaSerializer()

    class Meta:
        model = GTSIL_Results_Distribution

class GTSILResultDistributionViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = GTSIL_Results_Distribution.objects.all()
    serializer_class = GTSILResultDistributionSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filter_fields = ('instructor', 'criteria','group', 'subject', 'lecture__gtsi__activity_type')
"""
class GTSILResultDistributionQueryViewSet(viewsets.ModelViewSet):
    queryset = GTSIL_Results_Distribution.objects.all()
    serializer_class = GTSILResultDistributionSerializer
    def get_querysety(self):
"""

# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
#router.register(r'users', UserViewSet)
router.register(r'distributions', GTSILResultDistributionViewSet)
#router.register(r'distributions/subject=(?P<subject_id>.+)/(?P<instructor_id>.+)/(?P<activity_id>.+)$', GTSILResultDistributionQueryViewSet)

# Uncomment the next lines to enable the admin:
admin.autodiscover()

urlpatterns = [
    url(r'^', include('app.urls', namespace="main")),
    url(r'^', include('news.urls', namespace="news")),
    url(r'^',include('blog.urls', namespace="blog")),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^apiz/', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls',namespace='rest_framework')),
    url(r'^i18n/', include('django.conf.urls.i18n')),
]