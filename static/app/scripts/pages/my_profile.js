// Стандартные параметры проверки полей. false - поле не проверено или содержит ошибку, true - всё нормально (Используется для отметки не обязательных полей).
var fields_valid_values = {'first_name':true, 'last_name':true, 'middle_name':true, 'password':true, 'password_login':false};

if (document.getElementById('middle_name') == null)
{
    fields_valid_values["middle_name"] = true;
}

// Регулярные выражения для проверки значений полей. Должны совпадать с таковыми на стороне сервера.
var name_regex = /^(([A-Za-z]+)([ ]?([ ]?[\-|\.|\'|\`][ ]?)?)([A-Za-z]+))$|^(([А-ЯА-яІіЇїЄєЁё]+)([ ]?([ ]?[\-|\.|\'|\`][ ]?)?)([А-ЯА-яІіЇїЄєЁё]+))$/;
var username_regex = /^([A-Z]|[a-z]|[0-9]|\.|_|\-){3,30}$/;
var email_regex = /^([A-Z]|[a-z]|[0-9]|\.|_|\-)+@([A-Z]|[a-z]|[0-9]|\.|_|\-)+\.([A-Z]|[a-z]){2,4}$/;

// Словарь локализации. В будущем планируется получать его из HTML разметки, в которую он попадёт на этапе формирования разметки на стороне сервера.
var registration_form_locale_dictioanary = {
    "required_field" : "Это необходимое поле, но оно пустое", "unacceptable_symbols": "Содержит недопустимые символы", "username_length" : "Имя пользователя должно состоять из 3-30 символов", "email_format" : "Неверный формает email",
    "password_mismatch" : "Пароли не совпадают",
    "form_isnot_complited" : "Не все поля заполнены верно",
    "name_isnt_available" : "Имя пользователя уже занято",
    "email_isnt_available" : "Email уже используется"
}

var submit_disabled = true; // Стандартное значение, запрещающее отправку формы (Не все поля проверены или же они содержат ошибку).

// Функция, обрабатывабщая стандатное событие проверки поля (по ID элемента). Принимает regex, значение поля, является ли поле обязательным и тексты, выводимые в случае ошибки.
function validator_notify(element_id, regex, field_value, is_required, empty_value, empty_field_error, regex_error)
{   // element_id - string, имя элемента, куда выводить сообщение, regex - регулярное выражение для проверки filed_value (значения поля), is_required - обязательно ли поле, empty_filed_error & regex_error - тексты ошибок.
	if (is_required == null)
	{
		is_required = true;
	}
	
	var field_name = 'field_' + element_id;
	if (!(field_value == empty_value || field_value == "") && field_value != null)
	{
		if(regex.test(field_value))
		{
			$(this).removeClass('invalid').removeClass('undeffined').addClass('valid');
			document.getElementById(element_id).style.boxShadow = '0 0 3px 2px lightgreen';
			!is_mobile() ? hide_element_notification(field_name) : $('#field_' + element_id).next().text('');
			fields_valid_values[element_id] = true;
		}
		else
		{
			!is_mobile() ? element_notification(field_name, regex_error, 'right middle', 'info') : $('#field_' + element_id).next().text(regex_error).css('color','red').animate({'paddingLeft':'5px'}, 400).animate({'paddingLeft':'3px'}, 400);
			$(this).removeClass('valid').removeClass('undeffined').addClass('invalid');
			fields_valid_values[element_id] = false;
			document.getElementById(element_id).style.boxShadow = '0 0 3px 2px red';
		}
	}
	else if (is_required)
	{
		!is_mobile() ? element_notification(field_name, empty_field_error, 'right middle', 'info') : $('#field_' + element_id).next().text(empty_field_error).css('color','red').animate({'paddingLeft':'5px'}, 400).animate({'paddingLeft':'3px'}, 400);
		$(this).removeClass('valid').removeClass('undeffined').addClass('invalid');
		fields_valid_values[element_id] = false;
		document.getElementById(element_id).style.boxShadow = '0 0 3px 2px red';
	}
	else
	{
		$(this).removeClass('invalid').removeClass('vaid').addClass('undeffined');
		document.getElementById(element_id).style.boxShadow = '';
		!is_mobile() ? hide_element_notification(field_name) : $('#field_' + element_id).next().text('');
		fields_valid_values[element_id] = true;
	}
}

// По завершению загрузки и подготовки страницы
$(document).ready(function()
{
    $('input#password1').change(function(){
        $('input#password2').val("");
        fields_valid_values['password'] = false;
        document.getElementById('password2').style.boxShadow = '';
        !is_mobile() ? hide_element_notification('password2') : $('#field_password2').next().text('');
    });
    
	// Обработчик потери фокуса для всех полей ввода текста
	$('input#first_name, input#last_name, input#middle_name, input#password2, input#password_login').unbind().blur( function()
	{
    	var field_id = $(this).attr('id');
		var field_value = $(this).val();
        !is_mobile() ? hide_element_notification(field_id) : $('#field_' + field_id).next().text('');
		
		switch(field_id)
		{
			// Эти поля обрабатываются одинаково, воизбежания повторения кода, делаем множественную выборку
			case 'first_name':
            case 'last_name':
            case 'middle_name':
			{
               	validator_notify(field_id, name_regex, field_value, false, "", registration_form_locale_dictioanary["required_field"], registration_form_locale_dictioanary["unacceptable_symbols"]);
				break;
			}
            
            case 'password2':
			{
				// Получаем значение полей из формы
				var password_value = document.getElementById("password1").value;
				var password_validator_value = document.getElementById("password2").value;
                
				if (password_value == password_validator_value)
                {
                    if (password_value != null && password_value != "")
                    {
                        fields_valid_values['password'] = true;
                        document.getElementById('password2').style.boxShadow = '0 0 3px 2px lightgreen';
                    }
                    else
                    {
                        fields_valid_values['password'] = true;
                        document.getElementById('password2').style.boxShadow = '';
                    }
                }
                else
                {
                    fields_valid_values['password'] = false;
                    document.getElementById('password2').style.boxShadow = '0 0 3px 2px red';
		            !is_mobile() ? element_notification(field_id, registration_form_locale_dictioanary["password_mismatch"], 'right middle', 'info') : $('#field_' + field_id).next().text(registration_form_locale_dictioanary["password_mismatch"]).css('color','red').animate({'paddingLeft':'5px'}, 400).animate({'paddingLeft':'3px'}, 400);
                }
				break;
			}
            
            case 'password_login':
            {
                if (field_value != null && field_value != "")
                {
                    fields_valid_values[field_id] = true;
                    document.getElementById(field_id).style.boxShadow = '';
                }
                else
                {
                    fields_valid_values[field_id] = false;
                    document.getElementById(field_id).style.boxShadow = '0 0 3px 2px red';
                }
                break;
            }
		}
		
		submit_disabled = true;
		
		//Проверим сначала значения общих полей 
        if (fields_valid_values["first_name"] && fields_valid_values["last_name"] && fields_valid_values["middle_name"] && fields_valid_values["password"] && fields_valid_values["password_login"])
		{
            submit_disabled = false;
		}
        else
        {
            submit_disabled = true;
        }
	});
});

// Перегрузка событий отправки формы регистрации студента на сервер.
$('#my_profile_form').submit(function(e)
{
    !is_mobile() ? hide_element_notification('sbutton_wrapper') : $('#sbutton_wrapper').next().text('');
    document.getElementById("password_login").focus();
    document.getElementById("submit").focus();
	if (submit_disabled)
	{
		!is_mobile() ? element_notification('sbutton_wrapper', registration_form_locale_dictioanary["form_isnot_complited"], 'right middle', 'info') : $('#sbutton_wrapper').next().text(registration_form_locale_dictioanary["form_isnot_complited"]).css('color','red').animate({'paddingLeft':'5px'}, 400).animate({'paddingLeft':'3px'}, 400);
		e.preventDefault();
	}
	else
	{
		return true;
	}
});