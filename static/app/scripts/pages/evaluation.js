$(document).ready(function() {
    $('#vote_form').submit(function() {
        $.post($('#vote_form').attr("action"), $('#vote_form').serialize(),  function(response) {
            if (response == 'Success')
            {
                $('#vote_form').hide('fast');
                $('#vote_result').html('Проголосовано!');
            }
        });
        return false;
    });
});

function load_survey_content_eval(obj)
{
    var container_div = '#surveys_ajax_inner';
    var content_div = 'div.survey_panel_inner';
    $(container_div).load(obj.getAttribute("href") + " " + content_div, function() {
        hide_element('surveys_ajax_links');
        show_element_fast('surveys_ajax_inner');
        $(container_div).animate({ opacity: 1 });
        $.getScript(survey_script);
        $(container_div).append("<button class='btn btn-default' id='submit' onclick='hide_element(\"surveys_ajax_inner\"); show_element_fast(\"surveys_ajax_links\");'>Назад</button>");
    });
}