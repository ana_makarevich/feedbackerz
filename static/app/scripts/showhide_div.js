/*
    Скрипт, отвечающий за сокрытие/отображения div`ов на страницах.
*/

// Сам Toggle.
function showhide_element(element_id)
{
    var div = document.getElementById(element_id);
    div.style.display = (div.style.display == "none") ? "" : "none";
}

// Скрывающая функция
function hide_element(element_id)
{
    var div = document.getElementById(element_id);
    div.style.display = "none";
}

// Отображающая функция
function show_element(element_id)
{ 
    var div = document.getElementById(element_id);
    div.style.display = ""; // Сброс стиля отображения на стандартное значение, заданное стилями.
}