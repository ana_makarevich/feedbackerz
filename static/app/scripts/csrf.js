/*
    Скрипт, предназначений для удовлетворения требований системы безопасности.
    Определяем метод отправки данных, нужен ли нам CSRF Token, достаём его из Cookies (get_cookie.js) и засовываем в заголовок перед отправкой данных (function prepare_csrf).
*/

function csrfSafeMethod(method)
{
    // Этим HTTP методам не нужна CSRF защита.
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

// Функция, правящая заголовок перед отправкой данных, засовуем CSRF Token.
function prepare_csrf()
{
	$.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain)
            {
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
        }
    });
}