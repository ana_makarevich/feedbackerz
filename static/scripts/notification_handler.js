/*
    Скрипт, содержащий переходники на функции notify.js
*/

// Создание оповещения для указанного элемента с указанными параметрами (текст, позиция, тип).
function element_notification(element_, text_, position_, type_)
{
  $('.' + element_).notify(text_, { position : position_, className : type_, style: 'bootstrap', gap : 3 });
}

// Сокрытие оповещения на заданный элемент (на тот случай, если оно ещё отображается, но уже не актуально).
function hide_element_notification(element_)
{
	$('.' + element_).notify('', { position : 'right middle', className : 'info', showDuration : 0, hideDuration : 200 });
}

// Оповещение, не используюшее элемент для привязки, работает на всю страницу.
function global_notification(text_, position_, type_)
{
	$.notify(text_, { position : position_, className : type_ });
}