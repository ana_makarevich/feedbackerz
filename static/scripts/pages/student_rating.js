// Автозапуск AJAX`ифации контента + привязка ивентов (внутри функций)
$(document).ready(function() {
    prepare_content();
    ajaxify_edit_links();
});

// Callback после загрузки контента через AJAX - заново привязываем все события
function on_ajax_load()
{
    prepare_content();
    ajaxify_edit_links();
}

// Привязка событий к активным элементам
function prepare_content()
{
    $('#tabletitle').click(function() { 
        $('#student_rating_loadable_content').toggle(); 
    });

    $('#application').click(function() { 
        $('#student_rating_form').toggle(); 
    });

    $("#student_rating_form").submit(function(event){
        $.ajax({
            type: "POST",
            url: $(this).attr('action'),
            data: $(this).serialize(),
            success: function(data)
            {
                reload_form();
                scroll_to("#student_rating_loadable_content");
            }
        });
        event.preventDefault();
        return false;
    });
}

// AJAX`ифация ссылок для редактирования
function ajaxify_edit_links()
{
    AjaxContent.init({containerDiv:"#student_rating_panel_content", contentDiv:"#student_rating_panel_content"}).ajaxify_links("a.edit_link");
}

// Открытие формы + Сброс
function fill_in_form()
{
    reload_form();
    $('#student_rating_form').show();
    scroll_to("#application");
}

function reload_form()
{
    $("#student_rating_panel_content").load(student_rating_url + " #student_rating_panel_content", function(){
        on_ajax_load();
    });
}