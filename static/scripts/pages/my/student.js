$(document).ready(function() {
    $('#collapse_1').addClass("in");
    var confcode_form = document.getElementById("confcode_form");
    var attach_group_form = document.getElementById("attach_group_form");
    
    if (confcode_form)
    {
        var err_code = 'Неверный код';
        var err_anker = 'confirm_code_field';
        $('#confcode_form').submit(function() {
            $.post(confcode_form.action, $("#confcode_form").serialize(),  function(response) {
                if (response == "Success")
                {
                    !is_mobile() ? hide_element_notification(err_anker) : $('#' + err_anker).next().text('');
                    $('#confcode_form').hide('fast');
                    $('#form_send_result').html("Статус подтверждён");
                    location.reload();
                }
                else if (response == "Error")
                {
                    !is_mobile() ? element_notification(err_anker, err_code, 'right middle', 'info') : $('#' + err_anker).next().text(err_code).css('color','red').animate({'paddingLeft':'5px'}, 400).animate({'paddingLeft':'3px'}, 400);
                }
            });
            return false;
        });
    }
    
    if (attach_group_form)
    {
        var err_code = 'Неверная группа';
        var err_anker = 'attach_group_field';
		$('#attach_group_form').submit(function() {
            $.post(attach_group_form.action, $("#attach_group_form").serialize(), function(response) {
                if (response == "Success")
                {
                    !is_mobile() ? hide_element_notification(err_anker) : $('#' + err_anker).next().text('');
                    // Временное решение для правильно работы страницы.
                    location.reload();
                }
                else if (response == "Error")
                {
                    !is_mobile() ? element_notification(err_anker, err_code, 'right middle', 'info') : $('#' + err_anker).next().text(err_code).css('color','red').animate({'paddingLeft':'5px'}, 400).animate({'paddingLeft':'3px'}, 400);
                }
            });
            return false;
        });
	}
    
    AjaxContent.init({containerDiv:"#tab_navigation_container", contentDiv:"div.ajax_panel"}).ajaxify_links("a.tab_navigation_link");
});

function lock_forms()
{
    $('form.lock').submit(function() {
        var submit_button = $(this).find('button[type=submit]');
        submit_button.prop("disabled", true);
        submit_button.text(submit_button[0].title);
        submit_button.prop("title", "Если кнопка заблокирована, но ничего не произошло, попробуйте снова, обновив страницу. Это ошибка.");
    });
}

function show_subjects_list()
{
    hide_element_fast('tab_navigation_container');
    hide_element_fast('surveys_panel');
    show_element_slow('subjects_list');
    scroll_to('#subjects_list');
}

function show_tab_navigation_container()
{
    hide_element_fast('subjects_list');
    hide_element_fast('surveys_panel');
    show_element_slow('tab_navigation_container');
    scroll_to('#tab_navigation_container');
}

function show_surveys(url)
{
    hide_element_fast('tab_navigation_container');
    hide_element_fast('subjects_list');
    show_element_slow('surveys_panel');
    scroll_to('#surveys_panel');
}

function on_ajax_load()
{
    $.getScript(eval_script_url);
    show_tab_navigation_container();
    lock_forms();
}

function load_survey_content(obj)
{
    var container_div = '#tab_navigation_container';
    var content_div = 'div.survey_panel';
    $(container_div).load(obj.getAttribute("href") + " " + content_div, function() {
        $(container_div).animate({ opacity: 1 });
        show_tab_navigation_container();
        $.getScript(survey_script);
        lock_forms();
    });
}