﻿function import_confcode_handler()
{
		$('#confcode_form').submit(function() {
		$.post(confirm_status_url, $("#confcode_form").serialize(),  function(response) {
			if (response == "Success")
			{
				hide_element_notification('confirm_code_field');
				$('#confcode_form').hide('fast');
				$('#form_send_result').html("Статус подтверждён");
                location.reload();
			}
			else if (response == "Error")
			{
				element_notification('confirm_code_field', 'Неверынй код', 'right middle', 'warn');
			}
		});
		return false;
	});
}

function import_attach_group_handler()
{
	if (document.getElementById("attach_group_form")) {
		$('#attach_group_form').submit(function() {
		$.post(attach_group_url, $("#attach_group_form").serialize(),  function(response) {
			if (response == "Success")
			{
				hide_element_notification('attach_group_field');
				$('#attach_group_form').hide('fast');
				// $('#form_send_result').html("Группа успешно привязана");
				// $("#tab_navigation").hide('fast');
                // $("#info_panel").hide('fast');
				// $("#tab_navigation").load(window.location.pathname + " #tab_navigation");
				// $("#subjects_list").load(window.location.pathname + " #subjects_list");
				// $("#tab_navigation").show('slow');
				$("#confcode_form").show('slow');
				// $("#info").show('slow');
				// AjaxContent.init({containerDiv:"#tab_navigation_container", contentDiv:"#tab_navigation_content"}).ajaxify_links("#tab_navigation ul ul li a");
    			// AjaxContent.init({containerDiv:"#tab_navigation_container", contentDiv:"#tab_navigation_content"}).ajaxify_links("#subjects_list ul li a");
			}
			else if (response == "Error")
			{
				element_notification('attach_group_field', 'Неверная группа', 'right middle', 'warn');
			}
		});
		return false;
	});
	}
}