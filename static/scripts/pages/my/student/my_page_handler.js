﻿$(document).ready(function() {
    import_confcode_handler();
	import_attach_group_handler();
    AjaxContent.init({containerDiv:"#tab_navigation_container", contentDiv:"#tab_navigation_content"}).ajaxify_links("#tab_navigation_link a");
    AjaxContent.init({containerDiv:"#tab_navigation_container", contentDiv:"#tab_navigation_content"}).ajaxify_links("#subjects_list ul li a");
});

function show_subjects_list()
{
	$("#tab_navigation_container").hide('fast');
	$("#subjects_list").show('slow');
}

function hide_subjects_list()
{
	$("#subjects_list").hide('fast');
	$("#tab_navigation_container").show('slow');
}