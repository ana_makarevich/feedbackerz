﻿var AjaxContent = function()
{
    var container_div = '';
    var content_div = '';
    return{
        getContent : function(url)
        {
            $.ajaxPrefilter(function( options, originalOptions, jqXHR ) {
                options.async = true;
            });
        
            $(container_div).animate({ opacity:0 }, // Прозрачность на 0
                function() // Загружает контент с помощью ajax
                {
                    hide_subjects_list();
                    $(container_div).load(url + content_div, // Загружает только выбранную часть
                    function()
                    {
                        $(container_div).animate({ opacity:1 }); // Возвращает прозрачность обратно на  1
                    }
                );
            });
        },
        ajaxify_links: function(elements)
        {
            // Предотвращает нажатие на ссылку
            $(elements).click(function()
            {
                AjaxContent.getContent(this.href);
                return false;
            });
        },
        // Задает первоначальные настройки и выводит объект
        init: function(params)
        {
            container_div = params.containerDiv;
            content_div = params.contentDiv;
            return this;
        }
    }
}();