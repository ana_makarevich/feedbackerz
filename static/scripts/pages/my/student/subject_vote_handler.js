﻿$(document).ready(function() {
	$('#vote_subject_form').submit(function() {
		$.post(send_url, $('#vote_subject_form').serialize(),  function(response) {
			if (response == 'Success')
			{
				//hide_element_notification('submit_btn_wrapper');
				$('#vote_subject_form').hide('fast');
				$('#vote_result').html('Проголосовано!');
			}
			else if (response == 'Error')
			{
				//element_notification('submit_btn_wrapper', 'Непонятная ошибка', 'right middle', 'warn');
			}
		});
		return false;
	});
});
