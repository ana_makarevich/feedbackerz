﻿/*
    Скрипт, отвечающий за отправку формы на странице входа.
    В случае ошибки, пользователю будет выведено сообщение без перезагрузки страницы.
    Входной параметр - form_send_url от Django.
*/

var form_id = "login_form";
var result_id = "result_field";
var error_message = "Неверные данные";
    
$(document).ready(function()
{
	$('#' + form_id).submit(function()
    {
        var do_not_send = true;
		$.post(form_send_url, $("#" + form_id).serialize(), function(response)
        {
            if (response == "Invalid login/password combination")
			{
				$('#' + result_id).html(error_message).css('color','red').animate({'paddingLeft':'5px'}, 400).animate({'paddingLeft':'3px'}, 400);
            }
            else
            {
                do_not_send = false;
            }
		});
        if (do_not_send){ return false; };
	});
});