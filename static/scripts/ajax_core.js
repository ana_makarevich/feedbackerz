/*
    Скрипт, позволяющий динамически менять содержимое страницы, подгружая часть контента с другой страницы.
    Необходим jQuery и декларация функции on_ajax_load(), которая выполнится после загрузки данных страницы.
    
    Аяксифация ссылок выглядит так:
    AjaxContent.init({containerDiv:"#tab_navigation_container", contentDiv:"div.ajax_panel"}).ajaxify_links("a.tab_navigation_link");
*/

var AjaxContent = function()
{
    var container_div = '';
    var content_div = '';
    return {
        getContent : function(url)
        {
            $.ajaxPrefilter(function( options, originalOptions, jqXHR ) {
                options.async = true;
            });
        
            $(container_div).load(url + " " + content_div, function() {
                scroll_to(container_div);
                on_ajax_load();
            });
        },
        ajaxify_links: function(elements)
        {
            // Предотвращает нажатие на ссылку
            $(elements).click(function()
            {
                AjaxContent.getContent(this.href);
                return false;
            });
        },
        // Задает первоначальные настройки и выводит объект
        init: function(params)
        {
            container_div = params.containerDiv;
            content_div = params.contentDiv;
            return this;
        }
    }
}();