#!/usr/bin/env python
# -*- coding:utf-8 -*-

# Стандартные библиотеки
from __future__ import unicode_literals
import json, urllib2, codecs
import time, pytz
import random
import re
from datetime import date
from datetime import datetime, timedelta
from django.utils import timezone

# Локальные импорты
from .models import (CustomUser, StudentProfile, InstructorProfile, 
	Group, Faculty, Direction, Speciality, Department, Instructor,
	ActivityType, Subject, Group_Term_Subject,GTS_Instructor,
	GTSI_Lecture)

# Универсальная функция, которая возвращает либо объект, либо None
def get_or_none(model, *args, **kwargs):
    try:
        return model.objects.get(*args, **kwargs)
    except model.DoesNotExist:
        return None

# Возвращает список факультетов, добытых из файла json, находящегося по указанно ссылке
# Ссылка передается в формате string
def get_faculties(json_link):
	"""
	Return faculties list as list of dictionaries with keys
	"""
	fp = urllib2.urlopen(json_link)
	all_objects = json.load((codecs.getreader('cp1251')(fp)))
	faculties = []
	faculties_list = all_objects['university']['faculties']
	return faculties_list

# Записывает все факультеты в базу данных
def load_faculties(outdated = False):
	"""
	Extract faculties dictionaries list and add faculties 
	that are not in the database

	Tables affected: Faculty
	"""
	if outdated:
		faculties_list = get_faculties('http://cist.nure.ua/ias/app/tt/P_API_GROUP_JSON')
		for faculty in faculties_list:
			get_or_create_faculty(faculty, False)
			
def load_directions(outdated = False):
	"""
	Extract all faculties dictionaries list, add faculties
	that are not in the database, then add directions
	for each faculty if they are not in the database

	Tables affected: Faculty, Direction
	"""
	if outdated:
		faculties_list = get_faculties('http://cist.nure.ua/ias/app/tt/P_API_GROUP_JSON')
		for faculty in faculties_list:
			f = get_or_create_faculty(faculty, True)
			directions_list = faculty['directions']
			for direction in directions_list:
				get_or_create_direction(direction, f, False)


def load_specialities(outdated = False):
	"""
	Extract all faculties dictionaries list, add faculties
	that are not in the database, then add directions
	for each faculty if they are not in the database, 
	then add specialities for each directions that
	are not in the database

	Tables affected: Faculty, Direction, Speciality
	"""
	if outdated:
		faculties_list = get_faculties('http://cist.nure.ua/ias/app/tt/P_API_GROUP_JSON')
		for faculty in faculties_list:
			f = get_or_create_faculty(faculty, True)
			directions_list = faculty['directions']
			for direction in directions_list:
				d = get_or_create_direction(direction, f, True)
				specialities_list = direction['specialities']
				for speciality in specialities_list:
					get_or_create_speciality(speciality, d, False)

def get_or_create_speciality(speciality, d, return_object = True):
	"""
	Check if the passed speciality exists in the database
	and if it doesn't then create one, save and return
	this object if return_object is True
	"""
	if not Speciality.objects.filter(speciality_id = speciality['id']).exists():
		s = Speciality(
			direction = d,
			speciality_id = speciality['id'],
			short_name = speciality['full_name'],
			full_name = speciality['short_name']
			)
		s.save()
		if return_object:
			return s
	else:
		if return_object:
			s = Speciality.objects.get(speciality_id = speciality['id'])
			return s

def load_groups(outdated = False):
	"""
	Extract all faculties dictionaries list, add faculties
	that are not in the database, then add directions
	for each faculty if they are not in the database, 
	then add specialities for each directions that
	are not in the database, then add groups for each 
	speciality if they are not in the database

	Tables affected: Faculty, Direction, Speciality, Group
	"""

	if outdated:
		faculties_list = get_faculties('http://cist.nure.ua/ias/app/tt/P_API_GROUP_JSON')
		for faculty in faculties_list:
			f = get_or_create_faculty(faculty, True)
			directions_list = faculty['directions']
			for direction in directions_list:	
				d = get_or_create_direction(direction, f, True)	
				specialities_list = direction['specialities']
				for speciality in specialities_list:	
					s = get_or_create_speciality(speciality, d, True)		
					groups_list = speciality['groups']
					for group in groups_list:
						if not Group.objects.filter(group_id = group['id']).exists():
							confirmation_code = random.randint(10000,99999)
							year = (int) ('20'+re.search('\d{2}', group['name']).group())
							if year < 2000 or year > timezone.now().year:
								year = None
							g = Group(
								speciality = s,
								group_id = group['id'],
								name = group['name'],
								confirmation_code = confirmation_code,
								curator = None,
								year = year,
								schedule_update_date = None
								)
							g.save()




def load_departments(outdated = False):
	"""
	Extract all faculties dictionaries lists, add faculties
	that are not in the databse, then add departments for 
	each faculty if they are not in the database

	Tables affected: Faculty, Department
	"""
	if outdated:
		faculties_list = get_faculties('http://cist.nure.ua/ias/app/tt/P_API_PODR_JSON')
		for faculty in faculties_list:	
			f = get_or_create_faculty(faculty, True)
			departments = faculty['departments']
			for department in departments:
				if not Department.objects.filter(department_id = department['id']).exists():
					d = Department(
						faculty = f,
						department_id = department['id'],
						short_name = department['short_name'],
						full_name = department['full_name']
						)
					d.save()

def load_instructors(outdated = False):
	"""
	Extract all faculties dictionaries lists, add faculties
	that are not in the databse, then add departments for 
	each faculty if they are not in the databse, then 
	add instructors for each department if they are not 
	in the database, then add department to instructor if it's not
	linked to her yet

	Tables affected: Faculty, Department, Instructor
	"""
	if outdated:
		faculties_list = get_faculties('http://cist.nure.ua/ias/app/tt/P_API_PODR_JSON')
		for faculty in faculties_list:				
			f = get_or_create_faculty(faculty, True)
			departments = faculty['departments']
			for department in departments:
				if not Department.objects.filter(department_id = department['id']).exists():
					d = Department(
						faculty = f,
						department_id = department['id'],
						short_name = department['short_name'],
						full_name = department['full_name']
						)
					d.save()
				else:
					d = Department.objects.get(department_id = department['id'])
				instructors = department['teachers']
				for instructor in instructors:
					if not Instructor.objects.filter(cist_id = instructor['id']).exists():
						fio = split_instructor(instructor['full_name'])
						last_name = fio[0]
						first_name = fio[1]
						middle_name = fio[2]
						t = Instructor(
							first_name = first_name,
							last_name = last_name,
							middle_name = middle_name,
							cist_id = instructor['id'])
						t.save()
						t.departments.add(d)
					else:
						t = Instructor.objects.get(cist_id = instructor['id'])
						if not t.departments.filter(department_id = d.department_id).exists():
							t.departments.add(d)

def get_or_create_faculty(faculty, return_object = True):
	"""
	Check if the passed faculty exists in the database
	and if it doesn't then create one, save and return this
	object if return_object is True

	"""
	if not Faculty.objects.filter(faculty_id=faculty['id']).exists():
		f = Faculty(
			faculty_id = faculty['id'],
			short_name = faculty['full_name'],
			full_name = faculty['short_name'])
		f.save()
		if return_object:
			return f
	else:
		if return_object:
			f = Faculty.objects.get(faculty_id = faculty['id'])
			return f

def get_or_create_direction(direction, f, return_object = True):
	"""
	Check if the passed direction exists in the database
	and if it doesn't then create one, save and return
	this object if return_object is True
	"""
	if not Direction.objects.filter(direction_id=direction['id']).exists():
		d = Direction(
			faculty =f,
			direction_id = direction['id'],
			short_name = direction['full_name'],
			full_name=direction['short_name'])
		d.save()
		if return_object:
			return d
	else:
		if return_object:
			d = Direction.objects.get(direction_id = direction['id'])
			return d



def split_instructor(fio_string):
	"""
	Split the string with the full instructor name into
	last_name, first_name and middle_name and return them
	as a tuple
	"""
	fio = fio_string.split()
	l = len(fio)
	if l==1:
		last_name=fio[0]
		first_name=""
		middle_name=""
	if l==2:
		last_name=fio[0]
		first_name=fio[1]	
		middle_name=""
	if l==3:
		last_name=fio[0]
		first_name=fio[1]	
		middle_name=fio[2]
	if l>3:
		last_name=fio[0]
		first_name=fio[1:l-2]
		middle_name=fio[l-1]
	return (last_name, first_name, middle_name)

def load_types(group_id):
	"""
	Loads types of activities (such as lectures, labs, etc) with their CIST ids
	"""
	fp = urllib2.urlopen('http://cist.nure.ua/ias/app/tt/P_API_EVENT_JSON?timetable_id='+group_id)
	all_objects = json.load((codecs.getreader('cp1251')(fp)))
	types = all_objects['types']
	for t in types:
		if not ActivityType.objects.filter(code = t['id']).exists():
			at = ActivityType(
				code = t['id'],
				activty_name = t['full_name'],
				short_name = t['short_name'],
				activity_type = t['type']
				)
			at.save()
		


def load_subjects(group_id):
	"""
	Load subjects for the group 
	"""
	fp = urllib2.urlopen('http://cist.nure.ua/ias/app/tt/P_API_EVENT_JSON?timetable_id='+group_id)
	all_objects = json.load((codecs.getreader('cp1251')(fp)))	
	subjects = all_objects['subjects']
	for subject in subjects:
		if not Subject.objects.filter(subject_id = subject['id']).exists():
			s = Subject(
				subject_id = subject['id'],
				full_name = subject['title'],
				short_name = subject['brief']
				)
			s.save()

def get_term(group_name):
	"""
	Get the current term for the given group
	"""
	year = int('20'+re.search('\d{2}',group_name).group(0))
	today = date.today()
	term = (today.year - year)*2
	# if 9 <= month <= 12 -> even semester
	# if 1 <= month <= 7 -> odd semester			
	if (today.month >= 9):				
		term+=1
	return term


def load_gts(group_id):
	"""
	Load gts objects to the database for the given group. If the subject is not
	in the subjects table, then the subject is added. 

	Tables affects: Subject, Group_Term_Subject
	"""
	fp = urllib2.urlopen('http://cist.nure.ua/ias/app/tt/P_API_EVENT_JSON?timetable_id='+group_id)
	all_objects = json.load((codecs.getreader('cp1251')(fp)))	
	subjects = all_objects['subjects']
	for subject in subjects:
		if not Subject.objects.filter(subject_id = subject['id']).exists():
			s = Subject(
				subject_id = subject['id'],
				full_name = subject['title'],
				short_name = subject['brief']
				)
			s.save()
		else:
			s = Subject.objects.get(subject_id = subject['id'])
		if not Group_Term_Subject.objects.filter(subject__subject_id = subject['id'], group__group_id = group_id):
			g = Group.objects.get(group_id=group_id)
			term = get_term(g.name)			
			gts = Group_Term_Subject(
				group = g,
				term = term,
				subject = s
				)
			gts.save()
			
def get_instructor(teachers):
	"""
	Get the instructor for the subject. This function handles the case when
	there are multiple instructors for one subject of None. In a case when
	there are multiple instructors the first one is chosen. If no instructors
	found, then None is returned. 
	"""
	if len(teachers) > 0:			
		instructor_id = teachers[0]
		if Instructor.objects.filter(cist_id=instructor_id).exists():				
			instructor = Instructor.objects.get(cist_id=instructor_id)
		else:
			instructor = None
	else:
		instructor = None
	return instructor

def load_gtsi(group_id):
	"""
	Load Group_Term_Subject_Instructor objects to the database. 

	Tables affected: GTS_Instructor
	"""
	fp = urllib2.urlopen('http://cist.nure.ua/ias/app/tt/P_API_EVENT_JSON?timetable_id='+group_id)
	all_objects = json.load((codecs.getreader('cp1251')(fp)))		
	events = all_objects['events']
	for event in events:		
		teachers = event["teachers"]
		instructor = get_instructor(teachers)		
		g = Group.objects.get(group_id = group_id)
		if Group_Term_Subject.objects.filter(group=g, subject__subject_id = event['subject_id'], term = get_term(g.name)).exists():
			gts = Group_Term_Subject.objects.get(group=g, subject__subject_id = event['subject_id'], term = get_term(g.name))
			if ActivityType.objects.filter(code = event["type"]).exists():
				activity_type = ActivityType.objects.get(code = event["type"])
				if not GTS_Instructor.objects.filter(gts=gts, instructor = instructor, activity_type = activity_type).exists():
					gtsi = GTS_Instructor(
						gts = gts,
						instructor = instructor,
						activity_type = activity_type
						)
					gtsi.save()


def load_lectures(group_id):
	"""
	Load lectures objects to the databse. 

	Tables affected: GTSI_Lecture

	Important note: this function updeated the 'schedule_update_date' in the
	group object. This is important to balance the frequncy of updates. 
	"""
	# Define the time frame
	tdback = timedelta(days=-10)
	tdfwd = timedelta(days=5)
	today = datetime.today()
	t_from = today+tdback
	t_to = today+tdfwd
	time_from = str(int(time.mktime(t_from.timetuple())))
	time_to = str(int(time.mktime(t_to.timetuple())))
	fp = urllib2.urlopen('http://cist.nure.ua/ias/app/tt/P_API_EVENT_JSON?timetable_id='+group_id+"&time_from="+time_from+"&time_to="+time_to)
	all_objects = json.load((codecs.getreader('cp1251')(fp)))		
	events = all_objects['events']
	for event in events:		
		instructor = get_instructor(event["teachers"]) #instructor id
		group_ids = event['groups']
		for groupid in group_ids:
			g = get_or_none(Group, group_id = groupid)
			if not g is None:
				#load_subjects(group_id)
				#load_gts(group_id)
				#load_types(group_id)
				#load_gtsi(group_id)
				g.schedule_update_date = timezone.now().date()
				g.save()
				subject = Subject.objects.get(subject_id = event['subject_id'])
				gts, created = Group_Term_Subject.objects.get_or_create(group = g, subject = subject, term = get_term(g.name))
				if not gts is None:
					if ActivityType.objects.filter(code = event["type"]).exists():
						activity_type = ActivityType.objects.get(code = event["type"])						
						gtsi,created = GTS_Instructor.objects.get_or_create(gts=gts, instructor = instructor, activity_type = activity_type)
						if not gtsi is None:						
							lecture_start_time = datetime.fromtimestamp(int(event["start_time"])).date()
							number_pair = event["number_pair"]						
							if not GTSI_Lecture.objects.filter(gtsi = gtsi,
								lecture_start_time = lecture_start_time).exists():							
								gtsil = GTSI_Lecture(
									gtsi = gtsi,
									lecture_start_time = lecture_start_time,
									lecture_name = None,
									number_pair = number_pair)
								gtsil.save()


def load_group_data(group_id):
	load_subjects(group_id)
	load_gts(group_id)
	load_types(group_id)
	load_gtsi(group_id)
	load_lectures(group_id)