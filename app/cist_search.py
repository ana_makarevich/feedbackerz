#!/usr/bin/env python
# -*- coding:utf-8 -*-


# Стандартные библиотеки
from __future__ import unicode_literals
import json, urllib2, codecs
import re

# Библиотеки Джанго
from django.utils.encoding import smart_unicode

def get_faculties():
	fp = urllib2.urlopen('http://cist.nure.ua/ias/app/tt/P_API_GROUP_JSON')
	all_objects = json.load((codecs.getreader('cp1251')(fp)))
	faculties = []
	faculties_list = all_objects['university']['faculties']
	return faculties_list

def collect_faculties():
	# Достанем весь джейсон	
	faculties_list = get_faculties()
	for faculty in faculties_list:
		faculties.append(faculty['full_name'])
	return faculties

def collect_specialities():	
	specialities = []
	faculties_list = get_faculties()
	for faculty in faculties_list:
		directions_list = faculty['directions']
		for direction in directions_list:
			d_name = direction['full_name']
			if d_name not in specialities and d_name != u'Інші':
				specialities.append(direction['full_name'])
			specialities_list = direction['specialities']
			for speciality in specialities_list:
				s_name = speciality['full_name']
				if s_name not in specialities and d_name != u'Інші':
					specialities.append(speciality['full_name'])
	specialities.sort()					
	return specialities

def collect_groups():	
	groups_ukr = []
	groups_latin = []
	faculties_list = get_faculties()
	for faculty in faculties_list:
		directions_list = faculty['directions']
		for direction in directions_list:			
			specialities_list = direction['specialities']
			for speciality in specialities_list:
				groups_list = speciality['groups']
				for group in groups_list:
					g_name = group['name']									
					processed = re.match(r"[u'АБВГДЕЖЗИІКЛМНОПРСТУФХЦЧШЩЫЭЮЯ]+",g_name)						
					if processed is not None:
						processed = processed.group()
						if processed not in groups_ukr:
							groups_ukr.append(processed)
					else:
						processed = re.match(r"[A-Z]+",g_name)
						if processed is not None:
							processed = processed.group()
							if processed not in groups_latin:
								groups_latin.append(processed)
	groups_latin.sort()							
	groups_ukr.sort()					
	return (groups_ukr+groups_latin)



