#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
Definition of views.
"""

# Стандартные библиотеки
from __future__ import unicode_literals
import time
import hashlib, datetime, random

# Библиотеки Джанго
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, HttpRequest
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.core.mail import send_mail
from django.utils import timezone

# Локальные импорты 
#from .models import User, StudentProfile, InstructorProfile, UserProfile, Group, Subject, Lecture, Instructor
#from .forms import UserForm, InstructorProfileForm, StudentProfileForm
from .models import (CustomUser, StudentProfile, InstructorProfile, 
	Group, ProfileType, Group_Term_Subject)
from .forms import CustomUserForm, InstructorProfileForm, StudentProfileForm
import cist_search, refresh_groups

def save_student(user_form, student_form):
	user = user_form.save()
	user.set_password(user.password)  

	# Делаем пользователя неактивным до тех пор, пока он не подтвердить свой адрес почты
	user.is_active = False
	user.status_confirmed = False

	salt = hashlib.sha1(str(random.random())).hexdigest()[:5]            
	activation_key = hashlib.sha1(salt+user.email).hexdigest()            
	key_expires = datetime.datetime.today() + datetime.timedelta(2)
	user.activation_key = activation_key
	user.key_expires = key_expires	
	email_subject = u'Подтвердите регистрацию в Feedbacker'
	message = "Привет, %s! Спасибо за регистрацию. Нажмите на ссылку, чтобы активировать свою учетную запись http://fdbkr.azurewebsites.net/confirm/%s" % (user.first_name, activation_key)	
	send_mail(email_subject, message, 'noreply@fdbkr.azurewebsites.net', [user.email], fail_silently=False)


	profile_type, created = ProfileType.objects.get_or_create(code = u"1", name = u"Студент", short_name = u"СТ")
	if created:
		profile_type.save()
	user.profile_type = profile_type
	# Сохраняем нашего юзера
	user.save()

	# Добываем данные для профиля юзера, но пока не сохраняем их
	profile = student_form.save(commit = False)

	# Связываем юзера с его профилем
	profile.user = user
		
	# По умолчанию пользователь - обычный студент - не староста
	profile.is_prefect = False

	# Извращаемся и добавляем группу пользователя
	# Если группа существует, привязываем ее к пользователю, если нет, то это поле у юзера остается нулевым 	
	group_name = u'{0}-{1}-{2}'.format(
		(student_form.cleaned_data['faculty_name']+student_form.cleaned_data['study_mode']),
		str(student_form.cleaned_data['year_started']),
		str(student_form.cleaned_data['group_number']))
	if Group.objects.filter(name=group_name).exists():
		group = Group.objects.get(name=group_name)
		profile.group = group
		if not Group_Term_Subject.objects.filter(group = group).exists():
			group_id = group.group_id
			refresh_groups.load_group_data(group_id)			
	else:
		profile.group = None
	profile.save()

	
def save_instructor(user_form, instructor_form):
	user = user_form.save()
	user.set_password(user.password)
	user.is_active = True
	user.status_confirmed = False	
	profile_type, created = ProfileType.objects.get_or_create(code = u"2", name = u"Преподаватель", short_name = u"ПР")
	if created:
		profile_type.save()
	user.profile_type = profile_type
	user.save()

	profile = instructor_form.save(commit = False)
            
	profile.user = user    
            
	profile.save()
	send_mail('Subject', 'Succesfully registered with feedbackerz!', 'noreply@fdbkr.azurewebsites.com', [user.email], fail_silently=False)