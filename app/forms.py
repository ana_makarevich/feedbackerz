#!/usr/bin/env python
# -*- coding:utf-8 -*-

# Стандартные библиотеки
from __future__ import unicode_literals
import re
import datetime

# Библиотеки Джанго
#from django.contrib.auth.models import User
from django import forms
from django.contrib.auth.forms import AuthenticationForm
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ValidationError
from django.utils import timezone

# Импорт каптчи
#from nocaptcha_recaptcha.fields import NoReCaptchaField

# Локальные импорты 
from .models import CustomUser, InstructorProfile, StudentProfile,Group, Criteria, GTSIL_Criteria_User_Vote, StudentRatingData
import cist_search 

groups = [u'АКІТ', u'АМЗІ', u'АРТ', u'АТ', u'АУТП', u'БІКС', u'БДІР',
u'БМІ', u'ВПС', u'ЕАБС', u'ЕК', u'ЕКИ', u'ЕПП', u'ЗІОД', u'ІКТ', u'ІМЗ', 
u'ІНФ', u'ІПЗ', u'ІТБМ', u'ІТМРТ', u'ІТП', u'ІУСТ',u'КІ', u'КІЗ', u'КІТПВ',
u'КН', u'КС', u'КСМ', u'КСУА', u'КТРС', u'КТСВПВ', u'ЛОЕТ', u'МВТ',
u'МСС', u'ПІ', u'ПІЗ', u'ПЗАС', u'ПЗС', u'ПМ', u'РЕА', u'РЕАЗ', u'РЕЗ', u'РПСК',
u'РТ', u'РТЗ', u'СІ', u'СА', u'САУ', u'СКС', u'СП', u'СПР', u'СТЗІ', u'СШІ', u'ТДВ',
u'ТЕМВ', u'ТК', u'ТКЗ', u'ТСМ', u'УІБ', u'УП', u'УФЕБ', u'ФТОІ', u'ЯСС', u'BME',
u'BMI', u'CE', u'CSN', u'EC', u'ICSS', u'КІУКІ', u'КБІКС']
#groups = cist_search.collect_groups()

# Глобально об'явленные регулярные выражения. Что бы не потерять.
name_regex = u'^(([A-Za-z]+)([ ]?([ ]?[\-|\.|\'|\`][ ]?)?)([A-Za-z]+))$|^(([А-ЯА-яІіЇїЄєЁё]+)([ ]?([ ]?[\-|\.|\'|\`][ ]?)?)([А-ЯА-яІіЇїЄєЁё]+))$'

scard_id_regex = u'^(ХА|XA)?[ ]?[№]?[ ]?(\d{8})$'
username_regex = u'^([A-Z]|[a-z]|[0-9]|\.|_|\-){3,30}$'
email_regex = u'^([A-Z]|[a-z]|[0-9]|\.|_|\-)+@([A-Z]|[a-z]|[0-9]|\.|_|\-)+\.([A-Z]|[a-z]){2,4}$'
phone_number_regex = u'^((((00|\+)?38[\- ]?)?\(?\d{3}\)?)|(((00|\+)380[\- ]?)?\(?\d{2}\)?))([\- ]?)(\d{3}[\- ]?)(\d{2}[\- ]?)(\d{2})$'

class CodeConfirmationForm(forms.Form):
	code = forms.CharField(label = u"Код подтверждения", max_length = 10)	


class ChooseGroupForm(forms.Form):	
	# Выбор группы (года поступления)	
	group_choices = [(x, x) for x in groups]
	faculty_name = forms.ChoiceField(choices = group_choices)
	# Выбор типа группы (ускоренники, заочники и т.п.)	
	study_modes = [u'',u'м', u'і',u'и', u'з', u'у', u'mi', u'пз', u'мд', u'n', u'з', u'зс', u'за', u'зм', u'зу', u'ми', u'мі', u'с', u'м']
	study_mode_choices = [(x,x) for x in study_modes]	
	study_mode = forms.ChoiceField(choices = study_mode_choices, required=False, initial=u'')	
	# Выбор года поступления
	this_year = timezone.now().year
	year_started = forms.ChoiceField(initial = u'14', choices =[(str(x)[2:4], str(x)[2:4]) for x in range(2004, this_year+1)])	
	# Выбор номера группы
	group_number = forms.ChoiceField(initial=1, choices=[(x, x) for x in range(1, 12)])	

class Criteria(forms.ModelForm):

	class Meta:
		model = GTSIL_Criteria_User_Vote
		fields = ('vote',)


class CustomUserForm(forms.ModelForm):
	password = forms.CharField(widget=forms.PasswordInput(), label = u'Пароль')
	password_validator = forms.CharField(widget=forms.PasswordInput(), label = u'Повторите ввод пароля')
	
	class Meta:
		model = CustomUser
		fields = ('first_name', 'last_name', 'username', 'email', 'password', 'password_validator')
		
		labels ={
		'first_name':_(u'Имя'),
		'last_name':_(u'Фамилия'),
		'username':_(u'Имя пользователя'),
		'email': _(u'E-mail'),
		}
		help_texts = {
		'username': (u'Обязательное поле. Максимум - 30 символов. Допустимы только латинские буквы, числа, а также символы @/./+/-/_'),		 
		}		
	
	def clean(self):
		#Проверка допустимости имени.
		if not re.match(name_regex, self.data['first_name']):
			raise ValidationError({'first_name': ['Имя может содержать только буквы и дефисы("-")',]})
		#Проверка допустимости фамилии.
		if not re.match(name_regex, self.data['last_name']):
			raise ValidationError({'last_name': ['Фамилия может содержать только буквы и дефисы!("-")',]})
		#Проверка допустимости имени пользователя.
		if not (re.match(username_regex, self.data['username']) or re.match(email_regex, self.data['username'])):
			raise ValidationError({'username': ['Имя пользователя может содержать только символы латинского (английского) алфавита и символы @,+,-,_',]})
		#Проверка допустимости email.
		if not re.match(email_regex, self.data['email']):
			raise ValidationError({'email': ['Адрес электронной почты задан в неверном формате',]})
		#Проверка допустимости полей пароля.
		if (self.data['password'] != self.data['password_validator']):
			raise ValidationError({'password_validator': [u'Пароли не совпадают!',]})
		if CustomUser.objects.filter(username=self.data['username']).exists():
			raise ValidationError({'username': [u'Пользователь с таким именем уже существует!',]})	
		if CustomUser.objects.filter(email=self.data['email']).exists():
			raise ValidationError({'email': [u'Такой адрес почты уже есть!',]})
		return self.cleaned_data


class StudentProfileForm(forms.ModelForm):	
	# Выбор группы (года поступления)	
	group_choices = [(x, x) for x in groups]
	faculty_name = forms.ChoiceField(choices = group_choices)
	# Выбор типа группы (ускоренники, заочники и т.п.)	
	study_modes = [u'',u'м', u'і', u'и', u'з', u'у', u'mi', u'пз', u'мд', u'n', u'з', u'зс', u'за', u'зм', u'зу', u'ми', u'мі', u'с', u'м']
	study_mode_choices = [(x,x) for x in study_modes]	
	study_mode = forms.ChoiceField(choices = study_mode_choices, required=False, initial=u'')	
	# Выбор года поступления
	this_year = timezone.now().year
	year_started = forms.ChoiceField(initial = u'14', choices =[(str(x)[2:4], str(x)[2:4]) for x in range(2004, this_year+1)])	
	# Выбор номера группы
	group_number = forms.ChoiceField(initial=1, choices=[(x, x) for x in range(1,12)])	

	class Meta:
		model = StudentProfile
		fields = ('year_started', 'faculty_name', 'group_number', 'study_mode')
		labels = {		
		'year_started':_(u'Год поступления'),
		'faculty_name':_(u'Поток'),
		'group_number':_(u'Номер группы'),
		'study_mode':_(u''),		
		}
	
	def clean(self):
		#Проверка допустимости номера студенческого.
		"""
		if (not re.match(scard_id_regex, self.data['student_id'])) and (self.data['student_id'] != ""):
			raise ValidationError({'student_id': [u'Номер студенческого должен содержать 8 цифр!',]})
		"""
		#if StudentProfile.objects.filter(student_id=self.data['student_id']).exists():
		#	raise ValidationError({'student_id': [u'Пользователь с таким номером студенческого уже существует. Обратитетесь к администратору',]})		
		"""	
		group_name = u'{0}-{1}-{2}'.format((self.data['faculty_name']+self.data['study_mode']), str(self.data['year_started']), str(self.data['group_number']))
		if not Group.objects.filter(name=group_name).exists():			
			raise ValidationError({'faculty_name': [u'Группы {0} нет в базе. Обратитетесь к администратору'.format(group_name),]})
		"""
		return self.cleaned_data

class InstructorProfileForm(forms.ModelForm):
	class Meta:
		model = InstructorProfile
		fields = ('middle_name',)
		labels = {
		'middle_name':_(u'Отчество'),
		
		}
	
	def clean(self):
		#Проверка допустимости отчества.
		if (not re.match(name_regex, self.data['middle_name'])) and (self.data['middle_name'] != ""):
			raise ValidationError({'middle_name': [u'Отчество должно содержать только буквы',]})
		#Проверка допустимости номера мобильного телефона.

		return self.cleaned_data

class StudentGrantDataForm(forms.ModelForm):	
	class Meta:
		model = StudentRatingData
		fields = ('last_name', 'first_name', 'middle_name', 'faculty_txt', 'department_txt',
			'group_txt', 'gpa', 'patents', 'competitions', 'articles_int', 'articles_nat',
			'presentation_int', 'presentation_nat', 'sport', 'scopus',)
		widgets = {
		'last_name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Фамилия', 'required':''}),
		'first_name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Имя', 'required':''}),
		'middle_name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Отчество', 'required':''}),
		'faculty_txt': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Факультет', 'required':''}),
		'department_txt': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Кафедра', 'required':''}),
		'group_txt': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Группа', 'required':''}),
		'gpa': forms.NumberInput(attrs={'class': 'form-control', 'placeholder': 'Средний балл (0-100)'}),
		#'english': forms.NumberInput(attrs={'class': 'form-control', 'placeholder': 'Количество сертификатов'}),
		'patents': forms.NumberInput(attrs={'class': 'form-control', 'placeholder': 'Количество экземпляров'}),
		'competitions': forms.NumberInput(attrs={'class': 'form-control', 'placeholder': 'Количество событий'}),
		'articles_int': forms.NumberInput(attrs={'class': 'form-control', 'placeholder': 'Количество статей'}),
		'articles_nat': forms.NumberInput(attrs={'class': 'form-control', 'placeholder': 'Количество статей'}),
		'presentation_int': forms.NumberInput(attrs={'class': 'form-control', 'placeholder': 'Количество докладов'}),
		'presentation_nat': forms.NumberInput(attrs={'class': 'form-control', 'placeholder': 'Количество докладов'}),
		'sport': forms.NumberInput(attrs={'class': 'form-control', 'placeholder': 'Количество мероприятий'}),
		'scopus': forms.NumberInput(attrs={'class': 'form-control', 'placeholder': 'Количество публикаций'}),
		}
		labels = {
		'last_name':_(u'Фамилия студента:'),
		'first_name':_(u'Имя студента:'),
		'middle_name':_(u'Отчество студента:'),
		'faculty_txt':_(u'Факультет:'),
		'department_txt':_(u'Кафедра:'),
		'group_txt':_(u'Группа:'),
		'gpa':_(u'Средний балл сданных экзаменов и зачетов (100 max):'),
		#'english':_(u'Знание иностранных языков, подтвержденное сертификатами IELTS, TOEFL (50k):'),
		'patents':_(u'Программные продукты; устройства и макеты, патенты на изобретения и полезные модели (20k)'),
		'competitions':_(u'Призеры конкурсов, олимпиад и выставок, подтвержденные дипломами (10k)'),
		'articles_int':_(u'Журнальные статьи зарубежные (20k)'),
		'articles_nat':_(u'Журнальные статьи национальные (10k)'),
		'presentation_int':_(u'Доклады на конференциях и семинарах зарубежные (15k)'),
		'presentation_nat':_(u'Доклады на конференциях и семинарах национальные (5k)'),
		'sport':_(u'Волонтерские, спортивные и культурные мероприятия, подтвержденные руководителями факультетов (5k)'),
		'scopus':_(u'Наукометрия в Scopus, WoS (20k)'),
		}		
