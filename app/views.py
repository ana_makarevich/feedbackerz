#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
Definition of views.
"""

# Стандартные библиотеки
from __future__ import unicode_literals
import time
import hashlib, random, json
import re 
from datetime import date, datetime, timedelta


# Библиотеки Джанго
from django.shortcuts import render
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponse, HttpResponseRedirect, HttpRequest, JsonResponse
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.core.mail import send_mail
from django.utils import timezone, translation
from django.views.generic import View, ListView, CreateView, DetailView
from django.views.generic.edit import DeleteView, UpdateView, FormView, FormMixin
from django.utils.decorators import method_decorator
from django.core.urlresolvers import reverse
from django.db.models import Avg, Sum, Count, Max
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.csrf import ensure_csrf_cookie
from django.utils.translation import ugettext_lazy as _
from django.db.models import Q
# Локальные импорты
from .forms import (CustomUserForm, StudentProfileForm, InstructorProfileForm,
    CodeConfirmationForm, ChooseGroupForm, StudentGrantDataForm)
#from .models import User, StudentProfile, InstructorProfile, UserProfile, Group, Subject, Lecture, Instructor
from .models import (CustomUser, StudentProfile, InstructorProfile, ProfileType,
    Group, Instructor,
    Subject, Group_Term_Subject, GTS_Instructor, GTSI_Lecture,
    Announcement, Deadline, Criteria, GTSIL_Criteria_User_Vote,
    GTSI_Criteria_User_Vote, GTSIL_Feedback, Constants, 
    ActivityType,  AfterLectureSurvey, 
    AfterLectureQuestion, AfterLectureChoice, AfterLectureStudentResult, 
    AfterLectureStudentResults, StudentRatingData,
    GTSIL_Result, GTSIL_Results_Distribution, GTSI_Feedback)
from news.models import NewsPiece
import cist_search, refresh_groups, refresh_instructor
from write_to_db import save_student, save_instructor

group_choices = [u'АКІТ', u'АМЗІ', u'АРТ', u'АТ', u'АУТП', u'БІКС', u'БДІР',
u'БМІ', u'ВПС', u'ЕАБС', u'ЕК', u'ЕКИ', u'ЕПП', u'ЗІОД', u'ІКТ', u'ІМЗ', 
u'ІНФ', u'ІПЗ', u'ІТБМ', u'ІТМРТ', u'ІТП', u'ІУСТ',u'КІ', u'КІЗ', u'КІТПВ',
u'КН', u'КС', u'КСМ', u'КСУА', u'КТРС', u'КТСВПВ', u'ЛОЕТ', u'МВТ',
u'МСС', u'ПІ', u'ПІЗ', u'ПЗАС', u'ПЗС', u'ПМ', u'РЕА', u'РЕАЗ', u'РЕЗ', u'РПСК',
u'РТ', u'РТЗ', u'СІ', u'СА', u'САУ', u'СКС', u'СП', u'СПР', u'СТЗІ', u'СШІ', u'ТДВ',
u'ТЕМВ', u'ТК', u'ТКЗ', u'ТСМ', u'УІБ', u'УП', u'УФЕБ', u'ФТОІ', u'ЯСС', u'BME',
u'BMI', u'CE', u'CSN', u'EC', u'ICSS']



class Index(View):
    def get(self, request):
        """Renders the home page."""            
        request.encoding = 'utf-8'
        context = RequestContext(request)    
        news = NewsPiece.objects.filter(in_rotation = True)
        pr = news.aggregate(Max('priority'))
        main = news.filter(priority=pr['priority__max'])[0]
        news = news.exclude(id = main.id)
        print news
        print pr['priority__max']
        print main
        context_dict = {}     
        context_dict['news'] = news
        context_dict['main'] = main


        """ 
        objs = GTSIL_Criteria_User_Vote.objects.exclude(criteria = None)
        for o in objs:
            if GTSIL_Result.objects.filter(lecture = o.gtsil, criteria = o.criteria).exists():
                to_modifty = GTSIL_Result.objects.get(lecture = o.gtsil, criteria =  o.criteria)
                to_modifty.sum_votes = to_modifty.sum_votes + o.vote 
                to_modifty.votes = to_modifty.votes + 1
                to_modifty.save()
            else:
                to_create = GTSIL_Result(lecture = o.gtsil,
                    criteria = o.criteria,
                    group = o.gtsil.gtsi.gts.group,
                    term = o.gtsil.gtsi.gts.term,
                    subject = o.gtsil.gtsi.gts.subject,
                    instructor = o.gtsil.gtsi.instructor,
                    sum_votes = o.vote,
                    votes = 1,
                    average = o.vote,
                    voters = objs.filter(gtsil=o.gtsil).values_list('student', flat=True).distinct().count()
                    )
                to_create.save()
        objs = GTSIL_Criteria_User_Vote.objects.exclude(criteria = None)
        for o in objs:
            ones = 1 if o.vote == 1 else 0
            twos = 2 if o.vote == 2 else 0 
            threes = 3 if o.vote == 3 else 0
            fours = 4 if o.vote == 4 else 0
            fives = 5 if o.vote == 5 else 0
            if GTSIL_Results_Distribution.objects.filter(lecture = o.gtsil, criteria = o.criteria).exists():
                to_modifty = GTSIL_Results_Distribution.objects.get(lecture = o.gtsil, criteria =  o.criteria)
                to_modifty.fully_disagree_count += ones
                to_modifty.disagree_count += twos
                to_modifty.not_sure_count += threes
                to_modifty.agree_count += fours
                to_modifty.fully_agree_count += fives
                to_modifty.save()
            else:
                to_create = GTSIL_Results_Distribution(lecture = o.gtsil,
                    criteria = o.criteria,
                    group = o.gtsil.gtsi.gts.group,
                    term = o.gtsil.gtsi.gts.term,
                    subject = o.gtsil.gtsi.gts.subject,
                    instructor = o.gtsil.gtsi.instructor,
                    fully_disagree_count = ones,
                    disagree_count = twos,
                    not_sure_count = threes,
                    agree_count = fours,
                    fully_agree_count = fives,
                    voters = objs.filter(gtsil=o.gtsil, criteria = o.criteria).values_list('student', flat=True).distinct().count()
                    )
                to_create.save()
        """
        #integral = grant_data.gpa + grant_data.english*50 + grant_data.patents*20 + grant_data.competitions*10 + grant_data.articles_int*20 + grant_data.articles_nat*10 + grant_data.presentation_int*15 + grant_data.presentation_nat*5 + grant_data.sport*5 + grant_data.scopus*20  
        #groups = cist_search.collect_groups(True)
        #refresh_groups.load_groups(True)
        #refresh_groups.load_faculties(True)
        #refresh_groups.load_directions(True)
        #refresh_groups.load_specialities(True)
        #refresh_groups.load_groups(True)
        #refresh_groups.load_departments(True)
        #refresh_groups.load_instructors(True)
        #refresh_groups.load_subjects(u"4801998")
        #refresh_groups.load_gts(u"4801998")
        #refresh_groups.load_types(u"4801998")
        #refresh_groups.load_gtsi(u"4801998")
        #refresh_groups.load_lectures(u"4801998")        
        return render(request, 'app/index.html', context_dict)
    

class Register(View):  
    def get(self, request):
        context = RequestContext(request)
        registered = False #will be set to True is the registration completes successfully
        user_form = CustomUserForm()
        student_form = StudentProfileForm()
        instructor_form = InstructorProfileForm()

        return render_to_response(
            'app/register.html',
            {'user_form': user_form, 'student_form': student_form, 'instructor_form': instructor_form, 'registered': registered, 'group_choices': group_choices},
            context)

class RegisterStudent(View):            
    registered = False #will be set to True if the registration completes successfully
    template_name = 'app/register.html'
    def post(self, request, *args, **kwargs):
        context = RequestContext(request)    
        user_form = CustomUserForm(data=request.POST)
        student_form = StudentProfileForm(data=request.POST)                
        if user_form.is_valid() and student_form.is_valid():
            # Если формы заполнены правильно, то сохраняем данные в учетной записи пользователя
            save_student(user_form, student_form)          
            self.registered = True      
      
            return render_to_response(
                'app/register.html',
                {'registered': self.registered},
                context
                )
        return render_to_response(
            self.template_name,
            {'registered': self.registered, 
            'student_form': student_form, 
            'user_form': user_form},
            context
            )            
    def get(self, request, *args, **kwargs): 
        context = RequestContext(request)       
        user_form = CustomUserForm()
        student_form = StudentProfileForm()          
        return render_to_response(
            self.template_name,
            {'registered': self.registered,
            'student_form': student_form,
            'user_form': user_form},
            context
            )


class RegisterInstructor(View):    
    registered = False #will be set to True is the registration completes successfully
    template_name = 'app/register.html'
    def post(self, request, *args, **kwargs):
        context = RequestContext(request)
        user_form = CustomUserForm(data=request.POST)        
        instructor_form = InstructorProfileForm(data = request.POST)
        if user_form.is_valid() and instructor_form.is_valid():       
            save_instructor(user_form, instructor_form)
            self.registered = True
            return render_to_response(
                'app/register.html',
                {'registered': self.registered},
                context)
        else:            
            #print user_form.errors, instructor_form.errors
            return render_to_response(
                self.template_name,
                {'registered': self.registered,
                'instructor_form': instructor_form, 
                'user_form': user_form},
                context
                )       
    def get(self, request, *args, **kwargs):
        context = RequestContext(request)
        user_form = CustomUserForm()        
        instructor_form = InstructorProfileForm()        
        return render_to_response(
            self.template_name,
            {'registered': self.registered,
            'instructor_form': instructor_form,
            'user_form': user_form},
            context
            )

class UserLogin(View):    

    def post(self, request, *args,**kwargs):
        request.encoding = 'utf-8'
        # extract the info provided by the user
        username = request.POST.get('username')
        password = request.POST.get('password')
        
        context = RequestContext(request)
        request.encoding = 'utf-8'

        #check if the credentials are valid
        if "@" in username:
            username = CustomUser.objects.filter(email = username)
            if username:
                username = username[0].username

        user = authenticate(username=username, password=password)
        if user:
            if user.is_active:
                login(request,user)
                return HttpResponseRedirect('/my/')
            else:
                # return HttpResponse("Your account is not activated")
                return render_to_response('app/login.html', {'login_err' : [u'Вы не активировали ваш аккаунт!']}, context)
        else:
            #print "Invalid login/password pair: {0},{1}".format(username,password)
            # return HttpResponse("Invalid login/password combination")
            return render_to_response('app/login.html', {'login_err' : [u'Неверное имя пользователя/пароль']}, context)
    def get(self, request, *args, **kwargs):
        context = RequestContext(request)
        request.encoding = 'utf-8'
        return render_to_response('app/login.html', {}, context)

# Process the data for the my.html page with user's private info
class My(View):
    @method_decorator(login_required)  

    def get(self, request, *args, **kwargs):

        request.encoding = 'utf-8'        
        context = RequestContext(request)        
        user_language = request.user.language
        translation.activate(user_language)
        request.session[translation.LANGUAGE_SESSION_KEY] = user_language

        #get current user
        user = request.user
        #get current time 
        now = timezone.now().date()
        nowtime = timezone.now()

        #set dictionaries to handle all cases    
        admin_info = {}
        account_type = ""
        context_dict = {'curtime': now}  
        test_string = _("Welcome")          
        context_dict['test_string'] = test_string
        context_dict['show_db_warning'] = Constants.objects.get(key='show_db_warning').value=="True"        
        """
        Student's account
        """
        # check if it's a student and extract info for this student
        if user.profile_type.short_name == u'СТ':                   
            # Check if there is profile linked to this user
            if StudentProfile.objects.filter(user = user).exists():  
                # If there is a profile check if it's linked to a study group
                if (not user.st_profile.group is None):
                    # If it's linked, then find the group 
                    group = user.st_profile.group  
                    context_dict['group'] = group
                    # and caclulate the semester           
                    term = refresh_groups.get_term(group.name)
                    context_dict['term'] = term

                    """
                    DB Updates Rules:
                    If the list of Group_Term_Subject has never been updtaed, 
                    or there are no GTS objects for this term or it was updated 
                    more than 10 days ago, then we update it. Same goes for 
                    Group_Term_Subject_Instructor and for lectures (but for 
                    lectures there is a 3-day limit)
                    """
                    # find and exract all Group_Term_Subject objects for 
                    # this term and this group
                    gtss = Group_Term_Subject.objects.filter(group=group, term = term)                                         
                    if len(gtss) < 1 or group.schedule_update_date == None:                        
                        try:
                            refresh_groups.load_gts(group.group_id)
                            gtss = Group_Term_Subject.objects.filter(group=group, term = term)
                        except:
                            pass
                    # find and extract all Group_Term_Subject_Instructor objects
                    # for this term and this group
                    context_dict['gtss'] = gtss
                    gtsis = GTS_Instructor.objects.filter(gts__group = group, gts__term = term)    
                    if len(gtsis) < 1 or group.schedule_update_date == None:
                        try:
                            refresh_groups.load_gtsi(group.group_id)
                            gtsis = GTS_Instructor.objects.filter(gts__group = group, gts__term = term)    
                        except:
                            pass

                    context_dict['gtsis'] = gtsis                
                    gtsils = GTSI_Lecture.objects.filter(gtsi__gts__group = group, gtsi__gts__term = term)
                    if len(gtsils) < 1 or group.schedule_update_date == None:
                        refresh_groups.load_lectures(group.group_id)
                        gtsils = GTSI_Lecture.objects.filter(gtsi__gts__group = group, gtsi__gts__term = term)
                    
                    """
                    shift = get_shift(gtsils, today)
                    start_from = today + timedelta(hours = 24 - today.hour -today.minute/60.0 - today.second/60.0)-timedelta(days=shift)
                    gtsils = gtsils.filter(lecture_start_time__lt = today,
                    lecture_start_time__gt = start_from)                
                    """
                    gtsils = get_lectures_by_date(gtsils,nowtime)                
                    context_dict['gtsils'] = gtsils
                    announcements = Announcement.objects.filter(group = group)
                    context_dict['announcements'] = announcements
                    deadlines = Deadline.objects.filter(gtsi__gts__group = group)
                    context_dict['deadlines'] = deadlines     
                    criteria = Criteria.objects.filter(faculty = None, department = None, overall = False, activity_type__code = 0)
                    context_dict['criteria'] = criteria
                    surveys = AfterLectureSurvey.objects.filter(gtsil__gtsi__gts__group = group)
                    context_dict['surveys'] = surveys

                    if not user.status_confirmed and (not user.st_profile.group is None):
                        code_confirmed = True
                        codeConfirmForm = CodeConfirmationForm()
                        context_dict['codeConfirmForm'] = codeConfirmForm
                else:
                    chooseGroupForm = ChooseGroupForm()
                    context_dict['chooseGroupForm'] = chooseGroupForm                    
            else:
                chooseGroupForm = ChooseGroupForm()
                context_dict['chooseGroupForm'] = chooseGroupForm                                    
            return render_to_response('app/student_account.html', context_dict, context)   
        """
        Общие настройки для завкафедры, декана и обычного преподавателя.
        """
        if user.profile_type.short_name == u'ПР' or user.profile_type.short_name == u'ЗК' or user.profile_type.short_name == u'Д':
            #subjects = GTS_Instructor.objects.filter(gts__group = user.st_profile.group)    
            instructor = user.inst_profile.pub_profile
            now = timezone.now()
            if instructor:
                # CIST profile 
                context_dict['pub_profile'] = instructor
                # subjects list
                gtsis = GTS_Instructor.objects.filter(instructor = instructor)
                subjects_ids = gtsis.values_list('gts__subject', flat=True).distinct()
                subjects = Subject.objects.filter(subject_id__in = subjects_ids)
                #groups list
                groups = subjects.values
                groups_ids =  GTS_Instructor.objects.filter(instructor=instructor).values_list('gts__group', flat=True).distinct()

                groups = Group.objects.filter(group_id__in = groups_ids)

                context_dict['m_groups'] = groups
                #surveys = AfterLectureStudentResults.objects.filter(student__group__group_id__in = groups_ids)                
                surveys = AfterLectureSurvey.objects.filter(gtsil__gtsi__gts__group__group_id__in = groups_ids,
                    gtsil__gtsi__gts__subject__subject_id__in = subjects_ids).distinct()
                sresults = []
                for s in subjects:
                    cand = {}
                    cand['subject'] = s
                    data = []
                    for gr in groups:                        
                        s2 = surveys.filter(gtsil__gtsi__gts__subject = s, gtsil__gtsi__gts__group = gr)                        
                        if s2.count() > 0:                           
                            data.append({'group': gr, 'surveys': s2})
                    cand['data'] = data
                    if len(data) > 0:
                        sresults.append(cand)
                context_dict['sresults'] = sresults
                subjs = []
                for s in subjects:
                    shi = caclulate_shi(instructor, s)
                    subjs.append({'subject': s, 'shi': shi})
                
                gtsilss = GTSI_Lecture.objects.filter(gtsi__instructor = instructor)
                gtsils = get_lectures_by_date(gtsilss,now)                
                context_dict['gtsils'] = gtsils
                context_dict['subjs'] = subjs
                times = get_lectures_by_date_instr(gtsilss,now)                
                context_dict['times'] = times
                #ids = Entry.objects.values_list('column_name', flat=True).filter(...)
                #my_models = MyModel.objects.filter(pk__in=set(ids))
                context_dict["departments"] = instructor.departments
                dept = request.user.inst_profile.primary_department
                context_dict["instructors"] = Instructor.objects.filter(departments = dept)
                

                #surveys = AfterLectureSurvey.objects.filter(gtsil__gtsi__gts__group = group)
                #surveyResults = AfterLectureStudentResults.objects.filter(student__group = group)
                """
                Все, что есть у заведующего кафедрой, но нет у других преподских аккаунтов
                """
                if user.profile_type.short_name == u'ЗК':
                    dept = user.inst_profile.primary_department
                    context_dict["dept_managed"] = dept      
                if not user.status_confirmed:
                    codeConfirmForm = CodeConfirmationForm()
                    context_dict['codeConfirmForm'] = codeConfirmForm
            else:            
                context_dict['pub_profile'] = None
            if user.inst_profile.can_apply_for_student_grant:
                context_dict["named_grants"] = True

            if user.profile_type.short_name == u'ЗК':
                return render_to_response('app/depthead_account.html', context_dict, context)  
            elif user.profile_type.short_name == u'Д':
                return render_to_response('app/fachead_account.html', context_dict, context)  
            elif user.profile_type.short_name == u'Р':
                return render_to_response('app/principal_account.html', context_dict, context)  
            else:
                return render_to_response('app/instructor_account.html', context_dict, context)  
        return render_to_response('app/admin_account.html', context_dict, context)   

class RefreshSchedule(View):
    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        user = request.user
        group = user.st_profile.group
        #print group.name
        refresh_groups.load_lectures(group.group_id)
        return HttpResponseRedirect(reverse('main:my'))

class TestResults(View):
    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        user = request.user
        context = RequestContext(request)
        context_dict = {}
        als_id = self.kwargs['survey_id']        
        group_id = self.kwargs['group_id']
        students = StudentProfile.objects.filter(group__group_id = group_id)        
        tresults = AfterLectureStudentResults.objects.filter(survey__als_id = als_id, student__group__group_id = group_id)
        total_questions = AfterLectureQuestion.objects.filter(survey__als_id = als_id).distinct().count()
        results = []
        for student in students:
            studres = tresults.filter(student = student)
            if studres:
                print studres
            
                res = str(studres[0].correct_answers) + u" из " + str(total_questions)
            else:
                res = u"Не проходил(а) тест"
            results.append({'student': student, 'res': res})
            
        context_dict['results'] = results
        context_dict['total'] = total_questions
        context_dict['group'] = Group.objects.get(group_id = group_id)
        return render_to_response('app/test_results.html', context_dict, context)   


class ConfirmStatus(View):
    @method_decorator(login_required)
    def post(self, request, *args,**kwargs):
        answer = "Error"
        user = request.user
        context = RequestContext(request)
        codeConfirmForm = CodeConfirmationForm(request.POST)
        if codeConfirmForm.is_valid():
            code = codeConfirmForm.cleaned_data['code']
            if user.profile_type.short_name == u'СТ':            
                if (code ==Group.objects.get(group_id = user.st_profile.group.group_id).confirmation_code):
                    user.status_confirmed = True
                    user.save()      
                    answer = "Success"    
            if user.profile_type.short_name == u'ПР':
                user.status_confirmed = True
                user.save()      
                answer = "Success"    

        return HttpResponse(answer)

class UserLogout(View):
    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        request.encoding = 'utf-8'
        logout(request)
        return HttpResponseRedirect(reverse('main:index'))

class AboutUs(View):
    def get(self, request, *args, **kwargs):
        request.encoding = 'utf-8'
        return render(request,'app/about_us.html')

class AboutFeedbacker(View):
    def get(self, request, *args, **kwargs):        
        request.encoding = 'utf-8'
        context = RequestContext(request)
        return render_to_response('app/about_feedbacker.html', context)

class RegisterConfirm(View):
    #check if user is already logged in and if he is redirect him to some other url, e.g. home    
    #activation key!!!!
    def get(self, request, *args, **kwargs):
        activation_key = self.kwargs['activation_key']
        if request.user.is_authenticated():
            HttpResponseRedirect(reverse('main:my'))
 
        # check if there is UserProfile which matches the activation key (if not then display 404)        
        if CustomUser.objects.filter(activation_key=activation_key).exists():        
            user = CustomUser.objects.get(activation_key=activation_key)    
            #check if the activation key has expired, if it has then render confirm_expired.html
            if user.key_expires < timezone.now():
                return render_to_response('app/confirm_expired.html')
            #if the key hasn't expired save user and set him as active and render some template to confirm activation    
            user.is_active = True    
            user.save()
            if is_nure_email(user):
                user.status_confirmed = True
            user.save()
            return render_to_response('app/confirm.html')    
        return HttpResponse("Invalid confirmation code")

def is_nure_email(user):
    email = user.email
    domain = email.split("@")[-1]
    return domain == "nure.ua"

class AttachGroup(View):
    @method_decorator(login_required)
    def post(self, request, *args,**kwargs):
        answer = "Error"
        context = RequestContext(request)
        chooseGroupForm = ChooseGroupForm(request.POST)
        user = request.user
        if chooseGroupForm.is_valid():       
            group_name = u'{0}-{1}-{2}'.format(
            (chooseGroupForm.cleaned_data['faculty_name']+chooseGroupForm.cleaned_data['study_mode']),
            str(chooseGroupForm.cleaned_data['year_started']),
            str(chooseGroupForm.cleaned_data['group_number']))
            if not StudentProfile.objects.filter(user = user).exists():
                profile = StudentProfile(
                    user = user,
                    group = None,
                    student_id = None,
                    is_prefect = False
                    )
                profile.save()
                answer = "Success"                               
            else:
                profile = StudentProfile.objects.get(user = user)
            if Group.objects.filter(name=group_name).exists():
                group = Group.objects.get(name=group_name)                            
                profile.group = group
                profile.save()  
                answer = "Success"
                if not Group_Term_Subject.objects.filter(group = group).exists():
                    group_id = group.group_id
                    refresh_groups.load_group_data(group_id)                                        
            return HttpResponse(answer)

class TakeSurvey(View):
    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        user = request.user
        request.encoding = 'utf-8'        
        context = RequestContext(request) 
        context_dict = {}   
        group = user.st_profile.group
        surveys = AfterLectureSurvey.objects.filter(gtsil__gtsi__gts__group = group)
        context_dict['group'] = group
        survey_id = self.kwargs['survey_id']
        survey = AfterLectureSurvey.objects.get(als_id = survey_id)
        context_dict['voted'] = False
        if AfterLectureStudentResults.objects.filter(survey = survey, student = user.st_profile).exists():
            context_dict['voted'] = True
        context_dict['survey'] = survey
        questions = survey.afterlecturequestion_set.all()
        qa = []
        for q in questions:
            answers = q.afterlecturechoice_set.all()
            question = {"question": q.question_text, "answers": answers}
            qa.append(question)
        context_dict['questions'] = questions
        context_dict['qa'] = qa
        #user._meta.get_all_related_objects()
        gtsil = survey.gtsil.all().get(gtsi__gts__group = group)
        context_dict['gtsil'] = gtsil
        return render_to_response('surveys/take_survey.html', context_dict, context)  
    @method_decorator(login_required)
    def post(self, request, *args, **kwargs):
        user = request.user
        request.encoding = 'utf-8'        
        context = RequestContext(request)
        data = json.loads(request.body)
        answers = data['survey_answers']
        num_questions = len(answers)
        survey = AfterLectureChoice.objects.get(alc_id = answers[0]).question.survey
        correct = 0
        for answer in answers: 
            st_answer = AfterLectureChoice.objects.get(alc_id = answer)
            st_answer.votes +=1
            st_answer.save()
            if st_answer.correct:
                correct+=1
            newStudentAnswer = AfterLectureStudentResult(answer = st_answer, student = user.st_profile)
            newStudentAnswer.save()
        newStudenResult = AfterLectureStudentResults(survey = survey, student = user.st_profile, correct_answers = correct)
        newStudenResult.save()
        return HttpResponse("Success")

"""
Вью, который осуществляет голосование по лекции.

Метод GET:
Собирает всю необходимую информацию для голосования:
- информацию о лекции
- список критериев
- голосовал ли уже студент за этот предмет (флаг voted)

Метод GET:
Добавляет в базу данных оценку пользователя за лекцию
Для оценки по каждому из критериев - своя запись в базе данных
TODO1: Изменить первичные ключи в этой срани
TODO2: Вынести в evaluation_views.py
"""
class EvaluateLecture(View):
    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        user = request.user
        request.encoding = 'utf-8'        
        context = RequestContext(request)        
        gtsil_id = self.kwargs['gtsil_id']
        gtsil = GTSI_Lecture.objects.get(id = gtsil_id)        
        context_dict = {}
        context_dict['gtsil'] = gtsil

        criteria = get_criteria(user, gtsil.gtsi, False)
        context_dict['criteria'] = criteria
        voted = False        
        if GTSIL_Criteria_User_Vote.objects.filter(gtsil = gtsil, student = user).exists():
            voted = True
        context_dict['voted'] = voted
        # достаем группу
        group = user.st_profile.group
        survey = AfterLectureSurvey.objects.filter(gtsil = gtsil)
        #context_dict["instructors"] = Instructor.objects.filter(departments = dept)
        return render_to_response('voting/lecture_evaluation.html', context_dict, context)   
    @method_decorator(login_required)
    def post(self, request, *args, **kwargs):  
        request.encoding = 'utf-8'        
        user = request.user
        context_dict = {}        
        context = RequestContext(request)        
        gtsil_id = self.kwargs['gtsil_id']
        gtsil = GTSI_Lecture.objects.get(id = gtsil_id) 
        context_dict['gtsil'] = gtsil
        atype = gtsil.gtsi.activity_type
        criteria = get_criteria(user, gtsil.gtsi, False)
        context_dict['criteria'] = criteria
        context_dict['voted'] = True
        skipped = request.POST.get('skipped')
        feedback = request.POST.get('feedback')
        if (feedback != ""):
            complaint = request.POST.get("is_complaint")
            is_complaint = False
            if (complaint == "on"):
                is_complaint = True
            newFeedback = GTSIL_Feedback(gtsil = gtsil, student = user, feedback = feedback, is_complaint = is_complaint)
            newFeedback.save()
        if skipped == "on":
            newVote = GTSIL_Criteria_User_Vote(gtsil = gtsil, criteria = None, student = user, vote = None)            
            newVote.save()
            return HttpResponse("Success")
        for cr in criteria:
            name = str(cr.id)
            vote = request.POST.get(name)           
            if (not vote is None): #for text field: vote != ""
                vote = int(vote)
                newVote = GTSIL_Criteria_User_Vote(gtsil = gtsil, criteria = cr, student = user, vote = vote)
                newVote.save()   
        return HttpResponse("Success")

"""
Вью, который позволяет голосовать по предмету

Метод GET:
Собирает всю необходимую информацию для голосования:
- информация о предмете
- критерии
- информация о том, голосовал ли пользователь
Метод POST:
Записывает результаты голосования: для каждого типа 
занятия и каждого предмета - своя запись.

TODO1: Проверить для разных типов занятий
TODO2: Вынести в evaluation_views.py
"""
class EvaluateSubject(View):
    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        user = request.user
        request.encoding = 'utf-8'        
        context = RequestContext(request)        
        gtsi_id = self.kwargs['gtsi_id']
        gtsi = GTS_Instructor.objects.get(id = gtsi_id)        
        context_dict = {}
        context_dict['gtsi'] = gtsi        
        atype = gtsi.activity_type
        criteria = get_criteria(user, gtsi, True)
        context_dict['criteria'] = criteria
        voted = False        
        if GTSI_Criteria_User_Vote.objects.filter(gtsi = gtsi, student = user).exists():
            voted = True
        context_dict['voted'] = voted
        return render_to_response('voting/subject_evaluation.html', context_dict, context) 

    @method_decorator(login_required)
    def post(self, request, *args, **kwargs):
        user = request.user
        request.encoding = 'utf-8'        
        context = RequestContext(request)        
        gtsi_id = self.kwargs['gtsi_id']
        gtsi = GTS_Instructor.objects.get(id = gtsi_id)        
        context_dict = {}
        context_dict['gtsi'] = gtsi        
        atype = gtsi.activity_type
        criteria = get_criteria(user, gtsi, True)
        for cr in criteria:
            name = str(cr.id)            
            vote = request.POST.get(name)
            if (not vote is None): #for text field: vote != ""
                vote = int(vote)
                newVote = GTSI_Criteria_User_Vote(gtsi = gtsi, criteria = cr, student = user, vote = vote)
                newVote.save()
        return HttpResponse("Success")

class FindProfile(View):
    @method_decorator(login_required)
    def post(self, request, *args, **kwargs):
        request.encoding = 'utf-8'        
        context = RequestContext(request)        
        fname = request.POST.get('fname')
        lname = request.POST.get('lname')
        # Search last names
        context_dict = {}
        result1 = Instructor.objects.filter(last_name__icontains = lname)
        result2 = Instructor.objects.filter(first_name__icontains = fname)
        results = result1 & result2
        context_dict['sresults'] = results
        if request.user.inst_profile.can_apply_for_student_grant:
            context_dict["named_grants"] = True
        return render_to_response('app/instructor_account.html', context_dict, context)   

class AttachProfile(View):
    @method_decorator(login_required)
    def post(self, request, *args, **kwargs):
        user = request.user
        request.encoding = 'utf-8'        
        context = RequestContext(request)        
        pub_profile_id = self.kwargs["instructor_id"]
        pub_profile = Instructor.objects.get(id = pub_profile_id)
        profile = user.inst_profile
        profile.pub_profile = pub_profile
        profile.save()
        #refresh_instructor.refresh_db(pub_profile.cist_id)
        context_dict = {}
        context_dict["success"] = "Success"
        return HttpResponseRedirect(reverse('main:my'))

def get_lectures_by_date(gtsils, today):    
    current = today + timedelta(hours = 24 - today.hour -today.minute/60.0 - today.second/60.0)
    count =0
    limit = 0
    lectures_by_date = []
    while count < 3 and limit < 10:        
        lectures_this_day = gtsils.filter(lecture_start_time__day = current.day, lecture_start_time__month = current.month, lecture_start_time__year = current.year)
        if lectures_this_day.exists():    
            lectures_by_date.append(lectures_this_day)
            count = count + 1
        limit = limit + 1
        current = current - timedelta(days=1)
    return lectures_by_date

def get_lectures_by_date_instr(gtsils, today):    
    current = today + timedelta(hours = 24 - today.hour -today.minute/60.0 - today.second/60.0)
    count =0
    limit = 0
    lectures_by_date = []
    output = []
    while count < 3 and limit < 10:        
        if gtsils.filter(lecture_start_time__day = current.day, lecture_start_time__month = current.month, lecture_start_time__year = current.year).exists():            
            lectures = []
            #all lectures for all groups for that day
            cur_gtsils = gtsils.filter(lecture_start_time__day = current.day, lecture_start_time__month = current.month, lecture_start_time__year = current.year)
            # all distinct lecture times for that day
            times = cur_gtsils.values_list('lecture_start_time').distinct()                           
            key =  times[0][0]
            for time in times:                                                
                lecs = cur_gtsils.filter(lecture_start_time = time[0])
                pairs = lecs.values_list('number_pair').distinct()
                for pair in pairs:                                        
                    lec_pairs = lecs.filter(number_pair = pair[0])
                    activity_type = lec_pairs[0].gtsi.activity_type.short_name
                    subject = lec_pairs.values_list('gtsi__gts__subject__full_name').distinct()[0]
                    groups = lec_pairs.values_list('gtsi__gts__group__name').distinct()
                    lectures.append({'subject':subject, 'groups': groups, 'gtsil_id': lecs[0].id, 'activity_short': activity_type})
            output.append([key, lectures])
            count = count + 1
        limit = limit + 1
        current = current - timedelta(days=1)
    return output

"""
Возвращает True, если имя пользователя занято
"""

def username_exists(request, username = "user"):
    answer = CustomUser.objects.filter(username = username).exists()
    return HttpResponse(answer)

"""
Возвращает True, если такой адрес почты уже есть
"""
def email_exists(request, email = "email"):
    return HttpResponse(CustomUser.objects.filter(email = email).exists())


"""
Возрвращает критерии для оценивания:
- если есть критерии от кафедры, то действуют критерии кафедры
- если критериев от кафедры нет, то действуют критерии факультета
- если критериев от факультета нет, то действуют общие критерии (выставленные 
    по умолчанию, либо ректором)
ВАЖНО: Общефакультетские критерии будут использованы и в том случае, если 
один и тот же преподаватель принадлежит к нескольким кафедрам внутри одного факультета)
Если же преподаватель относится к нескольким кафедрам разных факультетов, то 
используютя (при наличии) критерии той кафедры, которая относится к факультету группы,
в которой учится оценивающий занятие студент.
"""
def get_criteria(user, gtsi, overall):
        atype = gtsi.activity_type
        if atype.activity_type == "practice" or atype.activity_type == "laboratory":
            atype = ActivityType.objects.get(code = 10)
        """
        faculty = user.st_profile.group.speciality.direction.faculty
        if gtsi.instructor != None:
            depts = gtsi.instructor.departments.all()
            depts = depts.filter(faculty = faculty)
            if len(depts) == 1:
                department = depts[0]
                criteria = Criteria.objects.filter(department = department, overall = overall, activity_type = atype)
                if criteria:
                    return criteria

        criteria = Criteria.objects.filter(faculty = faculty, overall = overall, activity_type = atype)
        if criteria:
            return criteria
        """
        criteria = Criteria.objects.filter(overall = overall, activity_type = atype)
        return criteria

"""
Для конкретной лекции, прошедшей в определенное время,
возвращает набор доступных параметров, по которым
пользователь может увидеть статистику. 
Важно: в данном случае лекция - это активность, 
произошедшая в конкретный момент времени (дата, время) у
конкретного списка групп с конкретным преподавателем. 
То есть лекция 19 февраля в 11:15 у Безкоровайного у группы КН-14-5
и лекция 19 февраля в 11:15 у Безкоровайного у группы КН-14-6  - это 
одно и то же событие. 
"""
class LectureResults(View):
    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        # Добыть пользователя
        user = request.user
        request.encoding = 'utf-8'        
        context = RequestContext(request)        

        # Извлечь необходимую информацию
        gtsil_id = self.kwargs['gtsil_id']        
        lecture = GTSI_Lecture.objects.get(id = gtsil_id)
        number_pair = lecture.number_pair
        activity_type_code = lecture.gtsi.activity_type.code
        subject = lecture.gtsi.gts.subject.full_name        
        time = lecture.lecture_start_time
        instructor = lecture.gtsi.instructor
        context_dict = {}
        #groups = json.dumps(list(GTSI_Lecture.objects.filter(lecture_start_time = time, gtsi__instructor = instructor).values_list('gtsi__gts__group__name', flat = True).distinct()))
        # Votes for this instructor, for this lecture, for these groups
        votes = GTSIL_Criteria_User_Vote.objects.filter(gtsil__gtsi__instructor = instructor,             
            gtsil__lecture_start_time = time,            
            gtsil__number_pair = number_pair).exclude(criteria = None)
        # Число проголосовавших
        voters = votes.values_list('student', flat = True).distinct().count()
        context_dict['voters'] = voters                
        if voters != 0:        
            context_dict['criteria'] = get_criteria_dict_from_votes(votes)
            # Время начал лекции
            # Группы
            
            # Группы, у которых проходила эта лекция
            groups = []        
            groups_ids =  GTS_Instructor.objects.filter(instructor=instructor, gts__subject = lecture.gtsi.gts.subject).values_list('gts__group', flat=True)
            grps = Group.objects.filter(group_id__in = groups_ids)        
            for group in grps:
                groups.append({'id': group.group_id, 'name': group.name})
            context_dict['groups'] = groups
        # Время начал лекции
        context_dict['lecture_time'] = time      
        # Название предмета
        context_dict['subject'] =subject        
        return JsonResponse(context_dict)

class SubjectResults(View):
    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):     
        request.encoding = 'utf-8'
        user = request.user
        instructor = user.inst_profile.pub_profile
        context = RequestContext(request)        
        subject_id = self.kwargs['subject_id']        
        subject = Subject.objects.get(subject_id = subject_id)                
        # Информация о предмете
        context_dict = {'subject_id': subject_id, 'subject_short_name': subject.short_name, 'subject_full_name': subject.full_name}

        # Значение по умолчанию для начальной даты. Равно дате начала семестра
        start_date_str = Constants.objects.get(key='current_term_start_date').value        
        start_date = datetime.strptime(start_date_str, '%d-%m-%Y').date()

        # Выбрать все оценки, которые есть для этого преподавателя 
        # вне зависимости от типа занятий за все время
        votes = GTSIL_Criteria_User_Vote.objects.filter(
            gtsil__gtsi__instructor = instructor,            
            gtsil__gtsi__gts__subject = subject).exclude(criteria=None)

        if votes == 0:
            context_dict['error'] = u'Никто еще не высказал своего мнения'
            return JsonResponse(context_dict)
        # Выбрать все предметы, которые когда либо вел этот преподаватель        
        # за все время
        activities = GTS_Instructor.objects.filter(
            instructor = instructor,
            gts__subject = subject)
        acts = []
        # Выбрать все типы, которые он вел когда-либо (с момента начала использования приложения)
        # за все время
        a_types_ids = activities.values_list('activity_type__code', flat = True).distinct()
        a_types = ActivityType.objects.filter(code__in = a_types_ids)
        # Сделать словарь для каждого типа занятий
        for a_type in a_types:
            activity = {}
            # Код типа занятия 
            activity['code'] = a_type.code
            # Короткое название типа занятия
            activity['short_name'] = a_type.short_name
            # Выбрать все оценки для занятия этого типа
            a_votes = votes.filter(gtsil__gtsi__activity_type = a_type)
            voters = votes.values_list('student', flat = True).distinct().count()
            context_dict['voters'] = voters                
            if len(a_votes) == 0:
                activity['error'] = u'Никто еще не высказал своего мнения'
                break
            # Выбрать все занятия этого семестра
            cur_term_votes = a_votes.filter(gtsil__lecture_start_time__gt = start_date)
            if len(cur_term_votes) > 0:
                # Выбрать дефолтной дату начала этого семестра
                activity['start_date'] = start_date_str          
                activity['groups'] = get_groups_dict_from_votes(cur_term_votes)
                activity['criteria'] = get_criteria_dict_from_votes(cur_term_votes)
            else:
                # Выбрать в качестве дефолтной дату первой записи оценки этого предмета
                vts = (a_votes.order_by('gtsil__lecture_start_time')[0]).gtsil.lecture_start_time
                activity['start_date'] = vts.strftime("%d-%m-%Y")                
                activity['groups'] = get_groups_dict_from_votes(a_votes)
                activity['criteria'] = get_criteria_dict_from_votes(a_votes)
            acts.append(activity)
        context_dict['activities'] = acts
        return JsonResponse(context_dict)

      
def get_groups_dict_from_votes(votes):
    groups = []
    groups_ids = votes.values_list('gtsil__gtsi__gts__group__group_id', flat=True).distinct()
    grps = Group.objects.filter(group_id__in = groups_ids)
    for group in grps:
        groups.append({'id': group.group_id, 'name': group.name})
    return groups

def get_criteria_dict_from_votes(votes):
    criteria = []
    criteria_ids = votes.values_list('criteria', flat = True)
    crtr = Criteria.objects.filter(id__in = criteria_ids)
    for cr in crtr:
        criteria.append({'id': cr.id, 'name': cr.criteria_name, 'weight': cr.weight})                
    return criteria

class LectureResultsGraphData(View):    
    @method_decorator(login_required)     
    def post(self, request, *args, **kwargs):
        data = json.loads(request.body)
        gtsil_id = int(data['gtsil_id'])
        time = GTSI_Lecture.objects.get(id = gtsil_id).lecture_start_time
        instructor = GTSI_Lecture.objects.get(id = gtsil_id).gtsi.instructor        
        show_all = data['show_all']        
        criteria = data['criteria']        
        context_dict = {}
        if show_all:            
            votes = GTSIL_Criteria_User_Vote.objects.filter(gtsil__lecture_start_time = time,
                criteria__id = criteria, gtsil__gtsi__instructor = instructor).exclude(criteria = None)
            voters = votes.values_list('student', flat = True).distinct().count()
        
        else: 
            groups_ids = data['groups']
            votes = GTSIL_Criteria_User_Vote.objects.filter(gtsil__lecture_start_time = time,
                criteria__id = criteria, gtsil__gtsi__gts__group__group_id__in = groups_ids,
                gtsil__gtsi__instructor = instructor).exclude(criteria = None)   
            voters = votes.values_list('student', flat = True).distinct().count()
        frequencies = {}
        frequencies['values_dict'] = get_frequencies_from_votes(votes)                
        #context_dict = {"1":2, "2": 0, "3": 4, "4": 4, "5": 8}        
        frequencies['voters'] = voters
        return JsonResponse(frequencies)

class SubjectResultsGraphData(View):
    @method_decorator(login_required)
    def post(self, request, *args, **kwargs):
        data = json.loads(request.body)
        user = request.user
        instructor = user.inst_profile.pub_profile
        subject_id = data['subject_id']
        start_date = data['start_date']
        end_date = data['end_date']
        activity_type_code = data['activity_type_id']
        criteria_id = data['criteria_id']
        show_all = data['show_all']
        if show_all:
            # Потенциально слабое место - дата не конвертирована, но работает... Если работать перестанет, 
            # то стоит поискать ошибку и здесь тоже
            votes = GTSIL_Criteria_User_Vote.objects.filter(gtsil__lecture_start_time__gte = start_date,
                gtsil__lecture_start_time__lte = end_date,
                criteria__id = criteria_id,
                gtsil__gtsi__activity_type__code = activity_type_code,
                gtsil__gtsi__instructor = instructor,
                gtsil__gtsi__gts__subject__subject_id = subject_id)
            voters = votes.values_list('student', flat = True).distinct().count()
        else: 
            groups_ids = data['groups']
            votes = GTSIL_Criteria_User_Vote.objects.filter(gtsil__lecture_start_time__gte = start_date,
                gtsil__lecture_start_time__lte = end_date,
                criteria__id = criteria_id,
                gtsil__gtsi__activity_type__code = activity_type_code,
                gtsil__gtsi__gts__group__group_id__in = groups_ids,
                gtsil__gtsi__instructor = instructor,
                gtsil__gtsi__gts__subject__subject_id = subject_id)
            voters = votes.values_list('student', flat = True).distinct().count()
        frequencies = {}
        frequencies['values_dict'] = get_frequencies_from_votes(votes)
        frequencies['voters'] = voters
        return JsonResponse(frequencies)

class GroupsAndCriteria(View):
    @method_decorator(login_required)
    def post(self, request, *args, **kwargs):
        data = json.loads(request.body)
        start_date = data['start_date']
        end_date = data['end_date']
        subject_id = data['subject_id']
        activity_type_code = data['activity_type_code']
        user = request.user
        instructor = user.inst_profile.pub_profile
        votes = GTSIL_Criteria_User_Vote.objects.filter(gtsil__lecture_start_time__gte = start_date,
            gtsil__lecture_start_time__lte = end_date,
            gtsil__gtsi__instructor = instructor,
            gtsil__gtsi__gts__subject__subject_id = subject_id,
            gtsil__gtsi__activity_type__code = activity_type_code)
        context_dict = {}
        context_dict['groups'] = get_groups_dict_from_votes(votes)
        context_dict['criteria'] = get_criteria_dict_from_votes(votes)
        context_dict['no_votes'] = len(context_dict['groups']) == 0
        return JsonResponse(context_dict)


def get_frequencies_from_votes(votes):
    frequencies = {}
    for i in range(1,6):
        frequencies[str(i)] = votes.filter(vote = i).count()                                
    return frequencies



def caclulate_shi(instructor, s):
    votes = GTSIL_Criteria_User_Vote.objects.filter(gtsil__gtsi__instructor = instructor, 
        gtsil__gtsi__gts__subject = s).exclude(criteria=None)
    if votes: 
        # Возможные значения голосов
        values = votes.values_list('vote', flat=True).distinct()
        # Критерии, которые использовались для опроса по этому предмету
        criteria = Criteria.objects.filter(id__in = votes.values_list('criteria__id', flat= True).distinct())
        # Максимально возможное значение SHI:
        max_shi = 5*(criteria.aggregate(Sum('weight'))['weight__sum'])    
        shi = 0
        for cr in criteria:
            # Выбрать все по этом критерию
            cr_votes = votes.filter(criteria = criteria)
            # Посчитать среднее для каждого критерия
            votes_avg = cr_votes.aggregate(Avg('vote'))['vote__avg']        
            shi += votes_avg*cr.weight
        shi = str(round((shi/max_shi)*100,2)) + "%"
    else:
        shi = "N/A"
    return shi

class Faq(View):
    def get(self, request, *args, **kwargs):
        context_dict = {}
        context = RequestContext(request)        
        return render_to_response('app/faq.html', context_dict, context)   


class ContactAdmin(View):
    def get(self, request, *args, **kwargs):
        context_dict = {}
        context = RequestContext(request)
        context_dict['sent'] = False
        return render_to_response('app/contact_admin.html', context_dict, context)
    def post(self, request, *args, **kwargs):
        request.encoding = 'utf-8'        
        context = RequestContext(request)        
        subject = request.POST.get("subject")
        email = request.POST.get("email")
        body = request.POST.get("letter_body")
        admin = 'electric.blake@gmail.com'
        context_dict = {}
        send_mail(subject, body, email, [admin], fail_silently=False)
        context_dict['sent'] = True
        return render_to_response('app/contact_admin.html', context_dict, context)

class ShowGroupInfo(View):
    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        context_dict = {}
        context = RequestContext(request)
        group_id = self.kwargs["group_id"]
        group = Group.objects.get(group_id = group_id)
        context_dict['group'] = group
        context_dict['num_users'] = StudentProfile.objects.filter(group = group).count()
        surveys = AfterLectureSurvey.objects.filter(gtsil__gtsi__gts__group = group)
        surveyResults = AfterLectureStudentResults.objects.filter(student__group = group)
        sresults = []
        context_dict['sresults'] = sresults
        for surv in surveys:
            count = surveyResults.filter(survey = surv).count()
            sresults.append({'surv': surv, 'count': count})

        return render_to_response('app/group_info.html', context_dict, context)        

class MyProfile(View):
    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        context_dict = {}
        context = RequestContext(request)
        context_dict['is_instructor'] = request.user.profile_type.short_name == u"ПР"
        context_dict["is_test_account"] = request.user.username == "demo"
        return render_to_response('app/my_profile.html', context_dict, context)
    @method_decorator(login_required)
    def post(self, request, *args, **kwargs):
        request.encoding = 'utf-8'        
        context = RequestContext(request)  
        user = request.user      
        context_dict = {}
        context_dict['is_instructor'] = request.user.profile_type.short_name == u"ПР"
        """
        email = request.POST.get("email")
        print email
        # Проверка доступности почты должна уйти при вставке валидаторов
        if email != "" and not CustomUser.objects.filter(email = email).exists():
            user.email = email
        """
        password = request.POST.get("password")
        context_dict["is_test_account"] = user.username == "demo"
        if authenticate(username=user.username, password=password) and user.username != "demo":
            first_name = request.POST.get("first_name")
            if first_name != "":
                user.first_name = first_name
            last_name = request.POST.get("last_name")
            if last_name != "":
                user.last_name = last_name
            if request.user.profile_type.short_name == u"ПР":
                middle_name = request.POST.get("middle_name")
                if middle_name != "":
                    user.inst_profile.middle_name = middle_name
                    user.inst_profile.save()
            password1 = request.POST.get("password1")
            password2 = request.POST.get("password2")
            if password1 == password2 and password1 != "":
                user.set_password(password1)
            context_dict['saved'] = True
            user.save()
        return render_to_response('app/my_profile.html', context_dict, context)


class Tutorials(View):
    def get(self, request, *args, **kwargs):
        context_dict ={}
        context = RequestContext(request)
        return render_to_response('app/tutorials.html', context_dict, context)


class StudentRatingView(View):
    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        students_data = StudentRatingData.objects.all().filter(rating_id=2).order_by('-integral')
        grant_form = StudentGrantDataForm()
        context_dict = {}
        context_dict["students_data"] = students_data
        context_dict["grant_form"] = grant_form
        context = RequestContext(request)
        context_dict["edit"] = False
        return render_to_response('app/student_rating_tab.html', context_dict, context)
    @method_decorator(login_required)
    def post(self, request, *args, **kwargs):
        request.encoding = 'utf-8'        
        students_data = StudentRatingData.objects.all().filter(rating_id=2).order_by('-integral')
        context_dict = {}
        context_dict["students_data"] = students_data
        context = RequestContext(request)
        user = request.user     
        grant_form = StudentGrantDataForm(data = request.POST) 
        context_dict["edit"] = False
        if grant_form.is_valid():
            grant_data = grant_form.save()    
            integral = grant_data.gpa + grant_data.english*50 + grant_data.patents*20 + grant_data.competitions*10 + grant_data.articles_int*20 + grant_data.articles_nat*10 + grant_data.presentation_int*15 + grant_data.presentation_nat*5 + grant_data.sport*5 + grant_data.scopus*20
        
            grant_data.integral = integral
            grant_data.added_by = user.inst_profile
            grant_data.save()
            grant_form = StudentGrantDataForm()
        else:
            grant_form = StudentGrantDataForm()
        context_dict["grant_form"] = grant_form
        return render_to_response('app/student_rating_tab.html', context_dict, context)

class OpenStudentRating(View):
    def get(self, request, *args, **kwargs):
        students_data = StudentRatingData.objects.all()
        students_data = students_data.filter(rating_id=1).order_by('-integral')
        context_dict = {}
        context_dict["students_data"] = students_data
        context = RequestContext(request)
        return render_to_response('app/open_student_rating.html', context_dict, context)

class EditStudentRating(View):
    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        students_data = StudentRatingData.objects.all().filter(rating_id=2).order_by('-integral')
        context_dict = {}
        rating_id = self.kwargs["rating_id"]
        rating = StudentRatingData.objects.get(id = rating_id)
        grant_form = StudentGrantDataForm(instance = rating)
        #my_record = MyModel.objects.get(id=XXX)
        #form = MyModelForm(instance=my_record)
        
        context_dict["students_data"] = students_data
        context_dict["grant_form"] = grant_form
        context_dict["edit"] = True
        context_dict["elid"] = rating_id
        context = RequestContext(request)
        return render_to_response('app/student_rating_tab.html', context_dict, context)
    @method_decorator(login_required)
    def post(self, request, *args, **kwargs):
        context = RequestContext(request)
        students_data = StudentRatingData.objects.all().filter(rating_id=2).order_by('-integral')
        context_dict = {}
        context_dict["students_data"] = students_data
        rating_id = self.kwargs["rating_id"]
        rating = StudentRatingData.objects.get(id = rating_id)
        grant_form = StudentGrantDataForm(request.POST, instance=rating)
        grant_data = grant_form.save()
        integral = grant_data.gpa + grant_data.english*50 + grant_data.patents*20 + grant_data.competitions*10 + grant_data.articles_int*20 + grant_data.articles_nat*10 + grant_data.presentation_int*15 + grant_data.presentation_nat*5 + grant_data.sport*5 + grant_data.scopus*20
        grant_data.integral = integral
        grant_data.save()

        context_dict["edit"] = False
        grant_form = StudentGrantDataForm()
        context_dict["grant_form"] = grant_form
        return HttpResponseRedirect(reverse('main:student_rating'))

class DeleteStudentRating(DeleteView):
    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        students_data = StudentRatingData.objects.all().filter(rating_id=2).order_by('-integral')
        context_dict = {}
        context_dict["students_data"] = students_data
        rating_id = self.kwargs["rating_id"]
        StudentRatingData.objects.get(id = rating_id).delete()
        context_dict["edit"] = False
        grant_form = StudentGrantDataForm()
        context_dict["grant_form"] = grant_form
        return HttpResponseRedirect(reverse('main:student_rating'))

# Эта штука должна быть у всех, рангом выше преподавателя, но у всех в разных видах
# Возможно, придется тоже как-то выносить это, потому что будер разная структура
class ShowComplaints(View):
    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        user = request.user
        context_dict = {}
        context = RequestContext(request)
        if user.profile_type.short_name == u'ЗК':
            dept = request.user.inst_profile.primary_department
            complaints = GTSIL_Feedback.objects.filter(gtsil__gtsi__instructor__departments = dept, pub_status = u'ОД')
            context_dict["complaints"] = complaints
            overall_complaints = GTSI_Feedback.objects.filter(gtsi__instructor__departments = dept, pub_status = u'ОД')
            context_dict["overall_complaints"] = overall_complaints
            return render_to_response('app/dept_complaints.html', context_dict, context)
        # А вот тут начинаются Вечера на хуторе близ Деканьки
        if user.profile_type.short_name == u'Д':
            fac = request.user.inst_profile.primary_faculty
            complaints = GTSIL_Feedback.objects.filter(gtsil__gtsi__instructor__departments__faculty = fac, pub_status = u'ОД')
            context_dict["complaints"] = complaints
            return render_to_response('app/fac_complaints.html', context_dict, context)



