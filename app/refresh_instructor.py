#!/usr/bin/env python
# -*- coding:utf-8 -*-

# Стандартные библиотеки
from __future__ import unicode_literals
import json, urllib2, codecs
import time, pytz
import random
import re
from datetime import date
from datetime import datetime
from django.utils import timezone
from .models import (CustomUser, StudentProfile, InstructorProfile, 
	Group, Faculty, Direction, Speciality, Department, Instructor,
	ActivityType, Subject, Group_Term_Subject,GTS_Instructor,
	GTSI_Lecture)

def refresh_db(cist_id):
	fp = urllib2.urlopen('http://cist.nure.ua/ias/app/tt/P_API_EVENT_JSON?timetable_id='+cist_id+'&type_id=2')
	#http://cist.nure.ua/ias/app/tt/P_API_EVENT_JSON?timetable_id=44&type_id=2
	all_objects = json.load((codecs.getreader('cp1251')(fp)))
	events = all_objects['events']
	group_ids = event['groups']
	for event in events:
		for groupid in group_ids:
			g = get_or_none(Group, group_id = groupid)
			if not g is None:
				subject = Subject.objects.get_or_none(subject_id = event['subject_id'])
				if not subject is None:
					gts, created = Group_Term_Subject.objects.get_or_create(group = g, subject = subject, term = get_term(g.name))
					if not gts is None:
						if ActivityType.objects.filter(code = event["type"]).exists():
							activity_type = ActivityType.objects.get(code = event["type"])
							instructor = Instructor.objects.get(cist_id = cist_id)
							gtsi,created = GTS_Instructor.objects.get_or_create(gts=gts, instructor = instructor, activity_type = activity_type)
							if not gtsi is None:						
								lecture_start_time = datetime.fromtimestamp(int(event["start_time"])).date()
								number_pair = event["number_pair"]						
								if not GTSI_Lecture.objects.filter(gtsi = gtsi,
									lecture_start_time = lecture_start_time,
									number_pair = number_pair).exists():							
									gtsil = GTSI_Lecture(
										gtsi = gtsi,
										lecture_start_time = lecture_start_time,
										lecture_name = None,
										number_pair = number_pair)
									gtsil.save()							

"""
"subject_id":1053099,
"start_time":1454996700,
"end_time":1455002400,
"type":0,
"number_pair":1,
"auditory":"334",
"teachers":[
44
],
"groups":[
3802959,
4026973
"""	