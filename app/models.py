#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
Definition of models.
"""
# Стандартные библиотеки
from __future__ import unicode_literals
import datetime
from string import letters

# Библиотеки Джанго
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils import timezone
from django.utils.encoding import smart_unicode
from django.utils.translation import ugettext_lazy as _
from django.db.models.signals import post_save
from django.core.urlresolvers import reverse

# Локальные импорты


# Константы
STUDENT = u'СТ'
INSTRUCTOR = u'ПР'
ADMIN = u'АД'
DEP_HEAD = u'ЗК'
FAC_HEAD = u'Д'
HEAD = u'Р'
STAFF = u'С'
type_choices = (
	(STUDENT, u'Студент'),
	(INSTRUCTOR,u'Преподаватель'),
	(ADMIN, u'Админ'),
	(DEP_HEAD, u'Заведующий кафедрой'),
	(FAC_HEAD, u'Декан'),
	(HEAD, u'Ректор'),
	(STAFF, u'Сотрудник'),
	)
class Constants(models.Model):
	key = models.CharField(primary_key = True, max_length =40)
	value = models.CharField(max_length=512)
	def __unicode__(self):
		return smart_unicode(self.key + ": " + self.value)
	def save(self, *args, **kwargs):
		if isinstance(self.key, str):
			self.key = self.key.decode("utf-8")
		if isinstance(self.value, str):
			self.value = self.value.decode("utf-8")
		super(Constants, self).save(*args, **kwargs)

class ProfileType(models.Model):
	code = models.CharField(max_length=10, primary_key=True)
	name = models.CharField(max_length=40)
	short_name = models.CharField(max_length=10)
	def __unicode__(self):
		return smart_unicode(self.name)

	def save(self, *args, **kwargs):
		if isinstance(self.code, str):
			self.code = self.code.decode("utf-8")
		if isinstance(self.name, str):
			self.name = self.name.decode("utf-8")
		if isinstance(self.short_name, str):
			self.short_name = self.short_name.decode("utf-8")
		super(ProfileType, self).save(*args, **kwargs)

# Факультет (ЦИСТ)
class Faculty(models.Model):

	faculty_id = models.CharField(max_length=10, primary_key=True)
	short_name = models.CharField(max_length = 40)
	full_name = models.CharField(max_length = 150)
	head = models.ForeignKey('InstructorProfile', blank = True, null = True, default = None)

	def __unicode__(self):			
		return smart_unicode(self.full_name)

	def save(self, *args, **kwargs):			
		if isinstance(self.faculty_id, str):
			self.faculty_id = self.faculty_id.decode("utf-8")
		if isinstance(self.short_name, str):
			self.short_name = self.short_name.decode("utf-8")
		if isinstance(self.full_name, str):
			self.full_name = self.full_name.decode("utf-8")
		super(Faculty, self).save(*args, **kwargs)

	class Meta:
		verbose_name = u'Факультет'
		verbose_name_plural = u'Факультеты'

# Кафедра (ЦИСТ)
class Department(models.Model):

	faculty = models.ForeignKey(Faculty)
	department_id = models.CharField(max_length = 10, primary_key = True)
	short_name = models.CharField(max_length = 40)
	full_name = models.CharField(max_length = 150)
	head = models.ForeignKey('InstructorProfile', blank = True, null = True, default = None)

	def __unicode__(self):
		return smart_unicode(self.faculty.short_name + ", " + self.full_name)

	def save(self, *args, **kwargs):			
		if isinstance(self.department_id, str):
			self.department_id = self.department_id.decode("utf-8")
		if isinstance(self.short_name, str):
			self.short_name = self.short_name.decode("utf-8")
		if isinstance(self.full_name, str):
			self.full_name = self.full_name.decode("utf-8")
		super(Department, self).save(*args, **kwargs)
	class Meta:
		verbose_name = u'Кафедра'
		verbose_name_plural = u'Кафедры'

# Направление (ЦИСТ)
class Direction(models.Model):

	faculty = models.ForeignKey('Faculty')
	direction_id = models.CharField(max_length = 10, primary_key = True)
	short_name = models.CharField(max_length=40)
	full_name = models.CharField(max_length=150)

	def __unicode__(self):
		return smart_unicode(self.full_name)

	def save(self, *args, **kwargs):			
		if isinstance(self.direction_id, str):
			self.direction_id = self.direction_id.decode("utf-8")
		if isinstance(self.short_name, str):
			self.short_name = self.short_name.decode("utf-8")
		if isinstance(self.full_name, str):
			self.full_name = self.full_name.decode("utf-8")
		super(Direction, self).save(*args, **kwargs)

	class Meta:
		verbose_name = u'Направление'
		verbose_name_plural = u'Направления'

# Специальность (ЦИСТ)
class Speciality(models.Model):

	direction = models.ForeignKey('Direction')
	speciality_id = models.CharField(max_length=10, primary_key = True)
	short_name = models.CharField(max_length=150)
	full_name = models.CharField(max_length=150)

	def __unicode__(self):
		return smart_unicode(self.full_name)

	def save(self, *args, **kwargs):			
		if isinstance(self.speciality_id, str):
			self.speciality_id = self.speciality_id.decode("utf-8")
		if isinstance(self.short_name, str):
			self.short_name = self.short_name.decode("utf-8")
		if isinstance(self.full_name, str):
			self.full_name = self.full_name.decode("utf-8")
		super(Speciality, self).save(*args, **kwargs)

	class Meta:
		verbose_name = u'Специальность'
		verbose_name_plural = u'Специальности'


# Справочник. Тип занятий: лекция, пз, лаба, консультация
class ActivityType(models.Model):	

	code = models.CharField(max_length=3, primary_key = True)
	activty_name = models.CharField(max_length = 100, blank = True, null=True)
	short_name = models.CharField(max_length=10, blank=True, null = True)
	activity_type = models.CharField(max_length = 100, blank = True, null=True)

	def __unicode__(self):
		return smart_unicode(self.activty_name)

	def save(self, *args, **kwargs):			
		if isinstance(self.code, str):
			self.code = self.code.decode("utf-8")
		if isinstance(self.short_name, str):
			self.short_name = self.short_name.decode("utf-8")
		if isinstance(self.activty_name, str):
			self.activty_name = self.activty_name.decode("utf-8")
		if isinstance(self.activity_type, str):
			self.activity_type = self.activity_type.decode("utf-8")
		super(ActivityType, self).save(*args, **kwargs)

	class Meta:
		verbose_name = u'Тип занятий'
		verbose_name_plural = u'Типы занятий'

# Дедлайн: задача или задание, привязанные ко времени. Например, ИДЗ, Реферат
# В этом отношении хранятся только названия типов заданий
class DeadlineType(models.Model):

	name = models.CharField(max_length = 40, blank = True, null = True)
	short_name = models.CharField(max_length = 4, blank = True, null = True)	

	def __unicode__(self):
		return smart_unicode(self.name)

	def save(self, *args, **kwargs):			
		if isinstance(self.name, str):
			self.name = self.name.decode("utf-8")
		if isinstance(self.short_name, str):
			self.short_name = self.short_name.decode("utf-8")		
		super(DeadlineType, self).save(*args, **kwargs)
	class Meta:
		verbose_name = u'Тип дедлайна'
		verbose_name_plural = u'Типы дедлайнов'

# Отношение Пользователь
# Имеет унаследованные атрибуты: id, username, first_name, last_name, email, is_active
# Из них мы используем только id, username, password
# Есть также другие унаследованные атрибуты, но мы к ним не обращаемся
class CustomUser(AbstractUser):
    # Данные для подтверждения по почте
	activation_key = models.CharField(max_length=100, blank=True, verbose_name = u'Ключ активации')
	key_expires = models.DateTimeField(default=timezone.now, verbose_name = u'Срок действия ключа')

	# Тип аккаунта: студент, преподаватель, админ
	#profile_type = models.CharField(choices = type_choices, default= STUDENT, max_length=10, verbose_name=u'Тип пользователя')	
	profile_type = models.ForeignKey(ProfileType, default = None, null = True, blank = True)

	# Подтверждение статуса (старосты, студента определенной группы, преподавателя)
	status_confirmed = models.BooleanField(default=False, verbose_name = u'Статус подтвержден?')		
	RUSSIAN = u'ru'
	UKRAINIAN = u'uk'
	ENGLISH = u'en-gb'

	LANGUAGE_CHOICES = (
		(RUSSIAN, u"Русский"),
		(UKRAINIAN, u"Украинский"),
		(ENGLISH, u"Английский"),
		)

	language = models.CharField(max_length = 2, choices = LANGUAGE_CHOICES, default = RUSSIAN)

	def __unicode__(self):
		return smart_unicode(self.get_full_name())

	class Meta:
		verbose_name = u'Пользователь'
		verbose_name_plural = u'Пользователи'

CustomUser._meta.get_field('username').verbose_name = u'Имя пользователя'			
CustomUser._meta.get_field('first_name').verbose_name = u'Имя'			
CustomUser._meta.get_field('last_name').verbose_name = u'Фамилия'			
CustomUser._meta.get_field('email').verbose_name = u'Адрес эл. почты'			
CustomUser._meta.get_field('is_active').verbose_name = u'Адрес эл.почты подтвержден?'			
CustomUser._meta.get_field('groups').verbose_name = u'Пользовательские группы'		
CustomUser._meta.get_field('date_joined').verbose_name = u'Дата регистрации'		
CustomUser._meta.get_field('is_superuser').verbose_name = u'Superuser?'		
CustomUser._meta.get_field('is_staff').verbose_name = u'Админ?'		
CustomUser._meta.get_field('last_login').verbose_name = u'Дата последнего посещения'		
CustomUser._meta.get_field('user_permissions').verbose_name = u'Права пользователя'		


# Публичный профиль преподавателя (берется из базы CIST)
class Instructor(models.Model):

	# Данные о преподавателе из базы CIST
	first_name = models.CharField(max_length=20, verbose_name = u'Имя')
	middle_name = models.CharField(max_length=20,
		verbose_name = u'Отчество',
		blank = True,
		null = True,
		default = None)
	last_name = models.CharField(max_length=20, verbose_name = u'Фамилия')
	cist_id = models.CharField(max_length=20,
		unique=True,
		blank=True,
		null=True,
		verbose_name = u'ID преподавателя (CIST)')	

	# Кафедра (один преподаватель может принадлежать к нескольким кафедрам)
	departments = models.ManyToManyField('Department')

	def __unicode__(self):
		return smart_unicode(self.last_name + ", " + self.first_name + " " + self.middle_name)

	def save(self, *args, **kwargs):			
		if isinstance(self.first_name, str):
			self.first_name = self.first_name.decode("utf-8")
		if isinstance(self.middle_name,str):
			self.middle_name = self.middle_name.decode("utf-8")
		if isinstance(self.last_name, str):
			self.last_name = self.last_name.decode("utf-8")
		if isinstance(self.cist_id, str):
			self.cist_id = self.cist_id.decode("utf-8")
		super(Instructor, self).save(*args, **kwargs)

	class Meta:
		verbose_name = u'Преподаватель'
		verbose_name_plural = u'Преподаватели'
		
# Отношение Профиль Преподавателя
class InstructorProfile(models.Model):
	# Связь с отношением пользователь	
	user = models.OneToOneField('CustomUser', primary_key = True, related_name="inst_profile")			

	# Номер телефона
	phone_number = models.CharField(max_length=15, verbose_name = u'Номер телефона', null = True, blank = True, default = None)

	# Отчество 
	middle_name = models.CharField(max_length = 20, verbose_name = u'Отчество', null = True, blank = True, default = None)

	pub_profile = models.ForeignKey('Instructor', null = True, blank = True, default = None, verbose_name = u'Публичный профиль')
	primary_department = models.ForeignKey('Department', null = True, blank = True, default = None) 
	primary_faculty = models.ForeignKey('Faculty',null = True, blank = True, default = None)
	can_apply_for_student_grant = models.BooleanField(default=False, verbose_name = u'Может подать на именной грант для студента?')

	def __unicode__(self):			
		return smart_unicode(self.user.last_name + u", " + self.user.first_name + u" " + self.middle_name)

	def save(self, *args, **kwargs):			
		if isinstance(self.middle_name, str):
			self.middle_name=self.middle_name.decode("utf-8")      	
		if isinstance(self.phone_number, str):
			self.phone_number=self.phone_number.decode("utf-8")      	
		super(InstructorProfile, self).save(*args, **kwargs)
	class Meta:
		verbose_name = u'Профиль преподавателя'
		verbose_name_plural = u'Профили преподавателей'

# Отношение Группа (например, КН-14-6)
class Group(models.Model):

	speciality = models.ForeignKey('Speciality', blank = True, null = True, default = None, verbose_name = u'Специальность')
	
	# Данные из CIST
	name = models.CharField(max_length=20, unique = True, verbose_name = u'Название группы')
	group_id = models.CharField(max_length=10, primary_key = True, verbose_name = u'ID группы (CIST)')
	year = models.PositiveSmallIntegerField(default = 2014, blank = True, null = True)
	schedule_update_date = models.DateField(default = None, blank = True, null = True)

	# Код подтверждения для подключения к группе, генерится автоматически
	confirmation_code = models.CharField(max_length=20,
		blank=True,
		null=True,
		default = u"",
		verbose_name = u'Код подтверждения')
	curator = models.ForeignKey('InstructorProfile', blank=True, null=True, default=None, verbose_name = u'Куратор')	

	def __unicode__(self):
		return smart_unicode(self.name)

	def save(self, *args, **kwargs):
		if isinstance(self.name, str):
			self.name=self.name.decode("utf-8")			
		super(Group, self).save(*args, **kwargs)

	class Meta:
		verbose_name = u'Группа (CIST)'
		verbose_name_plural = u'Группы (CIST)'

# Отношение Профиль Студента
class StudentProfile(models.Model):		

	# Связь с отношением пользователь	
	user = models.OneToOneField('CustomUser', primary_key = True, related_name="st_profile")			

	# Связь с отношением Группа
	group = models.ForeignKey('Group', blank = True, null= True, default = None, verbose_name=u'Группа')

	# Номер студенческого 
	student_id = models.CharField(max_length=20, verbose_name = u'Номер студенческого', blank = True, null = True, default = None)

	# Поле is_perfect имеет значение True, если пользователь староста, в противном случае - False
	is_prefect = models.BooleanField(default=False, verbose_name = u'Староста?')

	def __unicode__(self):	
		stud = smart_unicode(self.user.first_name + " " + self.user.last_name)
		return stud

	def save(self, *args, **kwargs):					
		if isinstance(self.student_id, str):
			self.student_id=self.student_id.decode("utf-8")      	
		super(StudentProfile, self).save(*args, **kwargs)

	class Meta:
		verbose_name = u'Профиль студента'
		verbose_name_plural = u'Профили студентов'
			



# Идентификационная карточка предмета (берется из базы ЦИСТа)
class Subject(models.Model):

	subject_id = models.CharField(max_length=30, primary_key = True)
	full_name = models.CharField(max_length=150, verbose_name=u'Название предмета')	
	short_name = models.CharField(max_length=40)

	def __unicode__(self):
		return smart_unicode(self.full_name)

	def save(self, *args, **kwargs):			
		if isinstance(self.subject_id, str):
			self.subject_id = self.subject_id.decode("utf-8")
		if isinstance(self.short_name, str):
			self.short_name = self.short_name.decode("utf-8")
		if isinstance(self.full_name, str):
			self.full_name = self.full_name.decode("utf-8")
		super(Subject, self).save(*args, **kwargs)

	class Meta:
		verbose_name = u'Предмет'
		verbose_name_plural = u'Предметы'

# Группа-Семестр-Предмет
class Group_Term_Subject(models.Model):

	# Группа
	group = models.ForeignKey('Group', 
		blank = True,
		null= True,
		default = None,
		verbose_name = u'Группа')

	# Семестр
	term = models.CharField(
		max_length=2,
		blank=True,
		null=True,
		verbose_name = u'Семестр')

	# Название предмета	
	subject = models.ForeignKey('Subject',
		blank = True,
		null = True,
		verbose_name = u'Предмет')

	def __unicode__(self):
		return smart_unicode(self.subject.full_name + ", " +
			self.group.name +
			", семестр " + self.term)

	def save(self, *args, **kwargs):			
		if isinstance(self.term, str):
			self.term = self.term("utf-8")		
		super(Group_Term_Subject, self).save(*args, **kwargs)

	class Meta:
		verbose_name = u'Группа-Семестр-Предмет'
		verbose_name_plural = u'Группа-Семестр-Предмет'

# (Группа-Семестр-Предмет)-Тип_занятий-Преподаватель
class GTS_Instructor(models.Model):

	gts = models.ForeignKey('Group_Term_Subject',
		blank = True,
		null = True,
		default = None,
		verbose_name = u'ГР_ПРЕДМ_СЕМ_ПРЕП')
	activity_type = models.ForeignKey('ActivityType',
		blank = True,
		null = True,
		verbose_name = u'Тип занятий')
	instructor = models.ForeignKey('Instructor',
		blank=True,
		null=True,
		default = None,
		verbose_name = u'Преподаватель')	
	def group():
		return self.gts.group

	def __unicode__(self):
		return smart_unicode(
			self.gts.subject.full_name + ", " +
			self.gts.group.name + ", " +
			self.gts.term + ", " +
			self.activity_type.short_name + ", "
			)

	class Meta:
		verbose_name = u'Группа_Семестр_Предмет_Преподаватель'
		verbose_name_plural = u'Группа_Семестр_Предмет_Преподаватель'

class Criteria (models.Model):

	criteria_name = models.CharField(max_length=100, blank=True, null=True)
	criteria_description = models.TextField(blank=True, null=True, default = None)
	activity_type = models.ForeignKey('ActivityType')
	criteria_source = models.CharField(max_length=250, blank = True, null = True)
	# if department is None it means that there are no specific criteria for this department
	# and so we use general criteria set by the dean

	department = models.ForeignKey('Department', default = None, blank = True, null = True)
	faculty = models.ForeignKey('Faculty', default = None, blank = True, null = True)
	overall = models.BooleanField(default = False)

	active = models.BooleanField(default = True)
	active_since = models.DateField(blank = True, null = True, default = None)
	active_until = models.DateField(blank = True, null = True, default = None)

	weight = models.FloatField(default = 1)


	def __unicode__(self):
		return smart_unicode(self.criteria_name)

	def save(self, *args, **kwargs):			
		if isinstance(self.criteria_name, str):
			self.criteria_name = self.criteria_name.decode("utf-8")
		if isinstance(self.criteria_description, str):
			self.criteria_description = self.criteria_description.decode("utf-8")	
		if isinstance(self.criteria_source, str):
			self.criteria_source = self.criteria_source.decode("utf-8")	
		super(Criteria, self).save(*args, **kwargs)

	class Meta:
		verbose_name = u'Критерий'
		verbose_name_plural = u'Критерии'

# ((Группа-Семестр-Предмет)-Тип_занятий-Преподаватель)-Критерий-Пользователь-Оценка
class GTSI_Criteria_User_Vote(models.Model):

	gtsi = models.ForeignKey('GTS_Instructor',
		blank = True,
		null = True,
		default = None,
		verbose_name = u'ГР_ПРЕДМ_СЕМ_ПРЕП_КРИТ')
	criteria = models.ForeignKey('Criteria',
		blank = True,
		null = True,
		default = None,
		verbose_name = u'Критерий оценки')
	student = models.ForeignKey('CustomUser',
		blank = True,
		null = True,
		default = None)
	vote = models.PositiveSmallIntegerField(blank = True,
		null = True,
		default = None)	
	
	def __unicode__(self):
		return smart_unicode(
			self.gtsi.gts.subject.full_name + ", " +
			self.gtsi.gts.group.name + "," +
			self.gtsi.gts.term + ", " +
			self.gtsi.activity_type.short_name + ", " +
			self.gtsi.instructor.last_name + ", " +
			self.criteria.criteria_name + " " +
			str(self.vote))

	class Meta:
		verbose_name = u'Оцека предмета'
		verbose_name_plural = u'Оценки предметов'

NEW = u'Н'
PROCESSED = u'О'
APPROVED = u'ОД'
REJECTED = u'ОТК'
STATUS_CHOICES = ((NEW, u'Новое'),(PROCESSED, u'Обработано'),)
PUB_CHOICES = ((NEW, u'Новое'), (APPROVED, u'Одобрено'), (REJECTED, u'Отказано'),)

class GTSI_Feedback(models.Model):

	gtsi = models.ForeignKey('GTS_Instructor',
		blank = True,
		null = True,
		default = None,
		verbose_name = u'ГР_ПРЕДМ_СЕМ_ПРЕП_КРИТ')
	student = models.ForeignKey('CustomUser',
		blank = True,
		null = True,
		default = None)

	feedback = models.TextField()

	is_complaint = models.BooleanField(default = False)
	timestamp = models.DateField(auto_now_add=True, blank=True)
	complaint_status = models.CharField(max_length = 10,
		choices = STATUS_CHOICES,
		default = NEW)
	pub_status = models.CharField(max_length=10,
		choices = PUB_CHOICES,
		default = NEW)

	def save(self, *args, **kwargs):			
		if isinstance(self.feedback, str):
			self.feedback = self.feedback.decode("utf-8")		
		super(GTSI_Feedback, self).save(*args, **kwargs)



# ((Группа-Семестр-Предмет)-Тип_занятий-Преподаватель)-Дата_лекции-Название лекции
class GTSI_Lecture(models.Model):

	gtsi = models.ForeignKey('GTS_Instructor',
		blank = True,
		null = True,
		default = None,
		verbose_name = u'ГР_ПРЕДМ_СЕМ_ПРЕП_ЛЕК')
	lecture_start_time = models.DateField(blank=True,
		null = True,
		default = None)
	lecture_name = models.CharField(max_length = 150,
		null = True,
		blank = True,
		default = None)
	number_pair = models.PositiveSmallIntegerField(default = None, 
		null = True, blank = True)

	def __unicode__(self):
		return smart_unicode(
			self.gtsi.gts.subject.full_name + ", " +
			self.gtsi.activity_type.short_name + ", " +
			self.gtsi.gts.group.name + ", " +
			self.gtsi.gts.term + ", " +						
			self.lecture_start_time.strftime('%Y-%m-%d'))

	def save(self, *args, **kwargs):			
		if isinstance(self.lecture_name, str):
			self.lecture_name = self.lecture_name.decode("utf-8")		
		super(GTSI_Lecture, self).save(*args, **kwargs)

	class Meta:
		verbose_name = u'Лекция'
		verbose_name_plural = u'Лекции'



class GTSIL_Feedback(models.Model):

	gtsil = models.ForeignKey('GTSI_Lecture',
		blank = True,
		null = True,
		default = None)
	student = models.ForeignKey('CustomUser',
		blank = True,
		null = True,
		default = None)

	feedback = models.TextField()

	is_complaint = models.BooleanField(default = False)
	timestamp = models.DateField(auto_now_add=True, blank=True)

	complaint_status = models.CharField(max_length = 10,
		choices = STATUS_CHOICES,
		default = NEW)
	pub_status = models.CharField(max_length=10,
		choices = PUB_CHOICES,
		default = NEW)

	def save(self, *args, **kwargs):			
		if isinstance(self.feedback, str):
			self.feedback = self.feedback.decode("utf-8")		
		super(GTSIL_Feedback, self).save(*args, **kwargs)



class  GTSI_Assignment(models.Model):

	gtsi = models.ForeignKey('GTS_Instructor',
		blank = True,
		null = True,
		default = None,
		verbose_name = u'ГР_ПРЕДМ_СЕМ_ПРЕП_ЛЕК')
	assignment_type = models.CharField(max_length=20)
	assignment_name = models.CharField(max_length=20)
	assignment_description = models.TextField()
	estimated_workload = models.FloatField()

	def save(self, *args, **kwargs):
		if isinstance(self.assignment_type, str):
			self.assignment_type = self.assignment_type.decode("utf-8")
		if isinstance(self.assignment_name, str):
			self.assignment_name = self.assignment_name.decode("utf-8")		
		if isinstance(self.assignment_description, str):
			self.assignment_description = self.assignment_description.decode("utf-8")

		super(GTSI_Assignment, self).save(*args,**kwargs)


class GTSIA_Feedback(models.Model):

	gtsia = models.ForeignKey('GTSI_Assignment',
		blank = True,
		null = True,
		default = None)
	student = models.ForeignKey('CustomUser',
		blank = True,
		null = True,
		default = None)

	feedback = models.TextField()

	is_complaint = models.BooleanField(default = False)



	def save(self, *args, **kwargs):			
		if isinstance(self.feedback, str):
			self.feedback = self.feedback.decode("utf-8")		
		super(GTSIA_Feedback, self).save(*args, **kwargs)


class GTSIA_Criteria_User_Vote(models.Model):
	gtsia = models.ForeignKey('GTSI_Assignment',
		blank = True,
		null = True, 
		default = None)
	student = models.ForeignKey('CustomUser',
		blank = True,
		null = True,
		default = None)
	criteria = models.ForeignKey('Criteria',
		blank = True,
		default = None,
		null = True)
	vote = models.PositiveSmallIntegerField(
		blank = True,
		null = True, 
		default = None)


		
# (((Группа-Семестр-Предмет)-Тип_занятий-Преподаватель)-Дата_лекции-Название_лекции)-Критерий-Пользователь-Оценка
class GTSIL_Criteria_User_Vote(models.Model):

	gtsil = models.ForeignKey('GTSI_Lecture',
		blank = True,
		null = True,
		default = None)
	criteria = models.ForeignKey('Criteria',
		blank = True,
		null = True,
		default = None,
		verbose_name = u'Критерий оценки')
	student = models.ForeignKey('CustomUser',
		blank = True,
		null = True,
		default = None)
	vote = models.PositiveSmallIntegerField(blank = True,
		null = True,
		default = None)	

	def __unicode__(self):
		return smart_unicode(self.vote)
	def group():
		return self.gtsil.gtsi.gts.group
			

	class Meta:
		verbose_name = u'Оценка лекции'
		verbose_name_plural = u'Оценки лекций'

class Announcement(models.Model):
	group = models.ManyToManyField('Group')
	author = models.ForeignKey('CustomUser',
		blank = True,
		null = True,
		default = None)
	title = models.CharField(
		max_length=100,
		blank = True,
		null = True,
		default = u'Объявление')
	text = models.TextField()
	date = models.DateField(null = True,
		blank = True,
		default = None)

	def __unicode__(self):
		return smart_unicode(self.title)

	def save(self, *args, **kwargs):			
		if isinstance(self.title, str):
			self.title = self.title.decode("utf-8")
		if isinstance(self.text, str):
			self.text = self.text.decode("utf-8")		
		super(Announcement, self).save(*args, **kwargs)

	class Meta:
		verbose_name = u'Объявление'
		verbose_name_plural = u'Объявления'

class Deadline(models.Model):
	gtsi = models.ForeignKey('GTS_Instructor')
	deadline_type = models.ForeignKey('DeadlineType',
		blank = True,
		null = True,
		default = None,
		verbose_name = u'Тип контроля')
	deadline = models.DateField(
		blank = True,
		null = True,
		default = None)
	description = models.TextField()
	title = models.CharField(max_length = 40)

	def __unicode__(self):
		return smart_unicode(
			self.deadline_type.name + ", " +
			self.gtsi.gts.subject.full_name + ", " +			
			self.deadline.strftime('%Y-%m-%d'))

	def save(self, *args, **kwargs):			
		if isinstance(self.description, str):
			self.description = self.description.decode("utf-8")
		if isinstance(self.title, str):
			self.title = self.title.decode("utf-8")		
		super(Deadline, self).save(*args, **kwargs)

	class Meta:
		verbose_name = u'Дедлайн'
		verbose_name_plural = u'Дедлайны'

class Question(models.Model):

	student = models.ForeignKey('StudentProfile',
		verbose_name = u'Автор вопроса')
	gtsi = models.ForeignKey('GTS_Instructor')
	title = models.CharField(
		max_length = 40,
		null = True,
		blank = True,
		default = u'Вопрос',
		verbose_name = u'Вопрос')
	text = models.TextField()
	private = models.BooleanField(default = True)
	date = models.DateTimeField(
		null = True,
		blank = True,
		default = None)

	def __unicode__(self):
		return smart_unicode(self.title)

	def save(self, *args, **kwargs):			
		if isinstance(self.title, str):
			self.title = self.title.decode("utf-8")
		if isinstance(self.text, str):
			self.text = self.text.decode("utf-8")		
		super(Question, self).save(*args, **kwargs)

	class Meta:
		verbose_name = u'Вопрос'
		verbose_name_plural = u'Вопросы'

class Answer(models.Model):
	question = models.ForeignKey('Question',
		related_name = "answer")
	date = models.DateField(
		null = True,
		blank = True,
		default = None)
	text = models.TextField()
	author = models.ForeignKey(CustomUser,
		blank = True,
		null = True,
		default = None)	

	def __unicode__(self):
		return smart_unicode("Re: " + self.question.title)

	def save(self, *args, **kwargs):			
		if isinstance(self.text, str):
			self.text = self.text.decode("utf-8")		
		super(Answer, self).save(*args, **kwargs)

	class Meta:
		verbose_name = u'Ответ'
		verbose_name_plural = u'Ответы'
"""
class UserSettings(models.Model):
	language = models.CharField(default = "Russian")

"""


class AfterLectureSurvey(models.Model):
	# id should be formed as: group_term_subject_instructor_startday_numberpair
	als_id = models.CharField(primary_key = True, max_length=200)
	gtsil = models.ManyToManyField(GTSI_Lecture)
	survey_name = models.CharField(max_length = 200)
	description = models.TextField()
	due_date = models.DateTimeField('Due date')
	author = models.ForeignKey(InstructorProfile, null = True, blank = True, default = None)
	def save(self, *args, **kwargs):			
		if isinstance(self.als_id, str):
			self.als_id = self.als_id.decode("utf-8")		
		if isinstance(self.survey_name, str):
			self.survey_name = self.survey_name.decode("utf-8")	
		if isinstance(self.description, str):
			self.description = self.description.decode("utf-8")										
		super(AfterLectureSurvey, self).save(*args, **kwargs)	
	def __unicode__(self):
		return smart_unicode(self.survey_name)		
	class Meta:
		verbose_name = u'Опрос после лекции'
		verbose_name_plural = u'Опросы после лекции'

class AfterLectureQuestion(models.Model):
	# id should be formed as: survery_id + number of question
	alq_id = models.CharField(primary_key = True, max_length=200)
	survey = models.ForeignKey(AfterLectureSurvey)
	question_text = models.CharField(max_length=200)
	weight = models.IntegerField(default=1)
	def save(self, *args, **kwargs):			
		if isinstance(self.alq_id, str):
			self.alq_id = self.alq_id.decode("utf-8")		
		if isinstance(self.question_text, str):
			self.question_text = self.question_text.decode("utf-8")							
		super(AfterLectureQuestion, self).save(*args, **kwargs)	
	def __unicode__(self):
		return smart_unicode(self.question_text)		
	class Meta:
		verbose_name = u'Вопрос для опроса'
		verbose_name_plural = u'Вопросы для опросов'

class AfterLectureChoice(models.Model):
	# id should be formed as: question_id + number of answer
	alc_id = models.CharField(primary_key = True, max_length=200)
	question = models.ForeignKey(AfterLectureQuestion, on_delete=models.CASCADE, blank = False, null = False)
	choice_text = models.CharField(max_length=500)
	votes = models.IntegerField(default=0)
	correct = models.BooleanField(default = False)
	def save(self, *args, **kwargs):			
		if isinstance(self.alc_id, str):
			self.alc_id = self.alc_id.decode("utf-8")		
		if isinstance(self.choice_text, str):
			self.choice_text = self.choice_text.decode("utf-8")					
		super(AfterLectureChoice, self).save(*args, **kwargs)	
	def __unicode__(self):
		return smart_unicode(self.choice_text)		
	class Meta:
		verbose_name = u'Вариант ответа для опроса'
		verbose_name_plural = u'Варианты ответа для опроса'

class AfterLectureStudentResult(models.Model):
	# id should be formed as: student id + answer id
	student = models.ForeignKey(StudentProfile)
	answer = models.ForeignKey(AfterLectureChoice, blank = False, null = False)

class AfterLectureStudentResults(models.Model):
	student = models.ForeignKey(StudentProfile, blank = False, null = False)
	survey = models.ForeignKey(AfterLectureSurvey)
	correct_answers = models.IntegerField(default = 0)


class StudentRatingData(models.Model):
	first_name = models.CharField(max_length=50, verbose_name = u"Имя")
	last_name = models.CharField(max_length=50, verbose_name = u"Фамилия")
	middle_name = models.CharField(max_length=50, verbose_name = u"Отчество")

	faculty = models.ForeignKey(Faculty, blank = True, null= True, default = None,verbose_name = u"Факультет")
	department = models.ForeignKey(Department, blank = True, null= True, default = None, verbose_name = u"Кафедра")
	group = models.ForeignKey(Group, blank = True, null= True, default = None, verbose_name = u"Группа")
	
	faculty_txt = models.CharField(max_length = 100, null= True, default = None, verbose_name = u"Факультет")
	department_txt = models.CharField(max_length = 100, null= True, default = None, verbose_name = u"Кафедра")
	group_txt = models.CharField(max_length = 100, null= True, default = None, verbose_name = u"Группа")

	#Средний балл сданных экзаменов и зачетов (100 max).
	gpa = models.FloatField(verbose_name = u"Средний балл")
	#Знание иностранных языков, подтвержденное сертификатами IELTS, TOEFL (50k);
	english = models.PositiveSmallIntegerField(verbose_name = u"Знание языков", default = 0)
	# Программные продукты; устройства и макеты, патенты на изобретения и полезные модели (20k);
	patents = models.PositiveSmallIntegerField(verbose_name = u"Патенты, изобретения", default = 0)
	# Призеры олимпиад и выставок, подтвержденные дипломами (10k);
	competitions = models.PositiveSmallIntegerField(verbose_name = u"Олимпиады", default = 0)
	# Журнальные статьи зарубежные (20k); 
	articles_int = models.PositiveSmallIntegerField(verbose_name = u"Статьи зарубежные", default = 0)
	#Журнлаьные статье национальные (10k);
	articles_nat = models.PositiveSmallIntegerField(verbose_name = u"Статьи национальные", default = 0)
	# Доклады на конференциях и семинарах зарубежные (15k); 
	presentation_int = models.PositiveSmallIntegerField(verbose_name = u"Доклады зарубежные", default = 0)
	#национальные (5k);
	presentation_nat = models.PositiveSmallIntegerField(verbose_name = u"Доклады национальные", default = 0)
	# Спортивные и культурные мероприятия (5k)
	sport = models.PositiveSmallIntegerField(verbose_name = u"Спортивные мероприятия", default = 0)
	# Наукометрия в Scopus, WoS (20k),  k – количество публикаций.
	scopus = models.PositiveSmallIntegerField(verbose_name = u"Публикации в Scoups", default = 0)
	# Интегральная оценка
	integral = models.FloatField(default=0, verbose_name = u"Интегральная оценка")
	# Кто добавил
	added_by = models.ForeignKey(InstructorProfile, blank = True, null = True, default = None, verbose_name = "Кто добавил")
	# Идентификатор рейтинга
	rating_id = models.IntegerField(verbose_name=u"Идентификатор рейтинга", default = 2)
	# Начало проведения рейтинга
	start_date = models.DateField(verbose_name = u"Начало конкурса", blank = True, null=True, default = None)
	# Конец проведения рейтинга
	end_date = models.DateField(verbose_name = u"Начало конкурса", blank = True, null=True, default = None)

	def save(self, *args, **kwargs):			
		if isinstance(self.first_name, str):
			self.first_name = self.first_name.decode("utf-8")		
		if isinstance(self.last_name, str):
			self.last_name = self.last_name.decode("utf-8")
		if isinstance(self.middle_name, str):
			self.middle_name = self.middle_name.decode("utf-8")	
		if isinstance(self.faculty_txt, str):
			self.faculty_txt = self.faculty_txt.decode("utf-8")					
		if isinstance(self.department_txt, str):
			self.department_txt = self.department_txt.decode("utf-8")					
		if isinstance(self.group_txt, str):
			self.group_txt = self.group_txt.decode("utf-8")									
		super(StudentRatingData, self).save(*args, **kwargs)	
	class Meta:
		verbose_name = u'Рейтинг для гранта'
		verbose_name_plural = u'Рейтинги для гранта'

class Instructor_Subject_SHI(models.Model):
	instructor = models.ForeignKey(Instructor)
	subject = models.ForeignKey(Subject)
	shi = models.FloatField(verbose_name = u"Индекс здоровья предмета")
	year_start = models.PositiveSmallIntegerField(default = 2015)
	year_end = models.PositiveSmallIntegerField(default = 2016)

class GTSIL_Result(models.Model):
	lecture = models.ForeignKey('GTSI_Lecture',
		blank = True,
		null = True,
		default = None)
	criteria = models.ForeignKey('Criteria',
		blank = True,
		null = True,
		default = None,
		verbose_name = u'Критерий оценки')
	group = models.ForeignKey(Group)
	term = models.PositiveSmallIntegerField()
	subject = models.ForeignKey(Subject)
	instructor = models.ForeignKey(Instructor)
	criteria = models.ForeignKey(Criteria)
	sum_votes = models.IntegerField()
	votes = models.IntegerField()
	voters = models.IntegerField()
	average = models.FloatField()
	def save(self, *args, **kwargs):
		self.average = self.sum_votes / float(self.votes)
		super(GTSIL_Result,self).save(*args, **kwargs)

class GTSIL_Results_Distribution(models.Model):
	lecture = models.ForeignKey('GTSI_Lecture',
		blank = True,
		null = True,
		default = None)
	criteria = models.ForeignKey('Criteria',
		blank = True,
		null = True,
		default = None,
		verbose_name = u'Критерий оценки')
	group = models.ForeignKey(Group)
	term = models.PositiveSmallIntegerField()
	subject = models.ForeignKey(Subject)
	instructor = models.ForeignKey(Instructor)
	criteria = models.ForeignKey(Criteria)
	fully_disagree_count = models.IntegerField()
	disagree_count = models.IntegerField()
	not_sure_count = models.IntegerField()
	agree_count = models.IntegerField()
	fully_agree_count = models.IntegerField()
	voters = models.IntegerField()
