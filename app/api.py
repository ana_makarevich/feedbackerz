#!/usr/bin/env python
# -*- coding:utf-8 -*-

from __future__ import unicode_literals

from django.http import HttpRequest, JsonResponse
from django.views.generic import View
from django.contrib.auth.models import Group
from app.models import CustomUser
from rest_framework import viewsets
from app.serializers import UserSerializer, GroupSerializer

class GTSILFeedback(View):
    def get(self, request, *args, **kwargs):
        """
        user = request.user
        request.encoding = 'utf-8'        
        context = RequestContext(request)        
        #from path
        gtsil_id = self.kwargs['gtsil_id']
        self.request.query_params.get('job-def-id', None)
        gtsil = GTSI_Lecture.objects.get(id = gtsil_id)        

        #gtsil_data = {}
        context_dict['gtsil'] = gtsil

        criteria = get_criteria(user, gtsil.gtsi, False)
        context_dict['criteria'] = criteria
        voted = False        
        if GTSIL_Criteria_User_Vote.objects.filter(gtsil = gtsil, student = user).exists():
            voted = True
        context_dict['voted'] = voted
        """
        context_dict = {}
        context_dict['voted_flag'] = False 
        context_dict['instructor'] = u"Обризан, Владимир Игоревич"
        context_dict['subject'] = u'Swagger и дети'
        context_dict['activity_date'] = u'20-02-2016'
        context_dict['feedback_text'] = u'Заполните эту форму, если вы хотите добавить что-то'
        context_dict['complaint_text'] = u'Поставьте галочку, если это жалоба'
        context_dict['activity_type'] = u'Лк'
        #context_dict['criteria_labels'] = {1: u'Вообще неверно', 2: u'Скорее нет, чем да', 3: u'В какой-то степени', 4: u'Скорее да, чем нет', 5: u'Полностью согласен' }
        criteria1 = {'criteria_id': 1, 'criteria_name': u'Мне была понятна цель лекции', 'criteria_text': u'Преподаватель объяснил цель лекции и то, как изучаемая тема вписывается в цели курса', 'criteria_labels': {1: u'Вообще неверно', 2: u'Скорее нет, чем да', 3: u'В какой-то степени', 4: u'Скорее да, чем нет', 5: u'Полностью согласен' }}
        criteria2 = {'criteria_id': 2, 'criteria_name': u'Я захотел разобраться в теме', 'criteria_text': u'Преподавателю удалось вызвать интерес к теме и желание вникнуть в нее. Он дал понять, что если я приложу усилия, то смогу во всем разобраться.', 'criteria_labels': {1: u'Вообще неверно', 2: u'Скорее нет, чем да', 3: u'В какой-то степени', 4: u'Скорее да, чем нет', 5: u'Полностью согласен' }}
        context_dict['criteria'] = [criteria1, criteria2]
        return JsonResponse(context_dict)

class Subjects(View)        :
    def get(self, request, *args, **kwargs):        
        subject1 = {'gtsil_id': 1, 'label': u'ТКП, Мищеряков, Лк'}        
        subject2 = {'gtsil_id': 2, 'label': u'ЧМ, Калита, Лк'}
        subject3 = {'gtsil_id': 3, 'label': u'ТВ, Гребенник, Лк'}
        subdat1 = {'time': "18-02-2016", 'subjects': [subject1, subject3]}
        subdat2 = {'time': "19-02-2016", 'subjects': [subject2]}
        activities = [subdat1, subdat2]
        context_dict={}
        context_dict['activities'] = activities
        return JsonResponse(context_dict)



class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = CustomUser.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
