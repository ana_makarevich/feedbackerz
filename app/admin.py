#!/usr/bin/env python
# -*- coding:utf-8 -*-

# Стандартные библиотеки
from __future__ import unicode_literals

# Библиотеки Джанго
from django.contrib import admin
#from django.contrib.auth.models import User

# Локальные импорты
from .models import (Group, StudentProfile, InstructorProfile, CustomUser, 
	Faculty, Direction, Speciality,
	ActivityType, DeadlineType, Instructor, Subject, Group_Term_Subject,
	Department, GTS_Instructor, GTSI_Criteria_User_Vote, GTSI_Lecture,
	GTSIL_Criteria_User_Vote, Announcement, Deadline, Question, Answer,
	ProfileType, Criteria, GTSIL_Feedback, GTSI_Feedback, Constants, AfterLectureSurvey, 
	AfterLectureQuestion, AfterLectureChoice, AfterLectureStudentResult, 
	AfterLectureStudentResults, StudentRatingData, GTSIL_Result,
	GTSIL_Results_Distribution)

#Register the database model so it will be visible in the Admin site


class ProfileTypeAdmin(admin.ModelAdmin):
	fields = (
		'code',
		'name',
		'short_name',
		)
class StudentInline(admin.StackedInline):
    model = StudentProfile    
    extra = 0
    #can_delete = False
    #verbose_name_plural = 'user'


class InstructorInline(admin.StackedInline):
	model = InstructorProfile
	extra = 0
		
class UserAdmin(admin.ModelAdmin):					

	list_display = (
		'username',
		'f_name',
		'email',
		'status_confirmed',
		'is_active',
		'profile_type',
		)

	list_filter = (
		'profile_type',
		)	

	readonly_fields = (
		'date_joined',
		'last_login',
		'key_expires',
		)			

	def f_name(self, obj):
		return ("%s %s" % (obj.first_name, obj.last_name))		

	f_name.short_description = u'Полное имя'	

class GroupAdmin(admin.ModelAdmin):

	list_display = (
		'name',
		'group_id',
		'confirmation_code',
		'curator',
		'schedule_update_date'
		)	
	search_fields = (
		'name',
		'group_id',
		'curator__user__last_name',
		)

class SpecialityInline(admin.StackedInline):
	model = Speciality

class DirectionInline(admin.StackedInline):
	model = Direction	
	inlines = [SpecialityInline]

class FacultyAdmin(admin.ModelAdmin):
	list_display = (
		'full_name',
		'short_name',
		'faculty_id',
		)
	inlines = [DirectionInline]

class DirectionAdmin(admin.ModelAdmin):
	list_display = (
		'full_name',
		'short_name',
		'faculty',
		'direction_id',
		)
	inlines = [SpecialityInline]

class SpecialityAdmin(admin.ModelAdmin):
	list_display = (
		'full_name',
		'short_name',
		'direction',
		'speciality_id',
		)		

class StudentAdmin(admin.ModelAdmin):	
	list_display = (
		'user',
		'group',
		'is_prefect',
		'email',
		)
	search_fields = (
		'user__last_name',
		'user__email',
		)	
	list_filter = (
		'is_prefect',
		'group__speciality'
		)	
	def email(self, obj):
		return ("%s" % (obj.user.email))

class SubjectAdmin(admin.ModelAdmin):
	list_display = (
		'full_name',
		'short_name',
		'subject_id',
		)	
	search_fields = (
		'full_name',
		'short_name',
		'subject_id'
		)	

class InstructorAdmin(admin.ModelAdmin):
	list_display = (
		'last_name',
		'first_name',
		'middle_name',
		'cist_id',
		)
	search_fields = (
		'last_name',
		)
	def f_name(self,obj):
		return ("%s, %s %s" % (obj.last_name, obj.first_name, obj.middle_name))

class DepartmentAdmin(admin.ModelAdmin):
	list_display = (
		'full_name',
		'faculty',		
		'short_name',
		'department_id',
		)
	list_filter = (
		'faculty__full_name',
		'full_name',
		)

class Group_Term_SubjectAdmin(admin.ModelAdmin):
	list_display = (
		'group',
		'term',
		'subject'
		)
	list_filter = (
		'subject',
		)

class GTS_InstructorAdmin(admin.ModelAdmin):
	list_display = (
		'id',		
		'subject',
		'instructor',
		'activity_type',
		'group',
		'term',			
		)
	search_fields = (
		'gts__subject__full_name',
		'gts__group__name',
		)
	def gts(self,obj):
		return ("%s, %s, %s" % (obj.gts.group.name, obj.gts.term, obj.gts.subject.full_name))
	def group(self, obj):
		return ("%s" % (obj.gts.group.name))
	def term(self, obj):
		return ("%s" % (obj.gts.term))
	def subject(self, obj):
		return ("%s" % (obj.gts.subject.full_name))


class GTSI_Criteria_User_VoteAdmin(admin.ModelAdmin):
	list_display = (
		'gtsi',
		'criteria',
		'student',
		'vote',
		)	
	
class GTSI_LectureAdmin(admin.ModelAdmin):
	list_display = (
		'id',
		'group',
		'term',
		'subject',
		'activity_type',
		'instructor',
		'lecture_start_time',
		'lecture_name',
		'number_pair',
		)	
	search_fields = (
		'gtsi__instructor__last_name',
		'gtsi__gts__group__name',
		'gtsi__gts__subject__full_name',
		'gtsi__activity_type__activty_name',
		'id',
		)	
	def group(self,obj):
		return ("%s" % (obj.gtsi.gts.group.name))
	def term(self, obj):
		return ("%s" % (obj.gtsi.gts.term))
	def subject(self, obj):
		return ("%s" % (obj.gtsi.gts.subject.full_name))
	def instructor(self, obj):
		if not obj.gtsi.instructor is None:
			return ("%s, %s %s" % (obj.gtsi.instructor.last_name, obj.gtsi.instructor.first_name, obj.gtsi.instructor.middle_name))
	def activity_type(self, obj):
		return ("%s" % (obj.gtsi.activity_type.activty_name))

class GTSIL_Criteria_User_VoteAdmin(admin.ModelAdmin):
	list_display = (
		'gtsil',
		'criteria',
		'student',
		'vote',
		)
class AnnouncementAdmin(admin.ModelAdmin):
	list_display = (
		'title',
		'author',
		'date',
		)
class DeadlineAdmin(admin.ModelAdmin):
	list_display = (
		'gtsi',
		'deadline_type',
		'deadline',
		'title',
		)
	list_filter = (
		'deadline_type',
		)
class AnswerInline(admin.StackedInline):
	model = Answer
	extra = 0

class QuestionAdmin(admin.ModelAdmin):
	list_display = (		
		'title',
		'to_whom',
		'student',		
		'date',
		)	
	list_filter = (
		'gtsi__instructor',
		'student',
		)
	search_fields = (
		'title',
		)
	def to_whom(sefl, obj):
		return ("%s %s" % (obj.gtsi.instructor.first_name, obj.gtsi.instructor.last_name))		
	to_whom.short_description = u'Адресат'	
	inlines = [AnswerInline]	

class CriteriaAdmin(admin.ModelAdmin):
	list_display = (
		'criteria_name',
		'activity_type',
		'overall',
		'faculty',
		'department',
		'id',
		)
class StudentRatingAdmin(admin.ModelAdmin):
	list_display = (
		'first_name',
		'last_name',
		'gpa',
		'integral',
		'faculty',
		'faculty_txt',
		'department',
		'department_txt',
		)
class ConstantsAdmin(admin.ModelAdmin):
	list_display = (
		'key',
		'value',)

class AfterLectureAnswerInline(admin.StackedInline):
	model = AfterLectureChoice
	#inlines = [SpecialityInline]

class AfterLectureQuestionAdmin(admin.ModelAdmin):
	inlines = [AfterLectureAnswerInline]	

class GTSILResultAdmin(admin.ModelAdmin):
	list_display = (
		'lecture',
		'criteria',
		'average',
		)
class GTSILResultDistributionAdmin(admin.ModelAdmin):
	list_display = (
		'lecture',
		'fully_disagree_count',
		'disagree_count',
		'not_sure_count',
		'agree_count',
		'fully_agree_count',
		'criteria',
		)
# Re-register UserAdmin
admin.site.register(CustomUser, UserAdmin)
admin.site.register(Direction, DirectionAdmin)
admin.site.register(Speciality, SpecialityAdmin)
admin.site.register(StudentProfile, StudentAdmin)
admin.site.register(InstructorProfile)
admin.site.register(Subject, SubjectAdmin)
admin.site.register(Group, GroupAdmin)
admin.site.register(Faculty, FacultyAdmin)
admin.site.register(Criteria, CriteriaAdmin)
admin.site.register(ActivityType)
admin.site.register(DeadlineType)
admin.site.register(Instructor, InstructorAdmin)
admin.site.register(Group_Term_Subject, Group_Term_SubjectAdmin)
admin.site.register(Department, DepartmentAdmin)
admin.site.register(GTS_Instructor, GTS_InstructorAdmin)
admin.site.register(GTSI_Criteria_User_Vote, GTSI_Criteria_User_VoteAdmin)
admin.site.register(GTSI_Lecture, GTSI_LectureAdmin)
admin.site.register(GTSIL_Criteria_User_Vote, GTSIL_Criteria_User_VoteAdmin)
admin.site.register(Announcement, AnnouncementAdmin)
admin.site.register(Deadline, DeadlineAdmin)
admin.site.register(Question, QuestionAdmin)
admin.site.register(ProfileType, ProfileTypeAdmin)
admin.site.register(GTSIL_Feedback)
admin.site.register(Constants, ConstantsAdmin) 
admin.site.register(AfterLectureSurvey)
admin.site.register(AfterLectureQuestion, AfterLectureQuestionAdmin)
admin.site.register(AfterLectureChoice)
admin.site.register(AfterLectureStudentResult)
admin.site.register(AfterLectureStudentResults)
admin.site.register(StudentRatingData, StudentRatingAdmin)
admin.site.register(GTSIL_Result, GTSILResultAdmin)
admin.site.register(GTSIL_Results_Distribution, GTSILResultDistributionAdmin)
admin.site.register(GTSI_Feedback)