/*
    Необходим jQuery и jQuery.MaskedInput.
    Скрипт используется на странице регистрации.
*/

// Регулярные выражения для проверки значений полей. Должны совпадать с таковыми на стороне сервера.
var name_regex = /^(([A-Za-z]+)([ ]?([ ]?[\-|\.|\'|\`][ ]?)?)([A-Za-z]+))$|^(([А-ЯА-яІіЇїЄєЁё]+)([ ]?([ ]?[\-|\.|\'|\`][ ]?)?)([А-ЯА-яІіЇїЄєЁё]+))$/;
var scard_id_regex = /^(ХА|XA)?[ ]?[№]?[ ]?(\d{8})$/;
var phone_number_regex = /^((((00|\+)?38[\- ]?)?\(?\d{3}\)?)|(((00|\+)380[\- ]?)?\(?\d{2}\)?))([\- ]?)(\d{3}[\- ]?)(\d{2}[\- ]?)(\d{2})$/;
var username_regex = /^([A-Z]|[a-z]|[0-9]|\.|_|\-){3,30}$/;
var email_regex = /^([A-Z]|[a-z]|[0-9]|\.|_|\-)+@([A-Z]|[a-z]|[0-9]|\.|_|\-)+\.([A-Z]|[a-z]){2,4}$/;

// Словарь локализации. В будущем планируется получать его из HTML разметки, в которую он попадёт на этапе формирования разметки на стороне сервера.
var registration_form_locale_dictioanary = {
    'required_field' : 'Это необходимое поле, но оно пустое', 'unacceptable_symbols': 'Содержит недопустимые символы', 'scard_8digit' : 'Необходим код из 8ми цифр',
    'phone_number_format' : 'Номер указан в неверном формате', 'email_format' : 'Неверный формает email',
    'form_isnot_complited' : 'Не все поля заполнены верно',
    'username_isnt_available' : 'Имя пользователя уже занято',
    'email_isnt_available' : 'Email уже используется',
    'password_mismatch' : 'Пароли не совпадают'
};

var scard_id_mask = 'ХА № 99999999';
var empty_student_id = 'ХА № ________';
var student_id_replacement_regex = /ХА|XA|\ |№/g;
var phone_number_mask = '+380 (99) 999 99 99';
var empty_phone_number = '+380 (__) ___ __ __';
var phone_number_replacement_regex = /\ |\(|\)|\-/g;

$(document).ready(function()
{
    $('#phone_number').mask(phone_number_mask, { autoclear: false });
    $('#student_id').mask(scard_id_mask, { autoclear: false });
});

$('form#student_form input.form-control, form#instructor_form input.form-control').unbind().blur(function()
{   // При смене фокуса с любого input поля форм. Передаём поле в функцию-валидатор.
    validate_field(this);
});

$('form#student_form input#password_student, form#instructor_form input#password_instructor').unbind().blur(function()
{   // При переключении фокуса с полей пароля, выполняем провеку поля.
    var field_value = document.getElementById(this.id).value;
    if (field_value == null || field_value == '')
    {
        $('#' + this.id).removeClass('valid').addClass('invalid');
        custom_notification(this.id, registration_form_locale_dictioanary['required_field']);
    }
    else
    {
        $('#' + this.id).removeClass('invalid');
        hide_custom_notification(this.id);
    }
});

$('form#student_form input#password_student, form#instructor_form input#password_instructor').change(function()
{   // При изменении полей пароля, очищаем поле проверки пароля и его валидацию.
    var password_validator_id = this.id.replace('password_', 'password_validator_');
    $('#' + this.id).removeClass('valid').removeClass('invalid');
    $('#' + password_validator_id).removeClass('valid').removeClass('invalid');
    $('#' + password_validator_id).val(''); 
    hide_custom_notification(password_validator_id);
});

$('form#student_form, form#instructor_form').submit(function(e)
{   // Ловим событие отправки формы для форм инструктора и студента.
    if(is_form_valid(this.id))
    {   // Передаём форму функции для проверки всех её полей. Если всё верно, разрешаем отправку, иначе - блокируем.
        
        // Конвертация полей номера телефона и номера студенческого в стандарт.
        // var student_id = document.getElementById('student_id').value;
		// student_id = student_id.replace(student_id_replacement_regex, '');	
		// $('#student_id').val(student_id);
        // var phone_number = document.getElementById('phone_number').value;
		// phone_number = phone_number.replace(phone_number_replacement_regex, '');
		// $('#phone_number').val(phone_number);
        
        return true;
    }
    e.preventDefault();
});

function is_form_valid(form_id_)
{   // Функция для проверки формы. Принимает идентификатор формы.
    var result = true;
    $('form#' + form_id_ + ' input.form-control').each(function()
    {   // Проверяем каждый input формы валидатором с помощью логического умножения (потому, начальное значение result = true). Если что-то не так, получим false.
        var is_field_valid = validate_field(this);
        result = (result && is_field_valid);
    });
    return result;
}

function validate_field(field_)
{   // Функция-валидатор полей. Принимает объект - поле.
    switch(field_.id)
	{
		// Эти поля обрабатываются одинаково, воизбежания повторения кода, делаем множественную выборку
		case 'first_name_student':
        case 'first_name_instructor':
		case 'last_name_student':
        case 'last_name_instructor':
		{
            return get_validation_result({
                field_id_: field_.id,
                field_value_: field_.value,
                regex_: name_regex,
                regex_error_: registration_form_locale_dictioanary['unacceptable_symbols'],
                is_required_: true
            });
		}
		
		case 'middle_name_instructor':
		{
           return get_validation_result({
                field_id_: field_.id,
                field_value_: field_.value,
                regex_: name_regex,
                regex_error_: registration_form_locale_dictioanary['unacceptable_symbols'],
                is_required_: false
            });
		}
		
		case 'student_id':
		{
            if (field_.value != empty_student_id)
            {
                return get_validation_result({
                    field_id_: field_.id,
                    field_value_: field_.value,
                    regex_: scard_id_regex,
                    regex_error_: registration_form_locale_dictioanary['scard_8digit'],
                    is_required_: false
                });
            }
            
            $('#' + field_.id).removeClass('valid').removeClass('invalid');
            hide_custom_notification(field_.id);
            return true;
		}
		
		case 'phone_number':
	    {
            if (field_.value != empty_phone_number)
            {
                return get_validation_result({
                    field_id_: field_.id,
                    field_value_: field_.value,
                    regex_: phone_number_regex,
                    regex_error_: registration_form_locale_dictioanary['phone_number_format'],
                    is_required_: false
                });
            }
            
            $('#' + field_.id).removeClass('valid').removeClass('invalid');
            hide_custom_notification(field_.id);
            return true;
		}
		
		case 'username_student':
        case 'username_instructor':
		{
            if (get_validation_result({
                field_id_: field_.id,
                field_value_: field_.value,
                regex_: username_regex,
                regex_error_: registration_form_locale_dictioanary['unacceptable_symbols'],
                is_required_: true
            }))
            {
                return check_field_on_server('/username_exists/', registration_form_locale_dictioanary['username_isnt_available'], field_.id, field_.value);
            }
            return false;
		}
		
		case 'email_student':
        case 'email_instructor':
		{
            if (get_validation_result({
                field_id_: field_.id,
                field_value_: field_.value,
                regex_: email_regex,
                regex_error_: registration_form_locale_dictioanary['email_format'],
                is_required_: true
            }))
            {
                return check_field_on_server('/email_exists/', registration_form_locale_dictioanary['email_isnt_available'], field_.id, field_.value);
            }
            return false;
		}
			
		case 'password_validator_student':
        case 'password_validator_instructor':
        {
            var password_id = field_.id.replace('_validator', '');
            var password_value = document.getElementById(password_id).value;
		    var password_validator_value = document.getElementById(field_.id).value;
            
            if (password_value != password_validator_value)
            {
                $('#' + password_id).removeClass('valid').addClass('invalid');
                $('#' + field_.id).removeClass('valid').addClass('invalid');
                custom_notification(field_.id, registration_form_locale_dictioanary['password_mismatch']);   
                return false;
            }
            
            if (password_value == null || password_value == '')
            {
                $('#' + password_id).removeClass('valid').addClass('invalid');
                $('#' + field_.id).removeClass('valid').addClass('invalid');
                custom_notification(field_.id, registration_form_locale_dictioanary['required_field']);                
                return false;
            }
            $('#' + password_id).removeClass('invalid').addClass('valid');
            $('#' + field_.id).removeClass('invalid').addClass('valid');
            // hide_element_notification('field_' + field_.id);
            hide_custom_notification(field_.id);
	        return true;
		}
        
        default:
            return true;
	}
}

function get_validation_result(params_)
{   // Входные параметры - field_id, field_value, regex, regex_error, is_required.
    if ((params_.field_value_ == null || params_.field_value_ == ''))
    {
        if (params_.is_required_)
        {
            $('#' + params_.field_id_).removeClass('valid').addClass('invalid');
            custom_notification(params_.field_id_, registration_form_locale_dictioanary['required_field']);
            return false;
        }
        else
        {
            $('#' + params_.field_id_).removeClass('valid').removeClass('invalid');
            document.getElementById(params_.field_id_).style.boxShadow = '';
            hide_custom_notification(params_.field_id_);
            return true;
        }
    }
            
    if (params_.regex_.test(params_.field_value_))
    {
        $('#' + params_.field_id_).removeClass('invalid').addClass('valid');
        hide_custom_notification(params_.field_id_);
        return true;
    }
    $('#' + params_.field_id_).removeClass('valid').addClass('invalid');
    custom_notification(params_.field_id_, params_.regex_error_);
    return false;
}

function check_field_on_server(url_, err_label_, field_id_, field_value_)
{   /* Функция для проверки полей на сервере. url_ - адрес проверки, err_label_ - сообщение при ошибке,
    field_id_ и field_value_ - идентификатор и значение валидируемого поля. */
    var result = false;
    $.ajax({
        url: url_ + field_value_,
        success: function (data) {
            if (data == 'False')
            {
                $('#' + field_id_).removeClass('invalid').addClass('valid');
                hide_custom_notification(field_id_);
                result = true;
            }
            else
            {
                $('#' + field_id_).removeClass('valid').addClass('invalid');
                custom_notification(field_id_, err_label_);
                result = false;
            }
        },
        async: false
    });
    return result;
}

function custom_notification(field_id_, notification_label_)
{   /* Функция, отображающее уведомление в одном из видов (мобильный или десктопный).
    Входные параметры - идентификатор поля (field_id_) и текст уведомления (err_label_) */
    $('#field_' + field_id_).next().text(notification_label_).animate({'paddingLeft':'5px'}, 400).animate({'paddingLeft':'3px'}, 400);
}

function hide_custom_notification(field_id_)
{   // Функция, скрывающая уведомления как в мобильном виде, так и в десктопном.
    $('#field_' + field_id_).next().text('');
}