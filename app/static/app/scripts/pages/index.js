$(document).ready(function() {
    $(".news_carousel .news_item").click(function() { window.location.href = $(this).find("a.ni_full_link").first().attr("href"); });
    if (is_touch_device()){
        $.getScript(jquery_mobile_js_url, function(data, textStatus, jqxhr) {
            $("#news_carousel").swiperight(function() { $(this).carousel('prev'); });  
            $("#news_carousel").swipeleft(function() { $(this).carousel('next'); });
        });
    }
});