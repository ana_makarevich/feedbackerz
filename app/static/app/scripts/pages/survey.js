/*
    Скрипт, используемый на страницах take_survey и student_account (через AJAX) для отправки ответов на опросы.
    Так как стандартным способом сериализовать форму мы не можем, делаем это кастомно.
    Скрипт вынесен в отдельный файл не просто так. Он подгружается после загрузки контента страницы через AJAX отдельно.
    Для работы необходим jQuery. Адрес отправки должен быть указан в форме (action).
*/

// Функция, которую нужно вызывать по нажатию кнопки "Отправить" на странице.
function send_survey()
{
    hide_element_notification('btn_send_survey');
    
    var survey_answers = document.getElementsByClassName("survey_optradio");    // Выборка всех доступных на странице radio_button`ов (вариантов ответов).
    var total_answers = document.getElementById('total_answers').value;         // Количество таких элементов всего. На странице должно быть скрытое поле с value = <суммарное количество вариантов ответов>.
    
    // Подготавливаем JSON для отправки на сервер.
    var answers_json = {};
    answers_json['survey_answers'] = [];
    
    // Обрабатываем нашу выборку radio_button`ов.
    for (var i = 0; i < survey_answers.length; i++)
    {
        // Если какой-либо из них выбран, добавляем его ID (имеено это и есть код ответа) в массив JSON`а.
        if(survey_answers[i].checked == true)
        {
            answers_json['survey_answers'].push(survey_answers[i].id);
        }
    }
    
    // Проверяем, даны ли ответы навсе вопросы. Проверка выполняется по суммарному количеству вариантов ответов и текущему количеству ответов в JSON`е.
    if (answers_json['survey_answers'].length == total_answers)
    {
        // Добавляем в HEADER запроса CSRF_TOKEN.
        prepare_csrf();
        
        $.ajax({
            type: "POST",
            url: $('#survey_form').attr('action'), // Получаем адрес для отправки данных из самой формы. Необходимо, так как скрипт внешний, значит ссылку с DJANGO мы просто так не подхватим.
            data: JSON.stringify(answers_json),
            success: function(response)
            {
                // При успешной отправке, прячем форму с опросом, отображаем текст об успешной отправке данных.
                hide_element_fast('survey_form');
                show_element_fast('survey_result');
            },
            contentType: 'application/json'
        });
    }
    else
    {
        element_notification('btn_send_survey', 'Не на все вопросы выбраны ответы', 'right middle', 'info')
    }
}

function show_surveys(url)
{
    hide_element_fast('tab_navigation_container');
    hide_element_fast('subjects_list');
    hide_element_fast('alert_hint');
    hide_element_fast('instructor_ajax_panel');
    hide_element_fast('lecture_statistics_panel');
    hide_element('subject_statistics_panel');
    hide_element('student_rating_panel');
    hide_element('fac_instructors_panel');
    show_element_slow('surveys_panel');
    scroll_to('#surveys_panel');
}