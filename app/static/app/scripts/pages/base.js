function is_mobile() { return ($(window).width() < 768) };
function is_touch_device() { return ('ontouchstart' in window || navigator.maxTouchPoints) ? true : false; };
function scroll_to(element, duration) { /* $('html,body').animate({ scrollTop: $(element).offset().top }, (duration || 1000)); */ }
$(document).ready(function () {
    if(!navigator.cookieEnabled) $("#cookies_div").show();
    location.hash && $(location.hash + '.collapse').collapse('show'); // Opera Mini & Hash link Collapse fix
    // Opera Mini Dropdown fix
    if ((typeof operamini != 'undefined') && (operamini)) $('.dropdown-toggle').click(function(e){ e.preventDefault(); $(this).collapse({ toggle:true }); });
    // Lock button after click
    $('form.lock').submit(function() {
        var submit_button = $(this).find('button[type=submit]');
        submit_button.prop("disabled", true);
        submit_button.text(submit_button[0].title);
        submit_button.prop("title", "Заргрузка...");
    });
    // Custom Sidebar JS
    $("ul.custom-sidebar li > a.custom-collapse").click(function() {
        if ($(this).attr('aria-expanded') == 'true') { $(this).parent().removeClass("active"); } else { $(this).parent().addClass("active"); }
    });
    if (!is_mobile() && !is_touch_device()) { $('ul.nav li.dropdown .dropdown-toggle').click(function() { return false; }); }
});