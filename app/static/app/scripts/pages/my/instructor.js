﻿/*
    Скрипт, обеспечивающий правильную работу страницы преподавателя.
    Отвечает за динамическое поведение страницы.
    Без этого скрипта страница не может выполнять свои функции - графики не работают, отображение полей с данными тоже.
    
    Необходимы jQuery, date.js, mustanche.js.
*/

// Заготовки текстов, которые мы потом будем использовать.
var chart_alt_html = "<p>Подождите, идёт обработка данных по выбранным параметрам...<br/>Если вы ждали достаточно долго, скорее всего произошла ошибка.</p>";
var gtsil_id;
var subject_id;
var lecture_groups_json;
var lecture_criteria_json;
var subject_groups_json = [];
var subject_criteria_json = [];
var activities_json;
var chart_name;

(function ()
{   // Это автозапуск.
    // Загружаем основу API для рисования графиков.
    google.charts.load('44', {'packages':['corechart']});
})();

function on_ajax_load()
{
    show_tab_navigation_container();
}

// Это отложенный автозапуск
$(document).ready(function() {
    AjaxContent.init({containerDiv:"#tab_navigation_container", contentDiv:"div.ajax_panel"}).ajaxify_links("a.tab_navigation_link");
    $('#info_panel_title').click(function() {
        $('#info_panel').toggle();
    });

    $('#show_attach').click(function() {
        $('#attach_text').toggle();
    });
});

// Функция, вызываемая автоматически после загрузки Charts API, предназначена непосредственно для отрисовки графика.
function drawChart(chart_data, chart_container, chart_name)
{   // chard_data - json с параметрами для отрисовки графика, chart_container - obj, имя контейнера для графика, куда его засунуть, chart_name - выводимое имя графика на графике.
    var labels_label = "Оценка";
    var value_label = "Количество голосов";
    var width = chart_container.clientWidth; // Получаем ширину контейнера графика, что бы последний всегда был подходящего размера.
    var height = width / 16 * 9; // Соотношение сторон графика 16 к 9. В будущем, будет добавлено динамическое изменение пропорций в зависимости от экрана устройства.
    
    // Создаём таблицу с данными.
    var data = new google.visualization.DataTable();
    // Создаём колонки.
    data.addColumn('string', labels_label);
    data.addColumn('number', value_label);
    
    // После создания колонок, засовываем полученные данные в виде строк.
    for (var column in chart_data.values_dict)
    {
        // Пропускаем итерацию, если элемент из прототипа.
        if(!chart_data.values_dict.hasOwnProperty(column)) continue;
        data.addRows([[column.toString(), chart_data.values_dict[column]]]);
    }
    
    var temp_array = [];
    for(var x in chart_data.values_dict)
    {
        temp_array.push(chart_data.values_dict[x.toString()]);
    }
    
    var max_height = Math.max.apply(null, temp_array);

    // Задаём параметры графика. Сюда же можно будет добавить кастомные цвета, напирмер.
    var options = {
        'title': chart_name,
        'width': width,
        'height': height,
        'legend': 'left',
        vAxis:{
            title: 'Количество голосов',
            viewWindow: {
              min: 0,
              max: max_height
            },
            gridlines: {
                count: max_height + 1
            },
            format: '#'
        },
        hAxis:{
            title: 'Оценка'
        }
    };

    // Вызываем отрисовку графика, передавая наши параметры графика и данные.
    var chart = new google.visualization.ColumnChart(chart_container);
    chart.draw(data, options);
    $("#" + chart_container.id).next().html("<p class='text-center'>Всего проголосовало (человек): " + chart_data.voters + "</p>");
    // chart_container.style.visibility = "visible";
}

// Функция, получающая и рисующая график с помощью Google Charts.
function getChart(get_chart_url, json_data_to_send, chart_container, chart_name)
{   // get_chart_url - адрес, куда мы отправляем запрос для получения данных графика, json_data_to_send - Отпправляемые JSON данные, chart_container - obj, контенер для графика, chart_name - string, имя графика.
    prepare_csrf();
    
    // chart_container.style.visibility = "hidden";
    
    $.ajax({
        url: get_chart_url,
        type: 'POST',
        contentType:'application/json',
        data: JSON.stringify(json_data_to_send),
        dataType:'json',
        success: function(chart_data)
        {
            if (chart_data.voters != 0)
            {
                // Вызываем функцию отрисовки после загрузки API.
                // show_element(chart_container.id);
                google.charts.setOnLoadCallback(drawChart(chart_data, chart_container, chart_name));
            }
            else
            {
                // hide_element(chart_container.id);
                chart_container.innerHTML = "<p class='text-center'>По выбранным параметрам нет данных</p>";
            } 
        }
    });
}

// Снимаем пометки со всех групп в списке.
function uncheckCheckboxesGroup(checkboxes_name)
{   // checkboxes_name - string, имя группы чекбоксов, с которых необходимо снять пометку.
    var checkboxes = document.getElementsByName(checkboxes_name);
    for (var checkbox = 0; checkbox < checkboxes.length; checkbox++)
    {
        checkboxes[checkbox].checked = false;
    }
}

// Снимаем пометку с указанного чекбокса. Используется для снятия пометки с "Все группы" и "Всё время".
function uncheckCheckbox(checkbox_id)
{   // checkbox_id - string, id чекбокса, с которого необходимо снять пометку.
    document.getElementById(checkbox_id).checked = false;
}

// Обработка и отрисовка области статистики по лекцию.
function displayLectureStatistics(url)
{   // url - string, получаемый адрес, откуда брать данные для отображения.
    hide_element('alert_hint');
    hide_element('lecture_chart_wrapper');
    hide_element('subject_statistics_panel');
    hide_element('instructor_ajax_panel');
    hide_element('fac_instructors_panel');
    hide_element('tab_navigation_container');
    hide_element('surveys_panel');
    hide_element('student_rating_panel');
    
    // Заменяем содержимое контейнера для графиков на предупреждение о возможной ошибке. На тот случай, если новый график по какой-то причине не загрузится.
    document.getElementById("lecture_chart_container").innerHTML = chart_alt_html;
    
    // Добываем gtsil_id из переданной ссылки url_.
    var url_parse = url.split('/');
    gtsil_id = url_parse[url_parse.length - 2];
    
    // Получаем JSON по указанной ссылке url_.
    $.getJSON(url, function(data)
    {
        // alert(JSON.stringify(data));
        
        // Добавляем в заголовок панели название предмета и время.
        var dt = new Date(data.lecture_time);
        var date = dt.toLocaleDateString("uk-UA");
        // var time = dt.getUTCHours() + ":" + dt.getUTCMinutes();
        document.getElementById("lecture_name").innerHTML = data.subject + ", " + date;
        
        // Проверяем наличие голосов, если их нет, говорим об этом пользователю, отобразив скрытый div.
        if (data.voters != 0)
        {   
            // Переключаем видимость с предупреждения на панель статистики.
            hide_element('lecture_nostatistics_group');
            show_element('lecture_statistics_group');
            
            // Получаем данные по группам и критериям.
            lecture_groups_json = data.groups;
            lecture_criteria_json = data.criteria;
            
            // Очищаем список групп и критериев, оставшихся от предыдущих отображений лекций/предметов.
            document.getElementById("lecture_statistics_groups_selector").innerHTML = "";
            document.getElementById("lecture_statistics_criteria_selector").innerHTML = "";
            
            // Шаблоны для генирации элементов списков групп и критериев.
            var groups_html = ['<label for="lecture_show_group_', '"><input type="checkbox" name="lecture_show_group_checkbox" id="lecture_show_group_', '" onclick="uncheckCheckbox(\'lecture_statistics_groups_all\');"> ', '</label><br>']
            var criteria_html = ['<div class="radio"><label for="lecture_display_by_criterion_', '"><input type="radio" name="optradio" id="lecture_display_by_criterion_', '">', '</label></div>'];
            
            // При наличии передаваемых групп, в цикле выводим их, используя шаблон.
            for (var group in lecture_groups_json)
            {
                // Пропускаем итерацию, если элемент из прототипа (во избежания дублирования и появления лишних элементов).
                if(!lecture_groups_json.hasOwnProperty(group)) continue;
                // Добавляем новую группу в список групп, склеив строку из шаблона и параметров текущей группы, полученных из JSON, добавляем всё это в groups_div.
                var new_groups_html = groups_html[0] + group + groups_html[1] + group + groups_html[2] + lecture_groups_json[group].name + groups_html[3];
                document.getElementById("lecture_statistics_groups_selector").innerHTML += new_groups_html;
            }
            
            // При наличии передаваемых критериев, в цикле выводим их, используя шаблон.
            for (var criterion in lecture_criteria_json)
            {
                // Пропускаем итерацию, если элемент из прототипа.
                if(!lecture_criteria_json.hasOwnProperty(criterion)) continue;
                // Добавляем новый критерий в список, склеив строку, запихивая её в criteria_div.
                var new_criteria_html = criteria_html[0] + criterion + criteria_html[1] + 
                criterion + criteria_html[2] + lecture_criteria_json[criterion].name + criteria_html[3];
                document.getElementById("lecture_statistics_criteria_selector").innerHTML += new_criteria_html;
            }
            
            // Ставим отметку на первый критерий, по-умолчанию, так как это radio buttons, значение по-умолчанию должно быть.
            document.getElementById("lecture_display_by_criterion_0").checked = true;
            document.getElementById("lecture_statistics_groups_all").checked = true;
            chart_name = data.subject;
            
            $(window).resize(function(){
                showLectureChart();
            });
            
            showLectureChart();
        }
        else
        {
            // Если у нас ноль голосов, не отображаем ничего кроме предупреждения о том, что у нас ноль голосов.
            hide_element('lecture_statistics_group');
            show_element('lecture_nostatistics_group');
        }
    });
    
    // После отработки всего контента, отображаем содержимое, скрывая подсказку.
    show_element('lecture_statistics_panel');
    scroll_to('#lecture_statistics_panel');
}

// $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
// //   var target = $(e.target).attr("href");
  
// });

// Функция отображения данных статистики по предмету.
function displaySubjectStatistics(url)
{   // url - string, получаемый адрес, откуда брать данные для отображения.
    // Нычкуем всё ненужное - подсказку о выборе лекции в меню и статистику по лекциям.
    hide_element('alert_hint');
    // hide_element('subject_chart_container');
    hide_element('lecture_statistics_panel');
    hide_element('instructor_ajax_panel');
    hide_element('fac_instructors_panel');
    hide_element('tab_navigation_container');
    hide_element('surveys_panel');
    hide_element('student_rating_panel');
    
    // Div`ы, содержание которых мы будем менять.
    var subject_statistics_tabs_header_div = document.getElementById('subject_statistics_tabs_header');
    var subject_statistics_tabs_content_div = document.getElementById('subject_statistics_tabs_content');
    var subject_name_div = document.getElementById("subject_name");
    
    // Испорожняем содержимое заголовка и контента системы вкладок.
    subject_statistics_tabs_header_div.innerHTML = "";
    subject_statistics_tabs_content_div.innerHTML = "";
    
    // Получаем JSON по ссылке url_.
    $.getJSON(url, function(recieved_data)
    {
        subject_name_div.innerHTML = recieved_data.subject_full_name;
        subject_id = recieved_data.subject_id;
        activities_json = recieved_data.activities;
        
        // Если у нас не содержится поле error, заполняем даными вкладки.
        if (!recieved_data.error && recieved_data.activities.length != 0)
        {
            hide_element('subject_no_votes_message');
            
            for (var activity_counter in recieved_data.activities)
            {
                // Пропускаем итерацию, если элемент из прототипа (во избежания дублирования и появления лишних элементов).
                if(!recieved_data.activities.hasOwnProperty(activity_counter)) continue;
                
                // Создаём вкладку и div, привязанный к ней, содержащий контент, получаемый из JSON.
                subject_statistics_tabs_header_div.innerHTML += "<li " + ((activity_counter == 0)?"class='active'":"") + "><a data-toggle='tab' href='" + "#subject_statistics_tab_" + activity_counter + "' name='" + activity_counter + "'>" + recieved_data.activities[activity_counter].short_name + "</a></li>";
                subject_statistics_tabs_content_div.innerHTML += "<div id='subject_statistics_tab_" + activity_counter + "' class='tab-pane fade " + ((activity_counter == 0)?"in active":"") + "'></div>";
                var current_tab_content = document.getElementById("subject_statistics_tab_" + activity_counter);
                
                $(window).resize(function(){
                    showSubjectChart(0);
                });
                
                $('ul.nav-tabs li a').on('shown.bs.tab', function(e){
                    showSubjectChart(e.target.name);
                    $(window).resize(function(){
                        showSubjectChart(e.target.name);
                    });
                });
                
                var current_date = Date.today().toString('yyyy-MM-dd');
                var start_date  = Date.parseExact(recieved_data.activities[activity_counter].start_date, "dd-mm-yyyy").toString('yyyy-mm-dd');
                // alert("Min: " + start_date + '\n' + "Max: " + current_date + '\n' + 'JSON: ' + recieved_data.activities[activity_counter].start_date + '\n');
                
                // Заливаем в div контента вкладки заготовку. Разметка аналогична разметки для лекций в html.
                current_tab_content.innerHTML = "<div class='col-lg-4'><form role='form'><div class='form-group' id='subject_tab_" +
                activity_counter + "_statistics_date_period'><label for='subject_tab_" + activity_counter + 
                "_statistics_date_period'>Период:</label><br><label for='subject_tab_" + activity_counter + 
                "_from_date'>С:<input class='form-control' type='date' id='subject_tab_" + activity_counter + 
                "_from_date' min='" + start_date + "' max='" + current_date + "' value='" + start_date + 
                "' onchange='recreateGroupsAndCriteria(\"" + activity_counter + "\")';></label><label for='subject_tab_" + 
                activity_counter + "_to_date'>По:<input class='form-control' type='date' id='subject_tab_" + 
                activity_counter + "_to_date' min='" + start_date + "' max='" + current_date + "' value='" + 
                current_date + "' onchange='recreateGroupsAndCriteria(\"" + activity_counter + "\")';></label></div><div id='grcr-group_tab_" + 
                activity_counter + "'><div class='form-group'><label for='subject_statistics_groups_selector_tab_" + activity_counter + 
                "'>Группы:</label><div class='checkbox' id='subject_statistics_groups_selector_tab_" + activity_counter +
                "'></div><div class='checkbox'><label for='subject_tab_" + activity_counter + 
                "_statistics_groups_all'><input type='checkbox' id='subject_tab_" + activity_counter + 
                "_statistics_groups_all' onclick='uncheckCheckboxesGroup(\"subject_show_group_checkbox\");' style='margin-right: 5px;' checked>Все группы</label></div></div><div class='form-group'><label for='criteria_selector'>Критерии:</label><div class='form_group' id='subject_statistics_criteria_selector_tab_" + 
                activity_counter + "'></div></div><div class='form-group'><div class='btn-group'><button type='button' class='btn btn-primary' name='display' onclick='showSubjectChart(" + activity_counter + 
                "); return false;'>Показать</button></div></div></div><div id='no_votes_tab_" + activity_counter + 
                "' hidden><p>За выбранный вами период никто не голосовал.</p></div></form></div><div class='col-lg-8'><div class='panel panel-info'><div class='panel-body'>1- Совершенно не согласен, 2 - Скорее нет, чем да, 3 - Не уверен, 4 - Скорее да, чем нет, 5 - Абсолютно согласен</div></div><div class='chart_wrapper' id='subject_chart_wrapper_tab_" + 
                activity_counter + "'><div class='chart_container' id='subject_chart_container_tab_" + activity_counter + 
                "'><p>Подождите, идёт обработка данных по выбранным параметрам...<br/>Если вы ждали достаточно долго, скорее всего произошла ошибка.</p></div><div></div></div></div>";
                
                // Достаём div`ы для групп и критериев в нашей вкладке.
                var current_groups_div = document.getElementById("subject_statistics_groups_selector_tab_" + activity_counter);
                var current_criteria_div = document.getElementById("subject_statistics_criteria_selector_tab_" + activity_counter);
                
                // Получаем данные по группам и критериям для данного activity.
                var current_groups = recieved_data.activities[activity_counter].groups;
                var current_criteria = recieved_data.activities[activity_counter].criteria;
                subject_groups_json[activity_counter] = current_groups;
                subject_criteria_json[activity_counter] = current_criteria;
                
                // Очищаем список групп и критериев, оставшихся от предыдущих отображений лекций/предметов.
                current_groups_div.innerHTML = "";
                current_criteria_div.innerHTML = "";
                
                // Шаблоны для генирации элементов списков групп и критериев.
                var groups_html = ['<label for="subject_tab_' + activity_counter + '_show_group_', '"><input type="checkbox" name="subject_show_group_checkbox" id="subject_tab_' + activity_counter + '_show_group_', '" onclick="uncheckCheckbox(\'subject_tab_' + activity_counter + '_statistics_groups_all\');"> ', '</label><br>'];
                var criteria_html = ['<div class="radio"><label for="subject_tab_' + activity_counter + '_display_by_criterion_', '"><input type="radio" name="optradio" id="subject_tab_' + activity_counter + '_display_by_criterion_', '">', '</label></div>'];
                
                // При наличии передаваемых групп, в цикле выводим их, используя шаблон.
                for (var group in current_groups)
                {
                    // Пропускаем итерацию, если элемент из прототипа (во избежания дублирования и появления лишних элементов).
                    if(!current_groups.hasOwnProperty(group)) continue;
                    // Добавляем новую группу в список групп, склеив строку из шаблона и параметров текущей группы, полученных из JSON, добавляем всё это в groups_div.
                    var new_groups_html = groups_html[0] + group + groups_html[1] + group + groups_html[2] + current_groups[group].name + groups_html[3];
                    current_groups_div.innerHTML += new_groups_html;
                }
                
                // При наличии передаваемых критериев, в цикле выводим их, используя шаблон.
                for (var criterion in current_criteria)
                {
                    // Пропускаем итерацию, если элемент из прототипа.
                    if(!current_criteria.hasOwnProperty(criterion)) continue;
                    // Добавляем новый критерий в список, склеив строку, запихивая её в criteria_div.
                    var new_criteria_html = criteria_html[0] + criterion + criteria_html[1] + criterion + criteria_html[2] + current_criteria[criterion].name + criteria_html[3];
                    current_criteria_div.innerHTML += new_criteria_html;
                }
            }
            show_element('subject_statistics_tabs');
            
            for (var counter = 0; counter < recieved_data.activities.length; counter++)
            {
                // Ставим отметку на первый критерий, по-умолчанию, так как это radio buttons, значение по-умолчанию должно быть.
                document.getElementById('subject_tab_' + counter + '_display_by_criterion_0').checked = true;
                showSubjectChart(counter);
            }
        }
        else // Если у нас нет activities, то отображаем соответствующее сообщение.
        {
            hide_element('subject_statistics_tabs');
            show_element('subject_no_votes_message');
        }
    });
    
    // Отображаем подготовленную панель статистики по предмету.
    show_element('subject_statistics_panel');
    scroll_to('#subject_statistics_panel');
}

// Отобразить график лекции.
function showLectureChart()
{
    // Подготавливаем JSON для отправки.
    var form_json_data = {};
    form_json_data['gtsil_id'] = gtsil_id;
    form_json_data['show_all'] = document.getElementById("lecture_statistics_groups_all").checked ? 1 : 0;
    
    if (!form_json_data['show_all'])
    {
        form_json_data['groups'] = [];
        var input_elements = document.getElementById("lecture_statistics_groups_selector").getElementsByTagName("input");
        for(var i = 0; i < input_elements.length; i++)
        {
            if (document.getElementById("lecture_show_group_" + i).checked == true)
            {
                form_json_data['groups'].push(lecture_groups_json[i].id);
            }
        }
    }
    
    form_json_data['criteria'];
    input_elements = document.getElementById("lecture_statistics_criteria_selector").getElementsByTagName("input");
    for(var i = 0; i < input_elements.length; i++)
    {
        if (document.getElementById("lecture_display_by_criterion_" + i).checked == true)
        {
            form_json_data['criteria'] = lecture_criteria_json[i].id;
        }
    }
    
    // alert(JSON.stringify(form_json_data));
    
    getChart(get_lecture_chart_url, form_json_data, document.getElementById("lecture_chart_container"), chart_name);
    show_element('lecture_chart_wrapper');
}

// Отобразить график предмета.
function showSubjectChart(tab_id)
{   // tab_id - int, идентификатор вкладки, где будет отображён график.
    // Подготавливаем JSON для отправки.
    var form_json_data = {};
    form_json_data['subject_id'] = subject_id;
    form_json_data['activity_type_id'] = activities_json[tab_id].code;
    
    form_json_data['start_date'] = document.getElementById('subject_tab_' + tab_id + '_from_date').value;
    form_json_data['end_date'] = document.getElementById('subject_tab_' + tab_id + '_to_date').value;
    
    form_json_data['show_all'] = document.getElementById("subject_tab_" + tab_id + "_statistics_groups_all").checked ? 1 : 0;
    if (!form_json_data['show_all'])
    {
        form_json_data['groups'] = [];
        var input_elements = document.getElementById("subject_statistics_groups_selector_tab_" + tab_id).getElementsByTagName("input");
        for(var i = 0; i < input_elements.length; i++)
        {
            if (document.getElementById("subject_tab_" + tab_id + "_show_group_" + i).checked == true)
            {
                form_json_data['groups'].push(subject_groups_json[tab_id][i].id);
            }
        }
    }
    
    form_json_data['criteria'];
    input_elements = document.getElementById("subject_statistics_criteria_selector_tab_" + tab_id).getElementsByTagName("input");
    for(var i = 0; i < input_elements.length; i++)
    {
        if (document.getElementById("subject_tab_" + tab_id + "_display_by_criterion_" + i).checked == true)
        {
            form_json_data['criteria_id'] = subject_criteria_json[tab_id][i].id;
            break;
        }
    }
    
    // form_json_data = {"gtsil_id":"599","show_all":1,"criteria":1};
    
    // alert(JSON.stringify(form_json_data));
    getChart(get_subject_chart_url, form_json_data, document.getElementById("subject_chart_container_tab_" + tab_id), chart_name);
    show_element('subject_chart_wrapper_tab_' + tab_id);
}

// Пересоздание элементов выбора критериев и групп.
function recreateGroupsAndCriteria(tab_id)
{   // tab_id - int, идентификатор вкладки, с которой мы работаем.
    document.getElementById("subject_tab_" + tab_id + "_statistics_groups_all").checked = true;
    
    var json_data_to_send = {};
    
    json_data_to_send['subject_id'] = subject_id;
    json_data_to_send['activity_type_code'] = activities_json[tab_id].code;
    json_data_to_send['start_date'] = document.getElementById('subject_tab_' + tab_id + '_from_date').value;
    json_data_to_send['end_date'] = document.getElementById('subject_tab_' + tab_id + '_to_date').value;
    
    // alert(JSON.stringify(json_data_to_send));
    // json_data_to_send = {"subject_id":"3416959", "activity_type_code":"0", "start_date":"2016-02-08", "end_date":"2016-03-05"};
    
    prepare_csrf();
    
    $.ajax({
        url: get_groups_crit,
        type: 'POST',
        contentType:'application/json',
        data: JSON.stringify(json_data_to_send),
        dataType:'json',
        success: function(recieved_data)
        {
            // alert(JSON.stringify(recieved_data));
            
            if (recieved_data.no_votes == false)
            {
                hide_element_fast('no_votes_tab_' + tab_id);
                hide_element_fast('grcr-group_tab_' + tab_id);
                
                // Достаём div`ы для групп и критериев в нашей вкладке.
                var current_groups_div = document.getElementById("subject_statistics_groups_selector_tab_" + tab_id);
                var current_criteria_div = document.getElementById("subject_statistics_criteria_selector_tab_" + tab_id);
                    
                // Получаем данные по группам и критериям для данного activity.
                var current_groups = recieved_data.groups;
                var current_criteria = recieved_data.criteria;
                subject_groups_json[tab_id] = current_groups;
                subject_criteria_json[tab_id] = current_criteria;
                
                // Очищаем список групп и критериев, оставшихся от предыдущих отображений лекций/предметов.
                current_groups_div.innerHTML = "";
                current_criteria_div.innerHTML = "";
                
                // Шаблоны для генирации элементов списков групп и критериев.
                var groups_html = ['<label for="subject_tab_' + tab_id + '_show_group_', '"><input type="checkbox" name="subject_show_group_checkbox" id="subject_tab_' + tab_id + '_show_group_', '" onclick="uncheckCheckbox(\'subject_tab_' + tab_id + '_statistics_groups_all\');"> ', '</label><br>'];
                var criteria_html = ['<div class="radio"><label for="subject_tab_' + tab_id + '_display_by_criterion_', '"><input type="radio" name="optradio" id="subject_tab_' + tab_id + '_display_by_criterion_', '">', '</label></div>'];
                    
                // При наличии передаваемых групп, в цикле выводим их, используя шаблон.
                for (var group in current_groups)
                {
                    // Пропускаем итерацию, если элемент из прототипа (во избежания дублирования и появления лишних элементов).
                    if(!current_groups.hasOwnProperty(group)) continue;
                    // Добавляем новую группу в список групп, склеив строку из шаблона и параметров текущей группы, полученных из JSON, добавляем всё это в groups_div.
                    var new_groups_html = groups_html[0] + group + groups_html[1] + group + groups_html[2] + current_groups[group].name + groups_html[3];
                    current_groups_div.innerHTML += new_groups_html;
                }
                    
                // При наличии передаваемых критериев, в цикле выводим их, используя шаблон.
                for (var criterion in current_criteria)
                {
                    // Пропускаем итерацию, если элемент из прототипа.
                    if(!current_criteria.hasOwnProperty(criterion)) continue;
                    // Добавляем новый критерий в список, склеив строку, запихивая её в criteria_div.
                    var new_criteria_html = criteria_html[0] + criterion + criteria_html[1] + 
                    criterion + criteria_html[2] + current_criteria[criterion].name + criteria_html[3];
                    current_criteria_div.innerHTML += new_criteria_html;
                }
                
                document.getElementById('subject_tab_' + tab_id + '_display_by_criterion_0').checked = true;
                
                show_element_fast('grcr-group_tab_' + tab_id);
                showSubjectChart(tab_id);
            }
            else
            {
                hide_element_fast('grcr-group_tab_' + tab_id);
                hide_element_fast('subject_chart_wrapper_tab_' + tab_id);
                show_element_fast('no_votes_tab_' + tab_id);
            }
        }
    });
}

function show_fac_instructors_panel()
{
    hide_element('alert_hint');
    hide_element('lecture_statistics_panel');
    hide_element('subject_statistics_panel');
    hide_element('instructor_ajax_panel');
    hide_element('tab_navigation_container');
    hide_element('surveys_panel');
    hide_element('student_rating_panel');
    show_element_fast('fac_instructors_panel');
    scroll_to('#fac_instructors_panel');
}

function show_fac_instructor_panel(name)
{
    hide_element('alert_hint');
    hide_element('lecture_statistics_panel');
    hide_element('subject_statistics_panel');
    hide_element('fac_instructors_panel');
    hide_element('tab_navigation_container');
    hide_element('surveys_panel');
    hide_element('student_rating_panel');
    show_element_fast('instructor_ajax_panel');
    document.getElementById('fac_instructor_name').innerText = name;
    scroll_to('#instructor_ajax_panel');
}

function show_tab_navigation_container()
{
    hide_element('alert_hint');
    hide_element('lecture_statistics_panel');
    hide_element('subject_statistics_panel');
    hide_element('fac_instructors_panel');
    hide_element('instructor_ajax_panel');
    hide_element('surveys_panel');
    hide_element('student_rating_panel');
    show_element_fast('tab_navigation_container');
    scroll_to('#tab_navigation_container');
}

function show_student_rating_panel()
{
    hide_element('alert_hint');
    hide_element('lecture_statistics_panel');
    hide_element('subject_statistics_panel');
    hide_element('instructor_ajax_panel');
    hide_element('tab_navigation_container');
    hide_element('surveys_panel');
    hide_element('fac_instructors_panel');
    $("#student_rating_panel_content").load(student_rating_url + " #student_rating_panel_content", function(){
        $.getScript(student_rating_js);
    });
    show_element_fast('student_rating_panel');
    scroll_to('#student_rating_panel');
}
