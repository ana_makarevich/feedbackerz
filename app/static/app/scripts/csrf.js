/*
    Скрипт, предназначений для удовлетворения требований системы безопасности.
    Для работы необходим get_cookie.js
*/

// Функция, определяющая, нужно ли использовать CSRF защиту.
function csrfSafeMethod(method)
{   // method - string, название метода.
    // Этим HTTP методам не нужна CSRF защита, возвращаем true, если не нужна.
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

// Функция, добавляющая в заголовок запроса наш CSRF token.
function prepare_csrf()
{
	$.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain)
            {
                // Необходимо достать CSRF token из cookies.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
        }
    });
}