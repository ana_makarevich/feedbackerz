/* 
    Скрипт, отвечающий за переадресацию между страницами по истечению заданного времени.
*/

// Стандартно заданный адрес переадресации и словарик сообщений.
var redirect_link = "/";
var registration_countdown_locale_dictioanary = { 'redirect_message' : 'Вы будете перенаправлены на главную в течении', 'redirecting_unit' : 'секунд', 'redirecting_message': 'Перенаправление...' };

// Функция, что задаёт параметры переадрессации. \
function redirect_countdown(secs_, element_, redirect_link_, redirect_message_)
{   // secs_ - длительность ожидания, element_ - имя контейнера для отображения обратного отсчёта, redirect_link_ - ссылка переадрессации, redirect_message_ - уведомление о перенаправлении.
    // Если передан адрес переадресации, используем его.
	if(redirect_link_ != null)
	{
		redirect_link = redirect_link_;
	}
	
    // То же самое и с сообщением переадресации.
	if(redirect_message_ != null)
	{
		registration_countdown_locale_dictioanary["redirect_message"] = redirect_message_;
	}
	
    // Запускаем таймер.
	countdown(secs_, element_);
}

// Рекурсивная функция, которая уменьшает значение секунд каждую секунду (таймер), а при нулевом значении - выполняет переадресацию.
function countdown(secs_, element_)
{   // secs_ - длительность таймера в секундах, element_ - название контейнера, куда выводиться обратный отсчёт.
	var element = document.getElementById(element_);
	element.innerHTML = registration_countdown_locale_dictioanary["redirect_message"] + " " + secs_ + " " + registration_countdown_locale_dictioanary["redirecting_unit"] + ".";
	
	if(secs_ < 1)
	{
		clearTimeout(timer);
		element.innerHTML = registration_countdown_locale_dictioanary["redirecting_message"];
		window.location.href = redirect_link;
	}
	
	secs_--;
	var timer = setTimeout('countdown('+ secs_ +',"'+ element_ +'")', 1000);
}