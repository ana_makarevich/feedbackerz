/*
    Скрипт-интерфейс для быстрого и удобного управления видимостью элементов с анимацией.
    Используется везде, где есть интерактивность с сокрытием/отображением элементов.
    Для использования необходи jQuery.
*/

// Функция, инвертирующая текущее состояние элемента (скрыт или отображается).
function showhide_element(element_id, duration)
{   // element_id - идентификатор элемента, duration (не обязательна) - длительность анимации в мс.
    if (duration == null) { duration = 0; }
    $("#" + element_id).toggle(duration);
}

// Функция, инвертирующая текущее состояние элемента с быстрой анимацией.
function showhide_element_fast(element_id)
{
    $("#" + element_id).toggle('fast');
}

// Функция, инвертирующая текущее состояние элемента с медленной анимацией.
function showhide_element_slow(element_id)
{
    $("#" + element_id).toggle('slow');
}

// Функция, скрывающая элемент.
function hide_element(element_id, duration)
{
    if (duration == null) { duration = 0; }
    $("#" + element_id).hide(duration);
}

// Функция, скрывающая элемент с быстрой анимацией.
function hide_element_fast(element_id)
{
    $("#" + element_id).hide('fast');
}

// Функция, скрывающая элемент с медленной анимацией.
function hide_element_slow(element_id)
{
    $("#" + element_id).hide('slow');
}

// Функция, отображающая элемент.
function show_element(element_id, duration)
{
    if (duration == null) { duration = 0; }
    $("#" + element_id).show(duration);
}

// Функция, отображающая элемент с быстрой анимацией.
function show_element_fast(element_id)
{ 
    $("#" + element_id).show('fast');
}

// Функция, отображающая элемент с медленной анимацией.
function show_element_slow(element_id)
{ 
    $("#" + element_id).show('slow');
}