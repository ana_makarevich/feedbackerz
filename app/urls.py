"""
Definition of urls for DjangoWebProject.
"""


from datetime import datetime
from django.conf.urls import patterns, url,include
from django.contrib import admin
from django.views.generic import TemplateView
from app.views import (Index, Register, UserLogin, 
    RegisterStudent, RegisterInstructor, 
    AboutFeedbacker, AboutUs, 
    My, UserLogout, RegisterConfirm,
    ConfirmStatus, AttachGroup,
    EvaluateLecture, EvaluateSubject,
    FindProfile, AttachProfile,
    LectureResults, LectureResultsGraphData, SubjectResults,
    SubjectResultsGraphData, GroupsAndCriteria, Faq, ContactAdmin,
    TakeSurvey, ShowGroupInfo, MyProfile, Tutorials, TestResults,
    RefreshSchedule, StudentRatingView, OpenStudentRating, EditStudentRating,
    DeleteStudentRating, ShowComplaints)
from app.api import GTSILFeedback, Subjects
from app.views import email_exists, username_exists
from django.utils.translation import ugettext_lazy as _
from rest_framework import routers
from app import api
# Uncomment the next lines to enable the admin:
admin.autodiscover()
router = routers.DefaultRouter()
router.register(r'users', api.UserViewSet)
router.register(r'groups', api.GroupViewSet)

urlpatterns = patterns('',
    # Examples:
    url(r'^$', Index.as_view(), name = 'index'),    
    url(r'^login/$', UserLogin.as_view(), name = 'login'),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls'), name = 'admin_doc'),

    # Uncomment the next line to enable the admin:
    url(r'^register/$', Register.as_view(), name = 'register'),
    url(r'^register/', include(patterns('',
        url(r'^student/$', RegisterStudent.as_view(), name = 'register_student'),
        url(r'^instructor/$', RegisterInstructor.as_view(), name = 'register_instructor'),
    ))),
    # url(r'^register_student/$', RegisterStudent.as_view(), name = 'register_student'),
    # url(r'^register_instructor/$', RegisterInstructor.as_view(), name = 'register_instructor'),
    url(r'^confirm_status/$', ConfirmStatus.as_view(), name = 'confirm_status'),
    url(r'^attach_group/$', AttachGroup.as_view(), name = 'attach_group'),
    url(_(r'^my/$'), My.as_view(), name = 'my'),
    url(r'^logout/$', UserLogout.as_view(), name = 'logout'),
    url(r'^about_us/$', AboutUs.as_view(), name = 'about_us'),
    url(r'^contact_admin/', ContactAdmin.as_view(), name = 'contact_admin'),
    url(r'^faq/$', Faq.as_view(), name='faq'),
    url(r'^about_feedbacker/$', AboutFeedbacker.as_view(), name = 'about_feedbacker'),            
    url(r'^admin/', include(admin.site.urls), name = 'admin'),
    url(r'^confirm/(?P<activation_key>\w+)/', RegisterConfirm.as_view(), name = 'confirm'),
    url(r'^user/password/reset/$', 'django.contrib.auth.views.password_reset',
        {'template_name': 'registration/password_reset_form.html', 'post_reset_redirect' : '/user/password/reset/done/'}, name="password_reset"),
    (r'^user/password/reset/done/$', 'django.contrib.auth.views.password_reset_done'),
    (r'^user/password/reset/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$',
        'django.contrib.auth.views.password_reset_confirm', 
        {'post_reset_redirect' : '/user/password/done/'}),
    (r'^user/password/done/$',
        'django.contrib.auth.views.password_reset_complete'),
    url(r'^subjects/(?P<gtsi_id>\d+)/$', EvaluateSubject.as_view(), name='subject'),
    url(r'^subjects/(?P<gtsi_id>\d+)/vote/$', EvaluateSubject.as_view(), name='vote_subject'),
    url(r'^lectures/(?P<gtsil_id>\d+)/$', EvaluateLecture.as_view(), name='lecture'),
    url(r'^lectures/vote/(?P<gtsil_id>\d+)/$', EvaluateLecture.as_view(), name='vote_lecture'),
    url(r'^username_exists/(?P<username>([A-Z]|[a-z]|[0-9]|\.|_|\-){3,30})$', username_exists),
    url(r'^email_exists/(?P<email>([A-Z]|[a-z]|[0-9]|\@|\.|_|\-){3,30})$', email_exists),
    url(r'^find_profile/$', FindProfile.as_view(), name = 'find_profile'),
    url(r'^attach_profile/(?P<instructor_id>\d+)/$', AttachProfile.as_view(), name = 'attach_profile'),
    url(r'^results/(?P<gtsil_id>\d+)/$', LectureResults.as_view(), name='lecture_results'),
    url(r'^subject_results/(?P<subject_id>\d+)/$', SubjectResults.as_view(), name='subject_results'),
    url(r'^graphdata/$', LectureResultsGraphData.as_view(), name='graphdata'),
    url(r'^subjgraphdata/$', SubjectResultsGraphData.as_view(), name = 'subjgraphdata'),
    url(r'^getgroupscrit/$', GroupsAndCriteria.as_view(), name='getgroupscrit'),
    url(r'^api/v1/lectures/(?P<gtsil_id>\d+)/$', GTSILFeedback.as_view(), name='gtsil_feedback_api'),
    url(r'^api/v1/subjects/$', Subjects.as_view(), name='subjects_api'),
    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^surveys/(?P<survey_id>\w+-\w+-\w+-\w+-\w+)/$', TakeSurvey.as_view(), name = 'survey'),
    url(r'^group/(?P<group_id>\w+)/$', ShowGroupInfo.as_view(), name = 'group_info'),
    url(r'^my/profile/$', MyProfile.as_view(), name = 'my_profile'),
    url(r'^tutorials/$', Tutorials.as_view(), name = 'tutorials'),
    url(r'^survey_results/(?P<survey_id>\w+-\w+-\w+-\w+-\w+)/(?P<group_id>\d+)/$', TestResults.as_view(), name = 'survey_results'),
    url(r'^refresh_schedule/$', RefreshSchedule.as_view(), name = 'refresh_schedule'),
    url(r'^student_rating/$', StudentRatingView.as_view(), name='student_rating'),
    url(r'^open_student_rating/$', OpenStudentRating.as_view(), name='open_student_rating'),
    url(r'^edit_student_rating/(?P<rating_id>\d+)/$', EditStudentRating.as_view(), name='edit_student_rating'),
    url(r'^delete_student_rating/(?P<rating_id>\d+)/$', DeleteStudentRating.as_view(), name='delete_student_rating'),
    url(r'^show_complaints/$', ShowComplaints.as_view(), name='show_complaints'),
    # url(r'^confirm_status_page/$', ConfirmStatusPage.as_view(), name="confirm_status_page"),
)