# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0019_auto_20151206_1826'),
    ]

    operations = [
        migrations.CreateModel(
            name='GTSI_Assignment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('assignment_type', models.CharField(max_length=20)),
                ('assignment_name', models.CharField(max_length=20)),
                ('assignment_description', models.TextField()),
                ('estimated_workload', models.FloatField()),
                ('gtsi', models.ForeignKey(default=None, blank=True, to='app.GTS_Instructor', null=True, verbose_name='\u0413\u0420_\u041f\u0420\u0415\u0414\u041c_\u0421\u0415\u041c_\u041f\u0420\u0415\u041f_\u041b\u0415\u041a')),
            ],
        ),
        migrations.CreateModel(
            name='GTSIA_Criteria_User_Vote',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('vote', models.PositiveSmallIntegerField(default=None, null=True, blank=True)),
                ('criteria', models.ForeignKey(default=None, blank=True, to='app.Criteria', null=True)),
                ('gtsia', models.ForeignKey(default=None, blank=True, to='app.GTSI_Assignment', null=True)),
                ('student', models.ForeignKey(default=None, blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
        ),
    ]
