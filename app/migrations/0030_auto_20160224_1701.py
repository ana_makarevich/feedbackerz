# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0029_gtsi_lecture_number_pair'),
    ]

    operations = [
        migrations.AlterField(
            model_name='group',
            name='schedule_update_date',
            field=models.DateField(default=None, null=True, blank=True),
        ),
    ]
