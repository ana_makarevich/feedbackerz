# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0035_auto_20160315_2159'),
    ]

    operations = [
        migrations.CreateModel(
            name='StudentRatingData',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('first_name', models.CharField(max_length=50)),
                ('last_name', models.CharField(max_length=50)),
                ('middle_name', models.CharField(max_length=50)),
                ('gpa', models.FloatField()),
                ('english', models.PositiveSmallIntegerField()),
                ('patents', models.PositiveSmallIntegerField()),
                ('competitions', models.PositiveSmallIntegerField()),
                ('articles_int', models.PositiveSmallIntegerField()),
                ('articles_nat', models.PositiveSmallIntegerField()),
                ('presentation_int', models.PositiveSmallIntegerField()),
                ('presentation_nat', models.PositiveSmallIntegerField()),
                ('sport', models.PositiveSmallIntegerField()),
                ('scopus', models.PositiveSmallIntegerField()),
            ],
        ),
    ]
