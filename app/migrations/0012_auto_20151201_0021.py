# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0011_auto_20151128_1757'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='instructor',
            name='instructor_profile',
        ),
        migrations.AddField(
            model_name='instructorprofile',
            name='pub_profile',
            field=models.ForeignKey(default=None, blank=True, to='app.Instructor', null=True, verbose_name='\u041f\u0443\u0431\u043b\u0438\u0447\u043d\u044b\u0439 \u043f\u0440\u043e\u0444\u0438\u043b\u044c'),
        ),
    ]
