# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0048_gtsil_result'),
    ]

    operations = [
        migrations.CreateModel(
            name='GTSIL_Results_Distribution',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('term', models.PositiveSmallIntegerField()),
                ('fully_disagree_count', models.IntegerField()),
                ('disagree_count', models.IntegerField()),
                ('not_sure_count', models.IntegerField()),
                ('agree_count', models.IntegerField()),
                ('fully_agree_count', models.IntegerField()),
                ('voters', models.IntegerField()),
                ('average', models.FloatField()),
                ('criteria', models.ForeignKey(to='app.Criteria')),
                ('group', models.ForeignKey(to='app.Group')),
                ('instructor', models.ForeignKey(to='app.Instructor')),
                ('lecture', models.ForeignKey(default=None, blank=True, to='app.GTSI_Lecture', null=True)),
                ('subject', models.ForeignKey(to='app.Subject')),
            ],
        ),
    ]
