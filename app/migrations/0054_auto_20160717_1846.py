# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0053_gtsil_feedback_approved'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='gtsil_feedback',
            name='approved',
        ),
        migrations.AddField(
            model_name='gtsil_feedback',
            name='pub_status',
            field=models.CharField(default='\u041d', max_length=10, choices=[('\u041d', '\u041d\u043e\u0432\u043e\u0435'), ('\u041e\u0414', '\u041e\u0434\u043e\u0431\u0440\u0435\u043d\u043e'), ('\u041e\u0422\u041a', '\u041e\u0442\u043a\u0430\u0437\u0430\u043d\u043e')]),
        ),
    ]
