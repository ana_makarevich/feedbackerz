# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0012_auto_20151201_0021'),
    ]

    operations = [
        migrations.AddField(
            model_name='criteria',
            name='active',
            field=models.BooleanField(default=True),
        ),
        migrations.AlterField(
            model_name='profiletype',
            name='name',
            field=models.CharField(max_length=40),
        ),
    ]
