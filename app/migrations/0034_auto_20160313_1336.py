# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0033_constants'),
    ]

    operations = [
        migrations.CreateModel(
            name='AfterLectureChoice',
            fields=[
                ('alc_id', models.CharField(max_length=200, serialize=False, primary_key=True)),
                ('choice_text', models.CharField(max_length=200)),
                ('votes', models.IntegerField(default=0)),
                ('correct', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='AfterLectureQuestion',
            fields=[
                ('alq_id', models.CharField(max_length=200, serialize=False, primary_key=True)),
                ('question_text', models.CharField(max_length=200)),
                ('weight', models.IntegerField(default=1)),
            ],
        ),
        migrations.CreateModel(
            name='AfterLectureStudentResult',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('answer', models.ForeignKey(to='app.AfterLectureChoice')),
                ('student', models.ForeignKey(to='app.StudentProfile')),
            ],
        ),
        migrations.CreateModel(
            name='AfterLectureStudentResults',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('correct_answers', models.IntegerField(default=0)),
                ('student', models.ForeignKey(to='app.StudentProfile')),
            ],
        ),
        migrations.CreateModel(
            name='AfterLectureSurvey',
            fields=[
                ('als_id', models.CharField(max_length=200, serialize=False, primary_key=True)),
                ('survey_name', models.CharField(max_length=200)),
                ('description', models.TextField()),
                ('due_date', models.DateTimeField(verbose_name='Due date')),
                ('gtsil', models.ManyToManyField(to='app.GTSI_Lecture')),
            ],
        ),
        migrations.AlterField(
            model_name='constants',
            name='key',
            field=models.CharField(max_length=40, serialize=False, primary_key=True),
        ),
        migrations.AddField(
            model_name='afterlecturestudentresults',
            name='survey',
            field=models.ForeignKey(to='app.AfterLectureSurvey'),
        ),
        migrations.AddField(
            model_name='afterlecturequestion',
            name='survey',
            field=models.ForeignKey(to='app.AfterLectureSurvey'),
        ),
        migrations.AddField(
            model_name='afterlecturechoice',
            name='question',
            field=models.ForeignKey(to='app.AfterLectureQuestion'),
        ),
    ]
