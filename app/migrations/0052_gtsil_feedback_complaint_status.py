# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0051_auto_20160712_1748'),
    ]

    operations = [
        migrations.AddField(
            model_name='gtsil_feedback',
            name='complaint_status',
            field=models.CharField(default='\u041d', max_length=10, choices=[('\u041d', '\u041d\u043e\u0432\u043e\u0435'), ('\u041e', '\u041e\u0431\u0440\u0430\u0431\u043e\u0442\u0430\u043d\u043e')]),
        ),
    ]
