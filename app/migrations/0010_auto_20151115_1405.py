# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0009_criteria_practicecriteria'),
    ]

    operations = [
        migrations.AlterField(
            model_name='gtsi_criteria_user_vote',
            name='criteria',
            field=models.ForeignKey(default=None, blank=True, to='app.Criteria', null=True, verbose_name='\u041a\u0440\u0438\u0442\u0435\u0440\u0438\u0439 \u043e\u0446\u0435\u043d\u043a\u0438'),
        ),
        migrations.AlterField(
            model_name='gtsil_criteria_user_vote',
            name='criteria',
            field=models.ForeignKey(default=None, blank=True, to='app.Criteria', null=True, verbose_name='\u041a\u0440\u0438\u0442\u0435\u0440\u0438\u0439 \u043e\u0446\u0435\u043d\u043a\u0438'),
        ),
    ]
