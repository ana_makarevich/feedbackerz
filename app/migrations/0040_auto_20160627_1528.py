# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0039_auto_20160627_1521'),
    ]

    operations = [
        migrations.AlterField(
            model_name='studentratingdata',
            name='department_txt',
            field=models.CharField(default=None, max_length=100, null=True),
        ),
        migrations.AlterField(
            model_name='studentratingdata',
            name='faculty_txt',
            field=models.CharField(default=None, max_length=100, null=True),
        ),
        migrations.AlterField(
            model_name='studentratingdata',
            name='group_txt',
            field=models.CharField(default=None, max_length=100, null=True),
        ),
    ]
