# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0021_gtsi_feedback'),
    ]

    operations = [
        migrations.CreateModel(
            name='GTSIL_Feedback',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('feedback', models.TextField()),
                ('feedback_type', models.BooleanField(default=False)),
                ('gtsil', models.ForeignKey(default=None, blank=True, to='app.GTSI_Lecture', null=True)),
                ('student', models.ForeignKey(default=None, blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
        ),
    ]
