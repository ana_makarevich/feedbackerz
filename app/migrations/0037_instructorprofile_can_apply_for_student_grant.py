# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0036_studentratingdata'),
    ]

    operations = [
        migrations.AddField(
            model_name='instructorprofile',
            name='can_apply_for_student_grant',
            field=models.BooleanField(default=False, verbose_name='\u041c\u043e\u0436\u0435\u0442 \u043f\u043e\u0434\u0430\u0442\u044c \u043d\u0430 \u0438\u043c\u0435\u043d\u043d\u043e\u0439 \u0433\u0440\u0430\u043d\u0442 \u0434\u043b\u044f \u0441\u0442\u0443\u0434\u0435\u043d\u0442\u0430?'),
        ),
    ]
