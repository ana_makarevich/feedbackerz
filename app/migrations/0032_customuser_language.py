# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0031_remove_customuser_language'),
    ]

    operations = [
        migrations.AddField(
            model_name='customuser',
            name='language',
            field=models.CharField(default='ru', max_length=2, choices=[('ru', '\u0420\u0443\u0441\u0441\u043a\u0438\u0439'), ('uk', '\u0423\u043a\u0440\u0430\u0438\u043d\u0441\u043a\u0438\u0439'), ('en-gb', '\u0410\u043d\u0433\u043b\u0438\u0439\u0441\u043a\u0438\u0439')]),
        ),
    ]
