# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0006_auto_20151107_0941'),
    ]

    operations = [
        migrations.AddField(
            model_name='activitytype',
            name='activity_type',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='activitytype',
            name='activty_name',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='activitytype',
            name='short_name',
            field=models.CharField(max_length=10, null=True, blank=True),
        ),
    ]
