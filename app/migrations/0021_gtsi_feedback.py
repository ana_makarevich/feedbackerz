# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0020_gtsi_assignment_gtsia_criteria_user_vote'),
    ]

    operations = [
        migrations.CreateModel(
            name='GTSI_Feedback',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('feedback', models.TextField()),
                ('feedback_type', models.BooleanField(default=False)),
                ('gtsi', models.ForeignKey(default=None, blank=True, to='app.GTS_Instructor', null=True, verbose_name='\u0413\u0420_\u041f\u0420\u0415\u0414\u041c_\u0421\u0415\u041c_\u041f\u0420\u0415\u041f_\u041a\u0420\u0418\u0422')),
                ('student', models.ForeignKey(default=None, blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
        ),
    ]
