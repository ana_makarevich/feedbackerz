# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0040_auto_20160627_1528'),
    ]

    operations = [
        migrations.AddField(
            model_name='studentratingdata',
            name='added_by',
            field=models.ForeignKey(default=None, blank=True, to='app.InstructorProfile', null=True, verbose_name='\u041a\u0442\u043e \u0434\u043e\u0431\u0430\u0432\u0438\u043b'),
        ),
        migrations.AlterField(
            model_name='studentratingdata',
            name='articles_int',
            field=models.PositiveSmallIntegerField(verbose_name='\u0421\u0442\u0430\u0442\u044c\u0438 \u0437\u0430\u0440\u0443\u0431\u0435\u0436\u043d\u044b\u0435'),
        ),
        migrations.AlterField(
            model_name='studentratingdata',
            name='articles_nat',
            field=models.PositiveSmallIntegerField(verbose_name='\u0421\u0442\u0430\u0442\u044c\u0438 \u043d\u0430\u0446\u0438\u043e\u043d\u0430\u043b\u044c\u043d\u044b\u0435'),
        ),
        migrations.AlterField(
            model_name='studentratingdata',
            name='competitions',
            field=models.PositiveSmallIntegerField(verbose_name='\u041e\u043b\u0438\u043c\u043f\u0438\u0430\u0434\u044b'),
        ),
        migrations.AlterField(
            model_name='studentratingdata',
            name='department',
            field=models.ForeignKey(default=None, blank=True, to='app.Department', null=True, verbose_name='\u041a\u0430\u0444\u0435\u0434\u0440\u0430'),
        ),
        migrations.AlterField(
            model_name='studentratingdata',
            name='department_txt',
            field=models.CharField(default=None, max_length=100, null=True, verbose_name='\u041a\u0430\u0444\u0435\u0434\u0440\u0430'),
        ),
        migrations.AlterField(
            model_name='studentratingdata',
            name='english',
            field=models.PositiveSmallIntegerField(verbose_name='\u0417\u043d\u0430\u043d\u0438\u0435 \u044f\u0437\u044b\u043a\u043e\u0432'),
        ),
        migrations.AlterField(
            model_name='studentratingdata',
            name='faculty',
            field=models.ForeignKey(default=None, blank=True, to='app.Faculty', null=True, verbose_name='\u0424\u0430\u043a\u0443\u043b\u044c\u0442\u0435\u0442'),
        ),
        migrations.AlterField(
            model_name='studentratingdata',
            name='faculty_txt',
            field=models.CharField(default=None, max_length=100, null=True, verbose_name='\u0424\u0430\u043a\u0443\u043b\u044c\u0442\u0435\u0442'),
        ),
        migrations.AlterField(
            model_name='studentratingdata',
            name='first_name',
            field=models.CharField(max_length=50, verbose_name='\u0418\u043c\u044f'),
        ),
        migrations.AlterField(
            model_name='studentratingdata',
            name='gpa',
            field=models.FloatField(verbose_name='\u0421\u0440\u0435\u0434\u043d\u0438\u0439 \u0431\u0430\u043b\u043b'),
        ),
        migrations.AlterField(
            model_name='studentratingdata',
            name='group',
            field=models.ForeignKey(default=None, blank=True, to='app.Group', null=True, verbose_name='\u0413\u0440\u0443\u043f\u043f\u0430'),
        ),
        migrations.AlterField(
            model_name='studentratingdata',
            name='group_txt',
            field=models.CharField(default=None, max_length=100, null=True, verbose_name='\u0413\u0440\u0443\u043f\u043f\u0430'),
        ),
        migrations.AlterField(
            model_name='studentratingdata',
            name='integral',
            field=models.PositiveSmallIntegerField(default=0, verbose_name='\u0418\u043d\u0442\u0435\u0433\u0440\u0430\u043b\u044c\u043d\u0430\u044f \u043e\u0446\u0435\u043d\u043a\u0430'),
        ),
        migrations.AlterField(
            model_name='studentratingdata',
            name='last_name',
            field=models.CharField(max_length=50, verbose_name='\u0424\u0430\u043c\u0438\u043b\u0438\u044f'),
        ),
        migrations.AlterField(
            model_name='studentratingdata',
            name='middle_name',
            field=models.CharField(max_length=50, verbose_name='\u041e\u0442\u0447\u0435\u0441\u0442\u0432\u043e'),
        ),
        migrations.AlterField(
            model_name='studentratingdata',
            name='patents',
            field=models.PositiveSmallIntegerField(verbose_name='\u041f\u0430\u0442\u0435\u043d\u0442\u044b, \u0438\u0437\u043e\u0431\u0440\u0435\u0442\u0435\u043d\u0438\u044f'),
        ),
        migrations.AlterField(
            model_name='studentratingdata',
            name='presentation_int',
            field=models.PositiveSmallIntegerField(verbose_name='\u0414\u043e\u043a\u043b\u0430\u0434\u044b \u0437\u0430\u0440\u0443\u0431\u0435\u0436\u043d\u044b\u0435'),
        ),
        migrations.AlterField(
            model_name='studentratingdata',
            name='presentation_nat',
            field=models.PositiveSmallIntegerField(verbose_name='\u0414\u043e\u043a\u043b\u0430\u0434\u044b \u043d\u0430\u0446\u0438\u043e\u043d\u0430\u043b\u044c\u043d\u044b\u0435'),
        ),
        migrations.AlterField(
            model_name='studentratingdata',
            name='scopus',
            field=models.PositiveSmallIntegerField(verbose_name='\u041f\u0443\u0431\u043b\u0438\u043a\u0430\u0446\u0438\u0438 \u0432 Scoups'),
        ),
        migrations.AlterField(
            model_name='studentratingdata',
            name='sport',
            field=models.PositiveSmallIntegerField(verbose_name='\u0421\u043f\u043e\u0440\u0442\u0438\u0432\u043d\u044b\u0435 \u043c\u0435\u0440\u043e\u043f\u0440\u0438\u044f\u0442\u0438\u044f'),
        ),
    ]
