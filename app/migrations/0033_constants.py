# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0032_customuser_language'),
    ]

    operations = [
        migrations.CreateModel(
            name='Constants',
            fields=[
                ('key', models.CharField(max_length=10, serialize=False, primary_key=True)),
                ('value', models.CharField(max_length=512)),
            ],
        ),
    ]
