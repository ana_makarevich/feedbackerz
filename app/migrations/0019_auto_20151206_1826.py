# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0018_auto_20151206_1823'),
    ]

    operations = [
        migrations.AddField(
            model_name='criteria',
            name='active_since',
            field=models.DateField(default=None, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='criteria',
            name='active_until',
            field=models.DateField(default=None, null=True, blank=True),
        ),
    ]
