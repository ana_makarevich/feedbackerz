# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0042_auto_20160628_1050'),
    ]

    operations = [
        migrations.AlterField(
            model_name='studentratingdata',
            name='integral',
            field=models.FloatField(default=0, verbose_name='\u0418\u043d\u0442\u0435\u0433\u0440\u0430\u043b\u044c\u043d\u0430\u044f \u043e\u0446\u0435\u043d\u043a\u0430'),
        ),
    ]
