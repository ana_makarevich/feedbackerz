# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0025_auto_20160126_1721'),
    ]

    operations = [
        migrations.AlterField(
            model_name='group',
            name='schedule_update_date',
            field=models.DateTimeField(default=None, null=True, blank=True),
        ),
    ]
