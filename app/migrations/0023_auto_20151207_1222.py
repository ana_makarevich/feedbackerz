# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0022_gtsil_feedback'),
    ]

    operations = [
        migrations.CreateModel(
            name='GTSIA_Feedback',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('feedback', models.TextField()),
                ('is_complaint', models.BooleanField(default=False)),
                ('gtsia', models.ForeignKey(default=None, blank=True, to='app.GTSI_Assignment', null=True)),
                ('student', models.ForeignKey(default=None, blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
        ),
        migrations.RenameField(
            model_name='gtsi_feedback',
            old_name='feedback_type',
            new_name='is_complaint',
        ),
        migrations.RenameField(
            model_name='gtsil_feedback',
            old_name='feedback_type',
            new_name='is_complaint',
        ),
    ]
