# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0014_auto_20151205_1425'),
    ]

    operations = [
        migrations.DeleteModel(
            name='PracticeCriteria',
        ),
        migrations.AddField(
            model_name='department',
            name='head',
            field=models.ForeignKey(default=None, blank=True, to='app.InstructorProfile', null=True),
        ),
        migrations.AddField(
            model_name='faculty',
            name='head',
            field=models.ForeignKey(default=None, blank=True, to='app.InstructorProfile', null=True),
        ),
    ]
