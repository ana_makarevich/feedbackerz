# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0049_gtsil_results_distribution'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='gtsil_results_distribution',
            name='average',
        ),
    ]
