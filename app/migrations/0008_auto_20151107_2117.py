# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0007_auto_20151107_1759'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProfileType',
            fields=[
                ('code', models.CharField(max_length=10, serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=10)),
                ('short_name', models.CharField(max_length=10)),
            ],
        ),
        migrations.AlterField(
            model_name='customuser',
            name='profile_type',
            field=models.ForeignKey(default=None, blank=True, to='app.ProfileType', null=True),
        ),
        migrations.AlterField(
            model_name='gtsi_lecture',
            name='gtsi',
            field=models.ForeignKey(default=None, blank=True, to='app.GTS_Instructor', null=True, verbose_name='\u0413\u0420_\u041f\u0420\u0415\u0414\u041c_\u0421\u0415\u041c_\u041f\u0420\u0415\u041f_\u041b\u0415\u041a'),
        ),
    ]
