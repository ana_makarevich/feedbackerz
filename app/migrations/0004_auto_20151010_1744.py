# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0003_auto_20151010_1555'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='group_term_subject',
            options={'verbose_name': '\u041f\u0440\u0435\u0434\u043c\u0442\u044b \u043f\u043e \u0433\u0440\u0443\u043f\u043f\u0430\u043c, \u043f\u043e \u0441\u0435\u043c\u0435\u0441\u0442\u0440\u0430\u043c', 'verbose_name_plural': '\u041f\u0440\u0435\u0434\u043c\u0435\u0442\u044b \u043f\u043e \u0433\u0440\u0443\u043f\u043f\u0430\u043c, \u043f\u043e \u0441\u0435\u043c\u0435\u0441\u0442\u0440\u0430\u043c'},
        ),
        migrations.AlterModelOptions(
            name='gts_instructor',
            options={'verbose_name': '\u0413\u0440\u0443\u043f\u043f\u0430_\u0421\u0435\u043c\u0435\u0441\u0442\u0440_\u041f\u0440\u0435\u0434\u043c\u0435\u0442_\u041f\u0440\u0435\u043f\u043e\u0434\u0430\u0432\u0430\u0442\u0435\u043b\u044c', 'verbose_name_plural': '\u0413\u0440\u0443\u043f\u043f\u0430_\u0421\u0435\u043c\u0435\u0441\u0442\u0440_\u041f\u0440\u0435\u0434\u043c\u0435\u0442_\u041f\u0440\u0435\u043f\u043e\u0434\u0430\u0432\u0430\u0442\u0435\u043b\u044c'},
        ),
        migrations.RemoveField(
            model_name='deadline',
            name='deadline_type',
        ),
        migrations.AddField(
            model_name='deadline',
            name='deadline_type',
            field=models.ForeignKey(default=None, blank=True, to='app.DeadlineType', null=True, verbose_name='\u0422\u0438\u043f \u043a\u043e\u043d\u0442\u0440\u043e\u043b\u044f'),
        ),
        migrations.RemoveField(
            model_name='group_term_subject',
            name='subject',
        ),
        migrations.AddField(
            model_name='group_term_subject',
            name='subject',
            field=models.ForeignKey(verbose_name='\u041f\u0440\u0435\u0434\u043c\u0435\u0442', blank=True, to='app.Subject', null=True),
        ),
        migrations.RemoveField(
            model_name='gts_instructor',
            name='activity_type',
        ),
        migrations.AddField(
            model_name='gts_instructor',
            name='activity_type',
            field=models.ForeignKey(verbose_name='\u0422\u0438\u043f \u0437\u0430\u043d\u044f\u0442\u0438\u0439', blank=True, to='app.ActivityType', null=True),
        ),
        migrations.RemoveField(
            model_name='gtsi_criteria_user_vote',
            name='criteria',
        ),
        migrations.AddField(
            model_name='gtsi_criteria_user_vote',
            name='criteria',
            field=models.ForeignKey(default=None, blank=True, to='app.SubjectCriteria', null=True, verbose_name='\u041a\u0440\u0438\u0442\u0435\u0440\u0438\u0439 \u043e\u0446\u0435\u043d\u043a\u0438'),
        ),
        migrations.RemoveField(
            model_name='gtsil_criteria_user_vote',
            name='criteria',
        ),
        migrations.AddField(
            model_name='gtsil_criteria_user_vote',
            name='criteria',
            field=models.ForeignKey(default=None, blank=True, to='app.LectureCriteria', null=True, verbose_name='\u041a\u0440\u0438\u0442\u0435\u0440\u0438\u0439 \u043e\u0446\u0435\u043d\u043a\u0438'),
        ),
        migrations.AlterField(
            model_name='instructor',
            name='middle_name',
            field=models.CharField(default=None, max_length=20, null=True, verbose_name='\u041e\u0442\u0447\u0435\u0441\u0442\u0432\u043e', blank=True),
        ),
    ]
