# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0056_auto_20160904_1413'),
    ]

    operations = [
        migrations.AddField(
            model_name='studentratingdata',
            name='end_date',
            field=models.DateField(default=None, null=True, verbose_name='\u041d\u0430\u0447\u0430\u043b\u043e \u043a\u043e\u043d\u043a\u0443\u0440\u0441\u0430', blank=True),
        ),
        migrations.AddField(
            model_name='studentratingdata',
            name='rating_id',
            field=models.IntegerField(default=1, verbose_name='\u0418\u0434\u0435\u043d\u0442\u0438\u0444\u0438\u043a\u0430\u0442\u043e\u0440 \u0440\u0435\u0439\u0442\u0438\u043d\u0433\u0430'),
        ),
        migrations.AddField(
            model_name='studentratingdata',
            name='start_date',
            field=models.DateField(default=None, null=True, verbose_name='\u041d\u0430\u0447\u0430\u043b\u043e \u043a\u043e\u043d\u043a\u0443\u0440\u0441\u0430', blank=True),
        ),
    ]
