# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0015_auto_20151206_1715'),
    ]

    operations = [
        migrations.AddField(
            model_name='instructorprofile',
            name='primary_department',
            field=models.ForeignKey(default=None, blank=True, to='app.Department', null=True),
        ),
        migrations.AddField(
            model_name='instructorprofile',
            name='primary_faculty',
            field=models.ForeignKey(default=None, blank=True, to='app.Faculty', null=True),
        ),
    ]
