# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0004_auto_20151010_1744'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='group_term_subject',
            options={'verbose_name': '\u0413\u0440\u0443\u043f\u043f\u0430-\u0421\u0435\u043c\u0435\u0441\u0442\u0440-\u041f\u0440\u0435\u0434\u043c\u0435\u0442', 'verbose_name_plural': '\u0413\u0440\u0443\u043f\u043f\u0430-\u0421\u0435\u043c\u0435\u0441\u0442\u0440-\u041f\u0440\u0435\u0434\u043c\u0435\u0442'},
        ),
        migrations.RemoveField(
            model_name='announcement',
            name='group',
        ),
        migrations.AddField(
            model_name='announcement',
            name='group',
            field=models.ManyToManyField(default=None, to='app.Group', null=True, blank=True),
        ),
    ]
