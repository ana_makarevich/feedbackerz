# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0024_criteria_weight'),
    ]

    operations = [
        migrations.AddField(
            model_name='group',
            name='schedule_update_date',
            field=models.DateField(default=None, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='group',
            name='year',
            field=models.PositiveSmallIntegerField(default=2014, null=True, blank=True),
        ),
    ]
