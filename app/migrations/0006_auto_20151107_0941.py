# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0005_auto_20151010_1853'),
    ]

    operations = [
        migrations.RenameField(
            model_name='instructor',
            old_name='department',
            new_name='departments',
        ),
        migrations.AlterField(
            model_name='announcement',
            name='group',
            field=models.ManyToManyField(to='app.Group'),
        ),
        migrations.AlterField(
            model_name='instructorprofile',
            name='middle_name',
            field=models.CharField(default=None, max_length=20, null=True, verbose_name='\u041e\u0442\u0447\u0435\u0441\u0442\u0432\u043e', blank=True),
        ),
        migrations.AlterField(
            model_name='instructorprofile',
            name='phone_number',
            field=models.CharField(default=None, max_length=15, null=True, verbose_name='\u041d\u043e\u043c\u0435\u0440 \u0442\u0435\u043b\u0435\u0444\u043e\u043d\u0430', blank=True),
        ),
        migrations.AlterField(
            model_name='question',
            name='student',
            field=models.ForeignKey(verbose_name='\u0410\u0432\u0442\u043e\u0440 \u0432\u043e\u043f\u0440\u043e\u0441\u0430', to='app.StudentProfile'),
        ),
        migrations.AlterField(
            model_name='question',
            name='title',
            field=models.CharField(default='\u0412\u043e\u043f\u0440\u043e\u0441', max_length=40, null=True, verbose_name='\u0412\u043e\u043f\u0440\u043e\u0441', blank=True),
        ),
        migrations.AlterField(
            model_name='speciality',
            name='short_name',
            field=models.CharField(max_length=150),
        ),
        migrations.AlterField(
            model_name='studentprofile',
            name='student_id',
            field=models.CharField(default=None, max_length=20, null=True, verbose_name='\u041d\u043e\u043c\u0435\u0440 \u0441\u0442\u0443\u0434\u0435\u043d\u0447\u0435\u0441\u043a\u043e\u0433\u043e', blank=True),
        ),
    ]
