# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0041_auto_20160627_1757'),
    ]

    operations = [
        migrations.CreateModel(
            name='Instructor_Subject_SHI',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('shi', models.FloatField(verbose_name='\u0418\u043d\u0434\u0435\u043a\u0441 \u0437\u0434\u043e\u0440\u043e\u0432\u044c\u044f \u043f\u0440\u0435\u0434\u043c\u0435\u0442\u0430')),
                ('year_start', models.PositiveSmallIntegerField(default=2015)),
                ('year_end', models.PositiveSmallIntegerField(default=2016)),
                ('instructor', models.ForeignKey(to='app.Instructor')),
                ('subject', models.ForeignKey(to='app.Subject')),
            ],
        ),
        migrations.AlterField(
            model_name='studentratingdata',
            name='articles_int',
            field=models.PositiveSmallIntegerField(default=0, verbose_name='\u0421\u0442\u0430\u0442\u044c\u0438 \u0437\u0430\u0440\u0443\u0431\u0435\u0436\u043d\u044b\u0435'),
        ),
        migrations.AlterField(
            model_name='studentratingdata',
            name='articles_nat',
            field=models.PositiveSmallIntegerField(default=0, verbose_name='\u0421\u0442\u0430\u0442\u044c\u0438 \u043d\u0430\u0446\u0438\u043e\u043d\u0430\u043b\u044c\u043d\u044b\u0435'),
        ),
        migrations.AlterField(
            model_name='studentratingdata',
            name='competitions',
            field=models.PositiveSmallIntegerField(default=0, verbose_name='\u041e\u043b\u0438\u043c\u043f\u0438\u0430\u0434\u044b'),
        ),
        migrations.AlterField(
            model_name='studentratingdata',
            name='english',
            field=models.PositiveSmallIntegerField(default=0, verbose_name='\u0417\u043d\u0430\u043d\u0438\u0435 \u044f\u0437\u044b\u043a\u043e\u0432'),
        ),
        migrations.AlterField(
            model_name='studentratingdata',
            name='patents',
            field=models.PositiveSmallIntegerField(default=0, verbose_name='\u041f\u0430\u0442\u0435\u043d\u0442\u044b, \u0438\u0437\u043e\u0431\u0440\u0435\u0442\u0435\u043d\u0438\u044f'),
        ),
        migrations.AlterField(
            model_name='studentratingdata',
            name='presentation_int',
            field=models.PositiveSmallIntegerField(default=0, verbose_name='\u0414\u043e\u043a\u043b\u0430\u0434\u044b \u0437\u0430\u0440\u0443\u0431\u0435\u0436\u043d\u044b\u0435'),
        ),
        migrations.AlterField(
            model_name='studentratingdata',
            name='presentation_nat',
            field=models.PositiveSmallIntegerField(default=0, verbose_name='\u0414\u043e\u043a\u043b\u0430\u0434\u044b \u043d\u0430\u0446\u0438\u043e\u043d\u0430\u043b\u044c\u043d\u044b\u0435'),
        ),
        migrations.AlterField(
            model_name='studentratingdata',
            name='scopus',
            field=models.PositiveSmallIntegerField(default=0, verbose_name='\u041f\u0443\u0431\u043b\u0438\u043a\u0430\u0446\u0438\u0438 \u0432 Scoups'),
        ),
        migrations.AlterField(
            model_name='studentratingdata',
            name='sport',
            field=models.PositiveSmallIntegerField(default=0, verbose_name='\u0421\u043f\u043e\u0440\u0442\u0438\u0432\u043d\u044b\u0435 \u043c\u0435\u0440\u043e\u043f\u0440\u0438\u044f\u0442\u0438\u044f'),
        ),
    ]
