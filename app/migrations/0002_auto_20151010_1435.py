# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ActivityType',
            fields=[
                ('code', models.CharField(max_length=3, serialize=False, primary_key=True)),
                ('activty_name', models.CharField(max_length=40, null=True, blank=True)),
                ('short_name', models.CharField(max_length=2, null=True, blank=True)),
            ],
            options={
                'verbose_name': '\u0422\u0438\u043f \u0437\u0430\u043d\u044f\u0442\u0438\u0439',
                'verbose_name_plural': '\u0422\u0438\u043f\u044b \u0437\u0430\u043d\u044f\u0442\u0438\u0439',
            },
        ),
        migrations.CreateModel(
            name='Announcement',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(default='\u041e\u0431\u044a\u044f\u0432\u043b\u0435\u043d\u0438\u0435', max_length=100, null=True, blank=True)),
                ('text', models.TextField()),
                ('date', models.DateTimeField(default=None, null=True, blank=True)),
            ],
            options={
                'verbose_name': '\u041e\u0431\u044a\u044f\u0432\u043b\u0435\u043d\u0438\u0435',
                'verbose_name_plural': '\u041e\u0431\u044a\u044f\u0432\u043b\u0435\u043d\u0438\u044f',
            },
        ),
        migrations.CreateModel(
            name='Answer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateTimeField(default=None, null=True, blank=True)),
                ('text', models.TextField()),
            ],
            options={
                'verbose_name': '\u041e\u0442\u0432\u0435\u0442',
                'verbose_name_plural': '\u041e\u0442\u0432\u0435\u0442\u044b',
            },
        ),
        migrations.CreateModel(
            name='Criteria',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('criteria_name', models.CharField(max_length=100, null=True, blank=True)),
                ('criteria_description', models.TextField(default=None, null=True, blank=True)),
            ],
            options={
                'verbose_name': '\u041a\u0440\u0438\u0442\u0435\u0440\u0438\u0439',
                'verbose_name_plural': '\u041a\u0440\u0438\u0442\u0435\u0440\u0438\u0438',
            },
        ),
        migrations.CreateModel(
            name='Deadline',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('deadline', models.DateTimeField(default=None, null=True, blank=True)),
                ('description', models.TextField()),
                ('title', models.CharField(max_length=40)),
            ],
            options={
                'verbose_name': '\u0414\u0435\u0434\u043b\u0430\u0439\u043d',
                'verbose_name_plural': '\u0414\u0435\u0434\u043b\u0430\u0439\u043d\u044b',
            },
        ),
        migrations.CreateModel(
            name='DeadlineType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=40, null=True, blank=True)),
                ('short_name', models.CharField(max_length=4, null=True, blank=True)),
            ],
            options={
                'verbose_name': '\u0422\u0438\u043f \u0434\u0435\u0434\u043b\u0430\u0439\u043d\u0430',
                'verbose_name_plural': '\u0422\u0438\u043f\u044b \u0434\u0435\u0434\u043b\u0430\u0439\u043d\u043e\u0432',
            },
        ),
        migrations.CreateModel(
            name='Department',
            fields=[
                ('department_id', models.CharField(max_length=10, serialize=False, primary_key=True)),
                ('short_name', models.CharField(max_length=40)),
                ('full_name', models.CharField(max_length=150)),
            ],
            options={
                'verbose_name': '\u041a\u0430\u0444\u0435\u0434\u0440\u0430',
                'verbose_name_plural': '\u041a\u0430\u0444\u0435\u0434\u0440\u044b',
            },
        ),
        migrations.CreateModel(
            name='Direction',
            fields=[
                ('direction_id', models.CharField(max_length=10, serialize=False, primary_key=True)),
                ('short_name', models.CharField(max_length=40)),
                ('full_name', models.CharField(max_length=150)),
            ],
            options={
                'verbose_name': '\u041d\u0430\u043f\u0440\u0430\u0432\u043b\u0435\u043d\u0438\u0435',
                'verbose_name_plural': '\u041d\u0430\u043f\u0440\u0430\u0432\u043b\u0435\u043d\u0438\u044f',
            },
        ),
        migrations.CreateModel(
            name='Faculty',
            fields=[
                ('faculty_id', models.CharField(max_length=10, serialize=False, primary_key=True)),
                ('short_name', models.CharField(max_length=40)),
                ('full_name', models.CharField(max_length=150)),
            ],
            options={
                'verbose_name': '\u0424\u0430\u043a\u0443\u043b\u044c\u0442\u0435\u0442',
                'verbose_name_plural': '\u0424\u0430\u043a\u0443\u043b\u044c\u0442\u0435\u0442\u044b',
            },
        ),
        migrations.CreateModel(
            name='Group_Term_Subject',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('term', models.CharField(max_length=2, null=True, verbose_name='\u0421\u0435\u043c\u0435\u0441\u0442\u0440', blank=True)),
            ],
            options={
                'verbose_name': '\u0413\u0440\u0443\u043f\u043f\u0430-\u0421\u0435\u043c\u0435\u0441\u0442\u0440-\u041f\u0440\u0435\u0434\u043c\u0435\u0442',
                'verbose_name_plural': '\u0413\u0440\u0443\u043f\u043f\u0430-\u0421\u0435\u043c\u0435\u0441\u0442\u0440-\u041f\u0440\u0435\u0434\u043c\u0435\u0442',
            },
        ),
        migrations.CreateModel(
            name='GTS_Instructor',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('activity_type', models.ManyToManyField(to='app.ActivityType')),
                ('gts', models.ForeignKey(default=None, blank=True, to='app.Group_Term_Subject', null=True, verbose_name='\u0413\u0420_\u041f\u0420\u0415\u0414\u041c_\u0421\u0415\u041c_\u041f\u0420\u0415\u041f')),
            ],
            options={
                'verbose_name': '\u0413\u0440\u0443\u043f\u043f\u0430-\u0421\u0435\u043c\u0435\u0441\u0442\u0440-\u041f\u0440\u0435\u0434\u043c\u0435\u0442-\u041f\u0440\u0435\u043f\u043e\u0434\u0430\u0432\u0430\u0442\u0435\u043b\u044c-\u0422\u0438\u043f',
                'verbose_name_plural': '\u0413\u0440\u0443\u043f\u043f\u0430-\u0421\u0435\u043c\u0435\u0441\u0442\u0440-\u041f\u0440\u0435\u0434\u043c\u0435\u0442-\u041f\u0440\u0435\u043f\u043e\u0434\u0430\u0432\u0430\u0442\u0435\u043b\u044c-\u0422\u0438\u043f',
            },
        ),
        migrations.CreateModel(
            name='GTSI_Criteria_User_Vote',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('vote', models.PositiveSmallIntegerField(default=None, null=True, blank=True)),
                ('criteria', models.ManyToManyField(to='app.Criteria')),
                ('gtsi', models.ForeignKey(default=None, blank=True, to='app.GTS_Instructor', null=True, verbose_name='\u0413\u0420_\u041f\u0420\u0415\u0414\u041c_\u0421\u0415\u041c_\u041f\u0420\u0415\u041f_\u041a\u0420\u0418\u0422')),
                ('student', models.ForeignKey(default=None, blank=True, to='app.StudentProfile', null=True)),
            ],
            options={
                'verbose_name': '\u041e\u0446\u0435\u043a\u0430 \u043f\u0440\u0435\u0434\u043c\u0435\u0442\u0430',
                'verbose_name_plural': '\u041e\u0446\u0435\u043d\u043a\u0438 \u043f\u0440\u0435\u0434\u043c\u0435\u0442\u043e\u0432',
            },
        ),
        migrations.CreateModel(
            name='GTSI_Lecture',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('lecture_start_time', models.DateTimeField(default=None, null=True, blank=True)),
                ('lecture_name', models.CharField(default=None, max_length=150, null=True, blank=True)),
                ('gtsi', models.ForeignKey(default=None, blank=True, to='app.GTS_Instructor', null=True, verbose_name='\u0413\u0420_\u041f\u0420\u0415\u0414\u041c_\u0421\u0415\u041c_\u041f\u0420\u0415\u041f_\u041a\u0420\u0418\u0422')),
            ],
            options={
                'verbose_name': '\u041b\u0435\u043a\u0446\u0438\u044f',
                'verbose_name_plural': '\u041b\u0435\u043a\u0446\u0438\u0438',
            },
        ),
        migrations.CreateModel(
            name='GTSIL_Criteria_User_Vote',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('vote', models.PositiveSmallIntegerField(default=None, null=True, blank=True)),
                ('criteria', models.ManyToManyField(to='app.Criteria')),
                ('gtsil', models.ForeignKey(default=None, blank=True, to='app.GTSI_Lecture', null=True)),
                ('student', models.ForeignKey(default=None, blank=True, to='app.StudentProfile', null=True)),
            ],
            options={
                'verbose_name': '\u041e\u0446\u0435\u043d\u043a\u0430 \u043b\u0435\u043a\u0446\u0438\u0438',
                'verbose_name_plural': '\u041e\u0446\u0435\u043d\u043a\u0438 \u043b\u0435\u043a\u0446\u0438\u0439',
            },
        ),
        migrations.CreateModel(
            name='Instructor',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('first_name', models.CharField(max_length=20, verbose_name='\u0418\u043c\u044f')),
                ('middle_name', models.CharField(max_length=20, verbose_name='\u041e\u0442\u0447\u0435\u0441\u0442\u0432\u043e')),
                ('last_name', models.CharField(max_length=20, verbose_name='\u0424\u0430\u043c\u0438\u043b\u0438\u044f')),
                ('cist_id', models.CharField(max_length=20, unique=True, null=True, verbose_name='ID \u043f\u0440\u0435\u043f\u043e\u0434\u0430\u0432\u0430\u0442\u0435\u043b\u044f (CIST)', blank=True)),
                ('department', models.ManyToManyField(to='app.Department')),
            ],
            options={
                'verbose_name': '\u041f\u0440\u0435\u043f\u043e\u0434\u0430\u0432\u0430\u0442\u0435\u043b\u044c',
                'verbose_name_plural': '\u041f\u0440\u0435\u043f\u043e\u0434\u0430\u0432\u0430\u0442\u0435\u043b\u0438',
            },
        ),
        migrations.CreateModel(
            name='Question',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(default='\u0412\u043e\u043f\u0440\u043e\u0441', max_length=40, null=True, blank=True)),
                ('text', models.TextField()),
                ('private', models.BooleanField(default=True)),
                ('date', models.DateTimeField(default=None, null=True, blank=True)),
                ('gtsi', models.ForeignKey(to='app.GTS_Instructor')),
                ('student', models.ForeignKey(to='app.StudentProfile')),
            ],
            options={
                'verbose_name': '\u0412\u043e\u043f\u0440\u043e\u0441',
                'verbose_name_plural': '\u0412\u043e\u043f\u0440\u043e\u0441\u044b',
            },
        ),
        migrations.CreateModel(
            name='Speciality',
            fields=[
                ('speciality_id', models.CharField(max_length=10, serialize=False, primary_key=True)),
                ('short_name', models.CharField(max_length=40)),
                ('full_name', models.CharField(max_length=150)),
                ('direction', models.ForeignKey(to='app.Direction')),
            ],
            options={
                'verbose_name': '\u0421\u043f\u0435\u0446\u0438\u0430\u043b\u044c\u043d\u043e\u0441\u0442\u044c',
                'verbose_name_plural': '\u0421\u043f\u0435\u0446\u0438\u0430\u043b\u044c\u043d\u043e\u0441\u0442\u0438',
            },
        ),
        migrations.CreateModel(
            name='Subject',
            fields=[
                ('subject_id', models.CharField(max_length=30, serialize=False, primary_key=True)),
                ('full_name', models.CharField(max_length=150, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u043f\u0440\u0435\u0434\u043c\u0435\u0442\u0430')),
                ('short_name', models.CharField(max_length=40)),
            ],
            options={
                'verbose_name': '\u041f\u0440\u0435\u0434\u043c\u0435\u0442',
                'verbose_name_plural': '\u041f\u0440\u0435\u0434\u043c\u0435\u0442\u044b',
            },
        ),
        migrations.AddField(
            model_name='group',
            name='curator',
            field=models.ForeignKey(default=None, blank=True, to='app.InstructorProfile', null=True, verbose_name='\u041a\u0443\u0440\u0438\u0440\u0443\u0435\u043c\u0430\u044f \u0433\u0440\u0443\u043f\u043f\u0430'),
        ),
        migrations.AlterField(
            model_name='customuser',
            name='activation_key',
            field=models.CharField(max_length=100, verbose_name='\u041a\u043b\u044e\u0447 \u0430\u043a\u0442\u0438\u0432\u0430\u0446\u0438\u0438', blank=True),
        ),
        migrations.AlterField(
            model_name='instructorprofile',
            name='middle_name',
            field=models.CharField(max_length=20, verbose_name='\u041e\u0442\u0447\u0435\u0441\u0442\u0432\u043e'),
        ),
        migrations.AlterField(
            model_name='instructorprofile',
            name='phone_number',
            field=models.CharField(default='', max_length=15, verbose_name='\u041d\u043e\u043c\u0435\u0440 \u0442\u0435\u043b\u0435\u0444\u043e\u043d\u0430'),
        ),
        migrations.AddField(
            model_name='instructor',
            name='instructor_profile',
            field=models.OneToOneField(related_name='pub_profile', null=True, default=None, to='app.InstructorProfile', blank=True, verbose_name='\u041f\u0443\u0431\u043b\u0438\u0447\u043d\u044b\u0439 \u043f\u0440\u043e\u0444\u0438\u043b\u044c'),
        ),
        migrations.AddField(
            model_name='gts_instructor',
            name='instructor',
            field=models.ForeignKey(default=None, blank=True, to='app.Instructor', null=True, verbose_name='\u041f\u0440\u0435\u043f\u043e\u0434\u0430\u0432\u0430\u0442\u0435\u043b\u044c'),
        ),
        migrations.AddField(
            model_name='group_term_subject',
            name='group',
            field=models.ForeignKey(default=None, blank=True, to='app.Group', null=True, verbose_name='\u0413\u0440\u0443\u043f\u043f\u0430'),
        ),
        migrations.AddField(
            model_name='group_term_subject',
            name='subject',
            field=models.ManyToManyField(to='app.Subject'),
        ),
        migrations.AddField(
            model_name='direction',
            name='faculty',
            field=models.ForeignKey(to='app.Faculty'),
        ),
        migrations.AddField(
            model_name='department',
            name='faculty',
            field=models.ForeignKey(to='app.Faculty'),
        ),
        migrations.AddField(
            model_name='deadline',
            name='deadline_type',
            field=models.ManyToManyField(to='app.DeadlineType'),
        ),
        migrations.AddField(
            model_name='deadline',
            name='gtsi',
            field=models.ForeignKey(to='app.GTS_Instructor'),
        ),
        migrations.AddField(
            model_name='answer',
            name='author',
            field=models.ForeignKey(default=None, blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='answer',
            name='question',
            field=models.ForeignKey(related_name='answer', to='app.Question'),
        ),
        migrations.AddField(
            model_name='announcement',
            name='author',
            field=models.ForeignKey(default=None, blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='announcement',
            name='group',
            field=models.ForeignKey(default=None, blank=True, to='app.Group', null=True),
        ),
        migrations.AddField(
            model_name='group',
            name='speciality',
            field=models.ForeignKey(default=None, blank=True, to='app.Speciality', null=True),
        ),
    ]
