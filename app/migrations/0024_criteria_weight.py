# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0023_auto_20151207_1222'),
    ]

    operations = [
        migrations.AddField(
            model_name='criteria',
            name='weight',
            field=models.FloatField(default=1),
        ),
    ]
