# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0057_auto_20160928_0811'),
    ]

    operations = [
        migrations.AlterField(
            model_name='studentratingdata',
            name='rating_id',
            field=models.IntegerField(default=2, verbose_name='\u0418\u0434\u0435\u043d\u0442\u0438\u0444\u0438\u043a\u0430\u0442\u043e\u0440 \u0440\u0435\u0439\u0442\u0438\u043d\u0433\u0430'),
        ),
    ]
