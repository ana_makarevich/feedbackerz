# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0054_auto_20160717_1846'),
    ]

    operations = [
        migrations.AddField(
            model_name='gtsi_feedback',
            name='complaint_status',
            field=models.CharField(default='\u041d', max_length=10, choices=[('\u041d', '\u041d\u043e\u0432\u043e\u0435'), ('\u041e', '\u041e\u0431\u0440\u0430\u0431\u043e\u0442\u0430\u043d\u043e')]),
        ),
        migrations.AddField(
            model_name='gtsi_feedback',
            name='pub_status',
            field=models.CharField(default='\u041d', max_length=10, choices=[('\u041d', '\u041d\u043e\u0432\u043e\u0435'), ('\u041e\u0414', '\u041e\u0434\u043e\u0431\u0440\u0435\u043d\u043e'), ('\u041e\u0422\u041a', '\u041e\u0442\u043a\u0430\u0437\u0430\u043d\u043e')]),
        ),
        migrations.AddField(
            model_name='gtsi_feedback',
            name='timestamp',
            field=models.DateField(default=django.utils.timezone.now, auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='gtsil_feedback',
            name='timestamp',
            field=models.DateField(default=datetime.datetime(2016, 7, 17, 19, 20, 43, 185175), auto_now_add=True),
            preserve_default=False,
        ),
    ]
