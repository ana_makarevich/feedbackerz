# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0026_auto_20160212_1723'),
    ]

    operations = [
        migrations.AlterField(
            model_name='gtsi_lecture',
            name='lecture_start_time',
            field=models.DateField(default=None, null=True, blank=True),
        ),
    ]
