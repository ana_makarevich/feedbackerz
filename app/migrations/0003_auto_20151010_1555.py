# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0002_auto_20151010_1435'),
    ]

    operations = [
        migrations.CreateModel(
            name='SubjectCriteria',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('criteria_name', models.CharField(max_length=100, null=True, blank=True)),
                ('criteria_description', models.TextField(default=None, null=True, blank=True)),
            ],
            options={
                'verbose_name': '\u041a\u0440\u0438\u0442\u0435\u0440\u0438\u0439 (\u0434\u043b\u044f \u043f\u0440\u0435\u0434\u043c\u0435\u0442\u0430)',
                'verbose_name_plural': '\u041a\u0440\u0438\u0442\u0435\u0440\u0438\u0438 (\u0434\u043b\u044f \u043f\u0440\u0435\u0434\u043c\u0435\u0442\u0430)',
            },
        ),
        migrations.RenameModel(
            old_name='Criteria',
            new_name='LectureCriteria',
        ),
        migrations.AlterModelOptions(
            name='lecturecriteria',
            options={'verbose_name': '\u041a\u0440\u0438\u0442\u0435\u0440\u0438\u0439 (\u0434\u043b\u044f \u043b\u0435\u043a\u0446\u0438\u0438)', 'verbose_name_plural': '\u041a\u0440\u0438\u0442\u0435\u0440\u0438\u0438 (\u0434\u043b\u044f \u043b\u0435\u043a\u0446\u0438\u0438)'},
        ),
        migrations.AlterField(
            model_name='group',
            name='curator',
            field=models.ForeignKey(default=None, blank=True, to='app.InstructorProfile', null=True, verbose_name='\u041a\u0443\u0440\u0430\u0442\u043e\u0440'),
        ),
        migrations.AlterField(
            model_name='group',
            name='speciality',
            field=models.ForeignKey(default=None, blank=True, to='app.Speciality', null=True, verbose_name='\u0421\u043f\u0435\u0446\u0438\u0430\u043b\u044c\u043d\u043e\u0441\u0442\u044c'),
        ),
        migrations.AlterField(
            model_name='gtsi_criteria_user_vote',
            name='criteria',
            field=models.ManyToManyField(to='app.SubjectCriteria'),
        ),
    ]
