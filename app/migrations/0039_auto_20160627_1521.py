# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0038_studentratingdata_integral'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='studentratingdata',
            options={'verbose_name': '\u0420\u0435\u0439\u0442\u0438\u043d\u0433 \u0434\u043b\u044f \u0433\u0440\u0430\u043d\u0442\u0430', 'verbose_name_plural': '\u0420\u0435\u0439\u0442\u0438\u043d\u0433\u0438 \u0434\u043b\u044f \u0433\u0440\u0430\u043d\u0442\u0430'},
        ),
        migrations.AddField(
            model_name='studentratingdata',
            name='department',
            field=models.ForeignKey(default=None, blank=True, to='app.Department', null=True),
        ),
        migrations.AddField(
            model_name='studentratingdata',
            name='department_txt',
            field=models.TextField(default=None, max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='studentratingdata',
            name='faculty',
            field=models.ForeignKey(default=None, blank=True, to='app.Faculty', null=True),
        ),
        migrations.AddField(
            model_name='studentratingdata',
            name='faculty_txt',
            field=models.TextField(default=None, max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='studentratingdata',
            name='group',
            field=models.ForeignKey(default=None, blank=True, to='app.Group', null=True),
        ),
        migrations.AddField(
            model_name='studentratingdata',
            name='group_txt',
            field=models.TextField(default=None, max_length=100, null=True),
        ),
    ]
