# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0017_customuser_language'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customuser',
            name='language',
            field=models.CharField(default='RU', max_length=2, choices=[('RU', '\u0420\u0443\u0441\u0441\u043a\u0438\u0439'), ('UK', '\u0423\u043a\u0440\u0430\u0438\u043d\u0441\u043a\u0438\u0439'), ('EN', '\u0410\u043d\u0433\u043b\u0438\u0439\u0441\u043a\u0438\u0439')]),
        ),
    ]
