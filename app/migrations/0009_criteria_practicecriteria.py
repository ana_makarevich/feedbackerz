# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0008_auto_20151107_2117'),
    ]

    operations = [
        migrations.CreateModel(
            name='Criteria',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('criteria_name', models.CharField(max_length=100, null=True, blank=True)),
                ('criteria_description', models.TextField(default=None, null=True, blank=True)),
                ('criteria_source', models.CharField(max_length=250, null=True, blank=True)),
                ('overall', models.BooleanField(default=False)),
                ('activity_type', models.ForeignKey(to='app.ActivityType')),
                ('department', models.ForeignKey(default=None, blank=True, to='app.Department', null=True)),
                ('faculty', models.ForeignKey(default=None, blank=True, to='app.Faculty', null=True)),
            ],
            options={
                'verbose_name': '\u041a\u0440\u0438\u0442\u0435\u0440\u0438\u0439',
                'verbose_name_plural': '\u041a\u0440\u0438\u0442\u0435\u0440\u0438\u0438',
            },
        ),
        migrations.CreateModel(
            name='PracticeCriteria',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('criteria_name', models.CharField(max_length=100, null=True, blank=True)),
                ('criteria_description', models.TextField(default=None, null=True, blank=True)),
            ],
            options={
                'verbose_name': '\u041a\u0440\u0438\u0442\u0435\u0440\u0438\u0439 (\u0434\u043b\u044f \u043f\u0437)',
                'verbose_name_plural': '\u041a\u0440\u0438\u0442\u0435\u0440\u0438\u0438 (\u0434\u043b\u044f \u043f\u0437)',
            },
        ),
    ]
