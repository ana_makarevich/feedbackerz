# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0037_instructorprofile_can_apply_for_student_grant'),
    ]

    operations = [
        migrations.AddField(
            model_name='studentratingdata',
            name='integral',
            field=models.PositiveSmallIntegerField(default=0),
        ),
    ]
