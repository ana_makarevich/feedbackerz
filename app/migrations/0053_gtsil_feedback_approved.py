# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0052_gtsil_feedback_complaint_status'),
    ]

    operations = [
        migrations.AddField(
            model_name='gtsil_feedback',
            name='approved',
            field=models.BooleanField(default=False),
        ),
    ]
