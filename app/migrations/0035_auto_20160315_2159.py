# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0034_auto_20160313_1336'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='afterlecturechoice',
            options={'verbose_name': '\u0412\u0430\u0440\u0438\u0430\u043d\u0442 \u043e\u0442\u0432\u0435\u0442\u0430 \u0434\u043b\u044f \u043e\u043f\u0440\u043e\u0441\u0430', 'verbose_name_plural': '\u0412\u0430\u0440\u0438\u0430\u043d\u0442\u044b \u043e\u0442\u0432\u0435\u0442\u0430 \u0434\u043b\u044f \u043e\u043f\u0440\u043e\u0441\u0430'},
        ),
        migrations.AlterModelOptions(
            name='afterlecturequestion',
            options={'verbose_name': '\u0412\u043e\u043f\u0440\u043e\u0441 \u0434\u043b\u044f \u043e\u043f\u0440\u043e\u0441\u0430', 'verbose_name_plural': '\u0412\u043e\u043f\u0440\u043e\u0441\u044b \u0434\u043b\u044f \u043e\u043f\u0440\u043e\u0441\u043e\u0432'},
        ),
        migrations.AlterModelOptions(
            name='afterlecturesurvey',
            options={'verbose_name': '\u041e\u043f\u0440\u043e\u0441 \u043f\u043e\u0441\u043b\u0435 \u043b\u0435\u043a\u0446\u0438\u0438', 'verbose_name_plural': '\u041e\u043f\u0440\u043e\u0441\u044b \u043f\u043e\u0441\u043b\u0435 \u043b\u0435\u043a\u0446\u0438\u0438'},
        ),
        migrations.AddField(
            model_name='afterlecturesurvey',
            name='author',
            field=models.ForeignKey(default=None, blank=True, to='app.InstructorProfile', null=True),
        ),
        migrations.AlterField(
            model_name='afterlecturechoice',
            name='choice_text',
            field=models.CharField(max_length=500),
        ),
    ]
