# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0013_auto_20151205_1351'),
    ]

    operations = [
        migrations.DeleteModel(
            name='LectureCriteria',
        ),
        migrations.DeleteModel(
            name='SubjectCriteria',
        ),
    ]
