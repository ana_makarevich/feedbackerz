# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0028_auto_20160224_1658'),
    ]

    operations = [
        migrations.AddField(
            model_name='gtsi_lecture',
            name='number_pair',
            field=models.PositiveSmallIntegerField(default=None, null=True, blank=True),
        ),
    ]
