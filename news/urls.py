#!/usr/bin/env python
# -*- coding:utf-8 -*-

from datetime import datetime
from django.conf.urls import patterns, url,include
from django.contrib import admin
from django.views.generic import TemplateView

from django.utils.translation import ugettext_lazy as _
from rest_framework import routers
from news.views import News, NewsPieceView, UpdateNewsPieceView, DeleteNewsPieceView, CreateNewsPieceView
# Uncomment the next lines to enable the admin:
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^news/$', News.as_view(), name='news',),
    url(r'^news/(?P<pk>\d+)/$', NewsPieceView.as_view(), name='newspiece-view',),
    url(r'^news/edit/(?P<pk>\d+)/$', UpdateNewsPieceView.as_view(), name='newspiece-edit',),
    url(r'^news/delete/(?P<pk>\d+)/$', DeleteNewsPieceView.as_view(), name='newspiece-delete',),
    url(r'^news/new$', CreateNewsPieceView.as_view(), name='newspiece-new',),)
