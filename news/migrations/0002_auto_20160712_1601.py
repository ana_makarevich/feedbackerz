# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='newspiece',
            name='in_rotation',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='newspiece',
            name='pinned',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='newspiece',
            name='priority',
            field=models.PositiveSmallIntegerField(default=1),
        ),
    ]
