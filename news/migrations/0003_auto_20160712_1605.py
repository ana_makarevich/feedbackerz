# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0002_auto_20160712_1601'),
    ]

    operations = [
        migrations.AlterField(
            model_name='newspiece',
            name='in_rotation',
            field=models.BooleanField(default=False, choices=[(True, 'Yes'), (False, 'No')]),
        ),
    ]
