# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('app', '0050_remove_gtsil_results_distribution_average'),
    ]

    operations = [
        migrations.CreateModel(
            name='NewsPiece',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=200)),
                ('body', models.TextField()),
                ('timestamp', models.DateTimeField()),
                ('short_description', models.TextField()),
                ('author', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
                ('tag', models.ManyToManyField(to='app.Tag', blank=True)),
            ],
        ),
    ]
