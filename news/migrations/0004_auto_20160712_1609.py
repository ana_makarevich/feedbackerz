# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0003_auto_20160712_1605'),
    ]

    operations = [
        migrations.AlterField(
            model_name='newspiece',
            name='in_rotation',
            field=models.BooleanField(default=False, choices=[(True, '\u0414\u0430'), (False, '\u041d\u0435\u0442')]),
        ),
        migrations.AlterField(
            model_name='newspiece',
            name='pinned',
            field=models.BooleanField(default=False, choices=[(True, '\u0414\u0430'), (False, '\u041d\u0435\u0442')]),
        ),
    ]
