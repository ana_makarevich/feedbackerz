#!/usr/bin/env python
# -*- coding:utf-8 -*-

# Стандартные библиотеки
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.
from news.models import NewsPiece

admin.site.register(NewsPiece)
