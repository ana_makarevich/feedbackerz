#!/usr/bin/env python
# -*- coding:utf-8 -*-

from __future__ import unicode_literals
from django.db import models
import datetime
from string import letters

# Библиотеки Джанго
from django.contrib.auth.models import AbstractUser
from django.utils import timezone
from django.utils.encoding import smart_unicode
from django.utils.translation import ugettext_lazy as _
from django.db.models.signals import post_save
from django.core.urlresolvers import reverse
from app.models import CustomUser
from blog.models import Tag
BOOL_CHOICES = ((True, u'Да'), (False, u'Нет'))

class NewsPiece(models.Model):
	title = models.CharField(max_length = 200)
	author = models.ForeignKey(CustomUser)
	body = models.TextField()
	timestamp = models.DateTimeField()
	short_description = models.TextField()
	pinned = models.BooleanField(default = False, choices = BOOL_CHOICES)
	in_rotation = models.BooleanField(default = False, choices = BOOL_CHOICES)
	tag = models.ManyToManyField(Tag, blank = True)
	priority = models.PositiveSmallIntegerField(default=1)

	def save(self, *args, **kwargs):
		if isinstance(self.title, str):
			self.title = self.title.decode("utf-8")
		if isinstance(self.body, str):
			self.body = self.body.decode("utf-8")
		if isinstance(self.short_description, str):
			self.short_description = self.short_description.decode("utf-8")
		super(NewsPiece, self).save(*args, **kwargs)	
	def get_absolute_url(self):
		return reverse('news:newspiece-view', kwargs={'pk': self.id})
