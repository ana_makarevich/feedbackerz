#!/usr/bin/env python
# -*- coding:utf-8 -*-

# Стандартные библиотеки
from __future__ import unicode_literals
import re
import datetime

# Библиотеки Джанго
#from django.contrib.auth.models import User
from django import forms
from django.contrib.auth.forms import AuthenticationForm
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ValidationError
from django.utils import timezone

# Импорт каптчи
#from nocaptcha_recaptcha.fields import NoReCaptchaField

# Локальные импорты 
from .models import NewsPiece

class CreateNewsPieceForm(forms.ModelForm):
	class Meta:
		model = NewsPiece
		fields = ('title', 'in_rotation', 'pinned', 'priority', 'short_description','body',)
		widgets = {
		'title': forms.TextInput(attrs={'class': 'form-control'}),
		'in_rotation': forms.RadioSelect,
		'pinned': forms.RadioSelect,
		'priority': forms.NumberInput(attrs={'class': 'form-control'}),
		'short_description': forms.Textarea(attrs={'class': 'form-control'}),
		'body': forms.Textarea(attrs={'class': 'form-control'}),
		}
		labels = {
		'title':_(u'Название новости'),
		'in_rotation':_(u'Выводить на главную?'),
		'pinned':_(u'Закрепить наверху?'),
		'priority':_(u'Приоритет новости (1-самый низкий, 5 - самый высокий)'),
		'short_description':_(u'Короткое описание'),
		'body':_(u'Тест новости'),
		}