#!/usr/bin/env python
# -*- coding:utf-8 -*-
from django.shortcuts import render

from django.views.generic import ListView, DetailView, UpdateView, DeleteView, CreateView
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.urlresolvers import reverse

from news.models import NewsPiece
from news.forms import CreateNewsPieceForm
from django.utils import timezone, translation
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import permission_required, login_required
from django.utils.decorators import method_decorator

# Create your views here.

class News(ListView):
    def get(self, request, *args, **kwargs):
        queryset = NewsPiece.objects.order_by('-timestamp')
        paginator = Paginator(queryset, 2) # Show 25 contacts per page

        page = request.GET.get('page')
        try:
            object_list = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            object_list = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            object_list = paginator.page(paginator.num_pages)


        return render(request, 'news/newspiece_list.html', {'object_list': object_list})


class NewsPieceView(DetailView):

    model = NewsPiece
    template_name = 'news/newspiece.html'


class UpdateNewsPieceView(UpdateView):
	model = NewsPiece
	form_class = CreateNewsPieceForm

	template_name = 'news/edit_news.html'

	def get_success_url(self):
		return reverse('news:news')

		def get_context_data(self, **kwargs):

			context = super(UpdateNewsPieceView, self).get_context_data(**kwargs)
			context['action'] = reverse('news:newspiece-edit', kwargs={'pk': self.get_object().id})

			return context
	@method_decorator(login_required)
	@method_decorator(permission_required('news.delete_newspiece',raise_exception=True))
	def dispatch(self, *args, **kwargs):
		return super(UpdateNewsPieceView, self).dispatch(*args, **kwargs)


class DeleteNewsPieceView(DeleteView):

    model = NewsPiece
    template_name = 'app/delete_blogpost.html'

    def get_success_url(self):
        return reverse('news:news')

    @method_decorator(login_required)
    @method_decorator(permission_required('news.delete_newspiece',raise_exception=True))
    def dispatch(self, *args, **kwargs):
    	user = self.request.user
    	return super(DeleteNewsPieceView, self).dispatch(*args, **kwargs)

class CreateNewsPieceView(CreateView):

    model = NewsPiece
    form_class = CreateNewsPieceForm
    template_name = 'news/edit_news.html'
    def form_valid(self, form):
        post = form.save(commit=False)
        post.author = self.request.user
        post.timestamp = timezone.now()
        return super(CreateNewsPieceView, self).form_valid(form)


    def get_success_url(self):
        return reverse('news:news')

    def get_context_data(self, **kwargs):

        context = super(CreateNewsPieceView, self).get_context_data(**kwargs)
        context['action'] = reverse('news:newspiece-new')
        context['user'] = self.request.user

        return context
    @method_decorator(login_required)
    @method_decorator(permission_required('news.add_newspiece',raise_exception=True))
    def dispatch(self, *args, **kwargs):
    	user = self.request.user
    	return super(CreateNewsPieceView, self).dispatch(*args, **kwargs)